package com.meetch.service;

import android.content.Context;
import android.content.Intent;
import android.util.Log;


import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.meetch.config.Config;
import com.meetch.constant.GetUserId;
import com.meetch.storage.MySharedPref;
import com.meetch.utils.NotificationUtils;

import org.json.JSONException;
import org.json.JSONObject;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    private NotificationUtils notificationUtils;
    String user_id,str_title,str_msg;
MySharedPref sp=new MySharedPref();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        user_id= GetUserId.getUserInfo(getApplicationContext());

        if (remoteMessage == null)
            return;

        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getBody());
        }

            if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());
            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());

                JSONObject data = json.optJSONObject("data");

                str_title=data.getString("title");
                str_msg=data.getString("message");

                JSONObject payload = data.getJSONObject("payload");

                String payload_userId = payload.getString("id");
                System.out.println("User Id Payload is@@@@"+payload_userId);
                System.out.println("User Id is@@@@"+user_id);

             //   handleInvitationMessage(payload);


                if(user_id!=null && !user_id.equalsIgnoreCase("") && !user_id.equalsIgnoreCase("null"))
                     //   && user_id.equalsIgnoreCase(payload_userId))
                {

                    handleInvitationMessage(payload);
                    }

                else
                {

                    handleNotification(remoteMessage.getNotification().getBody());

                    Log.i("user not login","loggedout");
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    private void handleNotification(String message) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();

            showNotificationMessage(getApplicationContext(), message, message, " ",pushNotification);

        }else{
            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
            showNotificationMessage(getApplicationContext(), message, message, " ",pushNotification);

        }
    }

    private void handleInvitationMessage(JSONObject payload) {

        try {


            try {
                String type = payload.getString("type");
               // showNotificationMessage(getApplicationContext(), str_title, chat_msg, " ", chat_intent);

            } catch (JSONException ex) {
                ex.printStackTrace();
            }


        }
        catch (Exception e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        }

    }


    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }
}
