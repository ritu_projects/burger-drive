package com.meetch.service;

import android.content.Intent;
import android.content.SharedPreferences;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.meetch.config.Config;
import com.meetch.storage.MySharedPref;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();
    private SharedPreferences pref;
    MySharedPref sp=new MySharedPref();
    @Override
    public void onTokenRefresh() {

        super.onTokenRefresh();

       String  refreshedToken = FirebaseInstanceId.getInstance().getToken();

        System.out.println("Refreshed Token is@@@"+refreshedToken);

        setTokenToPrefernce(refreshedToken);

        Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("fcmtoken", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void setTokenToPrefernce(String refreshedToken) {

        sp.saveData(getApplicationContext(),"fcmtoken",refreshedToken);
        System.out.println("Refreshed Token is@@@" + refreshedToken);
    }


}

