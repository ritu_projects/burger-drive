package com.meetch.rectrofit;



import com.meetch.bean.SignInResponseModelResponse.SignInResponseModel;
import com.meetch.bean.addDeliveryAddressmodel.AddtoDeliveryAddressModel;
import com.meetch.bean.addtoCart_model.Addtocart_Model;
import com.meetch.bean.addtocart_combo.Addtocart_Combo_Bean;
import com.meetch.bean.applypromomodel.Applypromocode_Model;
import com.meetch.bean.cartdelete_model.CartDeleteModel;
import com.meetch.bean.changepasswordbean.ChangePasswordBean;
import com.meetch.bean.checkoutmodel.Checkout_Model;
import com.meetch.bean.combodetailsparam_bean.ComboDetailsParams;
import com.meetch.bean.deletedeliver_bean.DeleteDeliveryAdd_Bean;
import com.meetch.bean.forgotpassword_model.ForgotPasswordModel;
import com.meetch.bean.forgotpassword_model.ForgotPasswordResponse;
import com.meetch.bean.getProduct_model.ProductModel;
import com.meetch.bean.getseachproductparameterbean.GetSearchProductParameter_Bean;
import com.meetch.bean.loginmodel.LoginModel;
import com.meetch.bean.orderdetails_bean.OrderDetailsBean;
import com.meetch.bean.ordertracking_bean.OrderTrackingBean;
import com.meetch.bean.productid_model.ProductId_Model;
import com.meetch.bean.sendchatbean.ChatSend_Bean;
import com.meetch.bean.signin_model.RegisterModel;
import com.meetch.bean.signin_model.RegisterResponse;
import com.meetch.bean.singlecasedeatial.Singlecasemodel;
import com.meetch.constant.Constants;


import okhttp3.ResponseBody;
import retrofit2.Call;

import retrofit2.http.Body;

import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Groofl on 23/10/17.
 */

public interface LoadInterface {


//----------------------------------- **** For Login ****---------------------------------------------------------------------------//

 @POST("api/login")
   @Headers({
            Constants.CONST_ACCEPT,
    })

    Call<SignInResponseModel> user_login(@Body LoginModel jsonObject);

//----------------------------------- **** For Registration ****---------------------------------------------------------------------------//
@POST("api/registration")
@Headers({
        Constants.CONST_ACCEPT,
})

Call<RegisterResponse> user_registration(@Body RegisterModel jsonObject);

    //----------------------------------- **** For ForgotPassword ****---------------------------------------------------------------------------//
    @POST("api/userForgotPassword")
    @Headers({
            Constants.CONST_ACCEPT,
    })

    Call<ForgotPasswordResponse> user_forgotpassword(@Body ForgotPasswordModel jsonObject);

    //----------------------------------- **** For profile detail ****---------------------------------------------------------------------------//
    @POST("api/getProfile")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> get_profile_detail(
            @Header("user-id") String userid,
            @Header("token") String token,
            @Body Singlecasemodel jsonObject);

    //----------------------------------- **** For Get Category ****---------------------------------------------------------------------------//
    @GET("api/getCategory")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> get_Category();

    //----------------------------------- **** For Get Category ****---------------------------------------------------------------------------//
    @GET("api/getComboList")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> get_ComboList();

    //----------------------------------- **** For Get Category ****---------------------------------------------------------------------------//
    @POST("api/comboDetail")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> get_comboDetail( @Body ComboDetailsParams comboDetailsParams);

    //----------------------------------- **** For Product ****---------------------------------------------------------------------------//
    @POST("api/getProduct")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> get_Product( @Body ProductModel jsonObject);


    //----------------------------------- **** For Addtocart detail ****---------------------------------------------------------------------------//
    @POST("api/addToCart")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> addtoCart(@Body Addtocart_Model jsonObject);

    //----------------------------------- **** For Addtocart detail ****---------------------------------------------------------------------------//
    @POST("api/addToCart")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> addtoCartCombo(@Body Addtocart_Combo_Bean jsonObject);


    //----------------------------------- **** For Get Banner ****---------------------------------------------------------------------------//
    @GET("api/getBanner")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> get_Banner();

    //----------------------------------- **** For Get Single Product Details ****---------------------------------------------------------------------------//
    @POST("api/getSingleProduct")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> get_SingleProduct(@Body ProductId_Model jsonObject);

    //----------------------------------- **** For AllDeliverAddress ****---------------------------------------------------------------------------//
    @POST("api/allDeliveryAddress")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> getallDeliveryAddress(
            @Body Singlecasemodel jsonObject);


    //----------------------------------- **** For Add DeliverAddress ****---------------------------------------------------------------------------//
    @POST("api/addDeliveryAddress")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> addDeliveryAddress(
            @Body AddtoDeliveryAddressModel jsonObject);

    //----------------------------------- **** For Get UserCart ****---------------------------------------------------------------------------//
    @POST("api/getUserCart")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> getUserCart(
            @Body Singlecasemodel jsonObject);

    //----------------------------------- **** For ApplyPromocode ****---------------------------------------------------------------------------//
    @POST("api/applyPromoCode")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> applyPromoCode(
            @Body Applypromocode_Model jsonObject);

    //----------------------------------- **** For Get UserCart Details ****---------------------------------------------------------------------------//
    @POST("api/getUserCartDetail")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> getUserCartDetail(
            @Body Singlecasemodel jsonObject);

    //----------------------------------- **** For checkout ****---------------------------------------------------------------------------//
    @POST("api/checkout")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> checkout(
            @Body Checkout_Model jsonObject);

    //----------------------------------- **** For getCurrentOrderList ****---------------------------------------------------------------------------//
    @POST("api/userCurrentorders")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> userCurrentorders(
            @Header("user-id") String userid,
            @Header("token") String token,
            @Body Singlecasemodel jsonObject);
    //----------------------------------- **** For getPastOrderList ****---------------------------------------------------------------------------//
    @POST("api/userpastorders")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> userpastorders(
            @Header("user-id") String userid,
            @Header("token") String token,
            @Body Singlecasemodel jsonObject);

    //----------------------------------- **** For getUserOrderSummary ****---------------------------------------------------------------------------//
    @POST("api/getUserOrderSummary")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> getUserOrderSummary(
            @Header("user-id") String userid,
            @Header("token") String token,
            @Body OrderDetailsBean jsonObject);

    //----------------------------------- **** For removeCart ****---------------------------------------------------------------------------//
    @POST("api/remove_cart_single")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> remove_cart_single(
            @Body CartDeleteModel jsonObject);

    //----------------------------------- **** For FAQ ****---------------------------------------------------------------------------//
    @GET("api/faq")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> faq();

    //----------------------------------- **** For getUserOrderTracking ****---------------------------------------------------------------------------//
    @POST("api/trackOrder")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> trackOrder(
            @Header("user-id") String userid,
            @Header("token") String token,
            @Body OrderTrackingBean jsonObject);

    //----------------------------------- **** For getUserOrderSummary ****---------------------------------------------------------------------------//
    @POST("api/userCancelOrder")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> userCancelOrder(

            @Body OrderDetailsBean jsonObject);

    //----------------------------------- **** For getUserOrderSummary ****---------------------------------------------------------------------------//
    @POST("api/orderchat")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> chatSend_Bean(

            @Body ChatSend_Bean jsonObject);

    //----------------------------------- **** For getUserOrderSummary ****---------------------------------------------------------------------------//
    @POST("api/getOrderchat")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> getOrderchat(

            @Body OrderTrackingBean jsonObject);

    //----------------------------------- **** For DeleteDelivery Address ****---------------------------------------------------------------------------//
    @POST("api/deleteDeliveryAddress")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> deleteDeliveryAddress(

            @Body DeleteDeliveryAdd_Bean jsonObject);
    //----------------------------------- **** For favorite ****---------------------------------------------------------------------------//
    @POST("api/favorite")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> favorite(
            @Header("user-id") String userid,
            @Header("token") String token,
            @Body Singlecasemodel jsonObject);

    //----------------------------------- **** For favorite Recent ****---------------------------------------------------------------------------//
    @POST("api/getRecentPurchase")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> getRecentPurchase(@Body Singlecasemodel jsonObject);

    //----------------------------------- **** For favorite ****---------------------------------------------------------------------------//
    @POST("api/getSpecialBurger")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> getSpecialBurger(@Body Singlecasemodel jsonObject);
    //----------------------------------- **** For Change Password ****---------------------------------------------------------------------------//
    @POST("api/uchangePassword")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> uchangePassword(

            @Body ChangePasswordBean jsonObject);

    //----------------------------------- **** For Seach Product ****---------------------------------------------------------------------------//
    @POST("api/searchProduct")
    @Headers({
            Constants.CONST_ACCEPT,

    })
    Call<ResponseBody> get_SearchProduct( @Body GetSearchProductParameter_Bean jsonObject);

}
