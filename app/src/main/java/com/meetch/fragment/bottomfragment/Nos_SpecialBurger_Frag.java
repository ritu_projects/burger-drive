package com.meetch.fragment.bottomfragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.meetch.R;

import java.util.ArrayList;

public class Nos_SpecialBurger_Frag extends Fragment implements View.OnClickListener {

    private RecyclerView favi_list_rcyle;
    private RelativeLayout rr_back;
    private TextView title;
    private Nos_SpecialBurger_Adp nos_specialBurger_frag;
    private ArrayList<String> arrayList=new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.specialburger_frag, container, false);

        init(v);
        return v;
    }

    private void init(View v) {
        arrayList.clear();
        arrayList.add("");
        favi_list_rcyle=(RecyclerView)v.findViewById(R.id.favi_list_rcyle);
        rr_back=(RelativeLayout)v.findViewById(R.id.rr_back);
        title=(TextView)v.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.text_Favo));

        rr_back.setOnClickListener(this);
        nos_specialBurger_frag=new Nos_SpecialBurger_Adp(getActivity(),arrayList);
        favi_list_rcyle.setLayoutManager(new LinearLayoutManager(getActivity()));
        favi_list_rcyle.setAdapter(nos_specialBurger_frag);

    }

    @Override
    public void onClick(View v) {
        if (v == rr_back) {
            getActivity().finish();
        }

    }



    public class Nos_SpecialBurger_Adp extends RecyclerView.Adapter<Nos_SpecialBurger_Adp.ViewHolder> {

        Context context;
        ArrayList<String>arrayList;

        public Nos_SpecialBurger_Adp(Activity homeActivity, ArrayList<String> arrayList) {
            this.context=homeActivity;
            this.arrayList=arrayList;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

            View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.favo_item,viewGroup,false);
           ViewHolder viewHolder=new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public ViewHolder(@NonNull View itemView) {
                super(itemView);
            }
        }
    }
}
