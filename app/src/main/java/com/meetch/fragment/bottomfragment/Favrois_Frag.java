package com.meetch.fragment.bottomfragment;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.meetch.R;
import com.meetch.adapter.Favo_ItemAdapter;
import com.meetch.bean.myfavorite_bean.MyFavorite_Bean;
import com.meetch.bean.singlecasedeatial.Singlecasemodel;
import com.meetch.constant.Constants;
import com.meetch.constant.GetUserId;
import com.meetch.constant.Utils;
import com.meetch.rectrofit.LoadInterface;
import com.meetch.utils.CustomTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Favrois_Frag extends Fragment implements View.OnClickListener {
    private RecyclerView favi_list_rcyle;
    private RelativeLayout rr_back;
    private Favo_ItemAdapter favo_itemAdapter;
    private ArrayList<String> arrayList=new ArrayList<>();

    ProgressBar loader;
    MyFavorite_Bean myFavorite_bean;
    CustomTextView title,txt_no_order_found;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.activity_favorites, container, false);

        init(v);
        return v;
    }

    private void init(View v) {
        arrayList.clear();
        arrayList.add("");
        favi_list_rcyle=(RecyclerView)v.findViewById(R.id.favi_list_rcyle);
        loader=(ProgressBar)v.findViewById(R.id.loader);
        rr_back=(RelativeLayout)v.findViewById(R.id.rr_back);
        txt_no_order_found=(CustomTextView)v.findViewById(R.id.txt_no_order_found);
        title=(CustomTextView) v.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.text_Favo));
rr_back.setVisibility(View.GONE);
        rr_back.setOnClickListener(this);
        callWebService();

    }

    @Override
    public void onClick(View v) {
        if(v==rr_back){

          //  getActivity().finish();
        }

    }
    public void callWebService()
    {
        if(Utils.isConnected(getActivity()))
        {

            try {
                getfavProduct();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }

    public  void getfavProduct()
    {

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        String  userid= GetUserId.getUserInfo(getActivity());
        String  usertoken= GetUserId.getUserToken(getActivity());

        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        Singlecasemodel singlecasemodel = new Singlecasemodel();
        singlecasemodel.setUserid(userid);

        Call<ResponseBody> resultCall = loadInterface.getRecentPurchase(singlecasemodel);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {

                                txt_no_order_found.setVisibility(View.GONE);

                                Gson gson = new Gson();
                                myFavorite_bean = gson.fromJson(responedata, MyFavorite_Bean.class);

                                favo_itemAdapter=new Favo_ItemAdapter(getActivity(),myFavorite_bean.getOrderList());
                                favi_list_rcyle.setLayoutManager(new LinearLayoutManager(getActivity()));
                                favi_list_rcyle.setAdapter(favo_itemAdapter);


                            }

                            else
                            {
                                favi_list_rcyle.setVisibility(View.GONE);
                                txt_no_order_found.setVisibility(View.VISIBLE);

                                txt_no_order_found.setText(getResources().getString(R.string.no_favproduct));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });

    }

}
