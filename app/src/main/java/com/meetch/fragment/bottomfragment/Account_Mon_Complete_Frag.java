package com.meetch.fragment.bottomfragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.meetch.R;
import com.meetch.activity.LoginActivity;
import com.meetch.activity.MyOrders;
import com.meetch.activity.bottom_add_delivery_address_gererladdress.Add_Gererladdress_Delivery_Address_Activity;
import com.meetch.activity.bottom_history_des_commandes.Historique_Des_Commandes_Activities;
import com.meetch.activity.bottom_mon_compte.ProfileActivity;
import com.meetch.activity.bottom_partage_lapplication.Partage_Lapplication_Activity;
import com.meetch.activity.bottom_soutien.Soutien_Activity;
import com.meetch.activity.mode_de_paiement.Mode_De_Paiement_Activity;
import com.meetch.apiclient.ApiClient;
import com.meetch.bean.singlecasedeatial.Singlecasemodel;
import com.meetch.constant.GetUserId;
import com.meetch.constant.Utils;
import com.meetch.rectrofit.LoadInterface;
import com.meetch.storage.MySharedPref;
import com.meetch.utils.CustomTextView;
import com.meetch.utils.CustomTextViewRegular;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

public class Account_Mon_Complete_Frag extends Fragment implements View.OnClickListener {
    RelativeLayout rr_profil,rr_mode_de_paiement,rr_historique_des_commandes,rr_soutien,
            rr_gérer_l_adresse,rr_partager_l_application,rr_se_déconnecter,rr_myorders;
        MySharedPref sp=new  MySharedPref();
String loggedin;
CircleImageView profile_icon;
ProgressBar loader;
    private CustomTextView title;

    private RelativeLayout rr_back;
    Dialog dialog;

        @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.mon_compte_bottom, container, false);
            loggedin=sp.getData(getActivity(),"ldata","null");
        init(v);
        return v;
    }

    private void init(View v) {

        rr_profil=(RelativeLayout)v.findViewById(R.id.rr_profil);
        rr_profil.setOnClickListener(this);

        rr_mode_de_paiement=(RelativeLayout)v.findViewById(R.id.rr_mode_de_paiement);
        rr_mode_de_paiement.setOnClickListener(this);

        profile_icon=(CircleImageView)v.findViewById(R.id.profile_icon);
        loader=(ProgressBar)v.findViewById(R.id.loader);

        rr_back=(RelativeLayout)v.findViewById(R.id.rr_back);
        title=(CustomTextView) v.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.account_mon_compte));
        rr_back.setVisibility(View.GONE);
     //   rr_back.setOnClickListener(this);

        getImage();


        rr_historique_des_commandes=(RelativeLayout)v.findViewById(R.id.rr_historique_des_commandes);
        rr_historique_des_commandes.setOnClickListener(this);

        rr_myorders=(RelativeLayout)v.findViewById(R.id.rr_myorders);
        rr_myorders.setOnClickListener(this);

        rr_soutien=(RelativeLayout)v.findViewById(R.id.rr_soutien);
        rr_soutien.setOnClickListener(this);

        rr_gérer_l_adresse=(RelativeLayout)v.findViewById(R.id.rr_gérer_l_adresse);
        rr_gérer_l_adresse.setOnClickListener(this);

        rr_partager_l_application=(RelativeLayout)v.findViewById(R.id.rr_partager_l_application);
        rr_partager_l_application.setOnClickListener(this);

        rr_se_déconnecter=(RelativeLayout)v.findViewById(R.id.rr_se_déconnecter);
        rr_se_déconnecter.setOnClickListener(this);

        if(loggedin!=null && !loggedin.equalsIgnoreCase("")  &&
                !loggedin.equalsIgnoreCase("null")) {

            rr_se_déconnecter.setVisibility(View.VISIBLE);
        }

        else
        {
            rr_se_déconnecter.setVisibility(View.GONE);

        }
    }


    private void getImage() {


        if(loggedin!=null && !loggedin.equalsIgnoreCase("")  &&
                !loggedin.equalsIgnoreCase("null")) {


                    callWebService();
        }
        else
        {

        }
    }
    public void callWebService()

    {
        if(Utils.isConnected(getActivity()))
        {

            try {
                getProfile();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }
    public void getProfile()
    {
        loader.setVisibility(View.VISIBLE);
        Retrofit retrofit= ApiClient.getClient(getActivity());
String userid= GetUserId.getUserInfo(getActivity());
        String token= GetUserId.getUserToken(getActivity());

        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        // System.out.println("API EmailId@@@"+email_id.getText().toString());
        Singlecasemodel singlecasemodel = new Singlecasemodel();
        singlecasemodel.setUserid(userid);
        Call<ResponseBody> resultCall = loadInterface.get_profile_detail(userid,token,singlecasemodel);
        resultCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                System.out.println("Status Response is!!!"+response);
                if (response.isSuccessful()) {
                    try {
                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {
                            JSONObject jsonobject1 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonobject1);
                            String status22 = jsonobject1.getString("error");
                            System.out.println("Status is@@@"+status22);

                            if (status22.equalsIgnoreCase("false")) {
                                loader.setVisibility(View.GONE);
                                String usersDetails=jsonobject1.getString("usersDetails");
                                System.out.println("ResultGetProfile***"+usersDetails);
                                JSONObject jsonObject=new JSONObject(usersDetails);


                                Picasso.with(getActivity())
                                        .load(jsonObject.getString("fullimage"))
                                        .placeholder(R.drawable.progress_animation2)
                                        .error(getResources().getDrawable(R.drawable.nouserimg))
                                        .into(profile_icon);
                            }
                            else   if (status22.equalsIgnoreCase("true")) {
                                loader.setVisibility(View.GONE);
                                String message = jsonobject1.getString("errorMessage");
                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loader.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "Veuillez vérifier la connexion ", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v==rr_profil)
        {

            if(loggedin!=null && !loggedin.equalsIgnoreCase("")  &&
                        !loggedin.equalsIgnoreCase("null")) {
                Intent ii = new Intent(getActivity(), ProfileActivity.class);
                startActivity(ii);
            }

            else
            {
                Intent ii = new Intent(getActivity(), LoginActivity.class);
                startActivity(ii);
            }
        }
        if(v==rr_mode_de_paiement)
        {
            if(loggedin!=null && !loggedin.equalsIgnoreCase("")  &&
                    !loggedin.equalsIgnoreCase("null")) {

                Intent ii = new Intent(getActivity(), Mode_De_Paiement_Activity.class);
                startActivity(ii);
            }
            else
            {
                Intent ii = new Intent(getActivity(), LoginActivity.class);
                startActivity(ii);
            }

        }
        if(v==rr_historique_des_commandes)
        {
            if(loggedin!=null && !loggedin.equalsIgnoreCase("")  &&
                    !loggedin.equalsIgnoreCase("null")) {
                Intent ii = new Intent(getActivity(), Historique_Des_Commandes_Activities.class);
                startActivity(ii);
            }
            else
            {
                Intent ii = new Intent(getActivity(), LoginActivity.class);
                startActivity(ii);
            }
        }

        if(v==rr_myorders)
        {
            if(loggedin!=null && !loggedin.equalsIgnoreCase("")  &&
                    !loggedin.equalsIgnoreCase("null")) {
                Intent ii = new Intent(getActivity(), MyOrders.class);
                startActivity(ii);
            }
            else
            {
                Intent ii = new Intent(getActivity(), LoginActivity.class);
                startActivity(ii);
            }
        }
        if(v==rr_soutien)
        {
            if(loggedin!=null && !loggedin.equalsIgnoreCase("")  &&
                    !loggedin.equalsIgnoreCase("null")) {
                Intent ii = new Intent(getActivity(), Soutien_Activity.class);
                startActivity(ii);
            }
            else
            {
                Intent ii = new Intent(getActivity(), LoginActivity.class);
                startActivity(ii);
            }
        }

        if(v==rr_gérer_l_adresse)
        {
            if(loggedin!=null && !loggedin.equalsIgnoreCase("")  &&
                    !loggedin.equalsIgnoreCase("null")) {
                Intent ii = new Intent(getActivity(), Add_Gererladdress_Delivery_Address_Activity.class);
                startActivity(ii);
            }
            else
            {
                Intent ii = new Intent(getActivity(), LoginActivity.class);
                startActivity(ii);
            }
        }

        if(v==rr_partager_l_application)
        {
            if(loggedin!=null && !loggedin.equalsIgnoreCase("")  &&
                    !loggedin.equalsIgnoreCase("null")) {
          //      Intent ii = new Intent(getActivity(), Partage_Lapplication_Activity.class);
//                startActivity(ii);
                shareApp();
            }
            else
            {
                Intent ii = new Intent(getActivity(), LoginActivity.class);
                startActivity(ii);
            }
        }
        if(v==rr_se_déconnecter)
        {
   showDialogforCancelCart();

        }

    }
    public void shareApp()
    {

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        // Add data to the intent, the receiving app will decide
        // what to do with it.
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "MEETECH");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.meetch");
        startActivity(Intent.createChooser(sharingIntent, "Partager via"));
    }

    public  void showDialogforCancelCart() {

        dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dlg_cacl_cart);
        CustomTextViewRegular  txt_dlte_cart=(CustomTextViewRegular)dialog.findViewById(R.id.txt_dlte_cart);

        txt_dlte_cart.setText(getResources().getString(R.string.txt_logout));

      LinearLayout  ll_ok=(LinearLayout)dialog.findViewById(R.id.ll_ok);
      LinearLayout  ll_cancel=(LinearLayout)dialog.findViewById(R.id.ll_cancel);

        ll_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                sp.saveData(getActivity(),"ldata","");
                sp.saveData(getActivity(),"token","");
                sp.saveData(getActivity(),"user_id","");
                sp.saveData(getActivity(),"email","");

                Intent ii=new Intent(getActivity(), LoginActivity.class);
                startActivity(ii);

            }
        });
        ll_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

}
