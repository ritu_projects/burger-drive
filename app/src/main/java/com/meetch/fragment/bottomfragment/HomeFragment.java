package com.meetch.fragment.bottomfragment;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.Gson;
import com.meetch.R;
import com.meetch.activity.Cart_PageActivity;
import com.meetch.activity.categorylist.CategoriesList_Activity;
import com.meetch.adapter.Best_OfferAdapter;
import com.meetch.adapter.MyPager;
import com.meetch.adapter.BurgerAdapter;
import com.meetch.bean.banner_model.Banner_Model;
import com.meetch.bean.category_allproduct_model.GetProduct_Bean;
import com.meetch.bean.comboList.ComboList_Bean;
import com.meetch.bean.getProduct_model.ProductModel;
import com.meetch.bean.productbean_model.ProductBeanModel;
import com.meetch.bean.singlecasedeatial.Singlecasemodel;
import com.meetch.constant.Constants;
import com.meetch.constant.GetUserId;
import com.meetch.constant.Utils;
import com.meetch.gpstracker.GPSTracker;
import com.meetch.rectrofit.LoadInterface;
import com.meetch.storage.MySharedPref;
import com.meetch.utils.CustomTextView;
import com.meetch.utils.CustomTextViewRegular;
import com.yahoo.mobile.client.android.util.rangeseekbar.RangeSeekBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class HomeFragment extends Fragment implements View.OnClickListener {

    private MyPager myPager;
ViewPager viewPager;
    private int dotscount;
    private ImageView[] dots;
RecyclerView best_offer_recyclerView,the_burger_recyclerView,trierpar_recyclerView,category_recyclerView;
    Best_OfferAdapter best_offerAdapter;
    GetProduct_Bean getProduct_bean,getProduct_bean2;
    Banner_Model banner_model;
ComboList_Bean comboList_bean;
    ArrayList<String> arrayList=new ArrayList<>();
    ArrayList<String> arrayList2=new ArrayList<>();

    BurgerAdapter _burgerAdapter;
    RelativeLayout rr_for_add_tocart,rr_filter;
    Dialog filter_dialog;
    LinearLayoutManager linearLayoutManager;
    FilterHomeAdp filterHomeAdp;
    Button btn_appliquer;
    ProgressBar loader;
    ProductBeanModel productBeanModell;
    String cat_name,cat_price,loggedin,current_address;
    MySharedPref sp=new MySharedPref();
    CustomTextView livre_address,min_price,max_price;
ProgressBar loader_filter;
Integer min_pricee,max_pricee;
Category_Adapter category_adapter;
RelativeLayout rr_search;
CustomTextView txt_price;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.home_fragment, container, false);

        getLatlongGps();

        init(v);
        return v;
    }

    private void getLatlongGps() {
        GPSTracker gps = new GPSTracker(getActivity());
        double latitude = gps.getLatitude();
        double longitude = gps.getLongitude();
        System.out.println("Latitude Home  "+latitude);
        System.out.println("Longitude Home "+longitude);
        getAddressfromLatlong(latitude,longitude);

    }

    public void  getAddressfromLatlong(double dub_latitude,double dub_logitude)
    {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(getActivity(), Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(dub_latitude, dub_logitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
try {
    current_address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

    String city = addresses.get(0).getLocality();
    String state = addresses.get(0).getAdminArea();
    String country = addresses.get(0).getCountryName();
    String postalCode = addresses.get(0).getPostalCode();
    String knownName = addresses.get(0).getFeatureName();
    System.out.println("Address is  "+current_address);
}
catch (IndexOutOfBoundsException e)
{
    System.out.println("Error is  "+e);
}

        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    private void init(View v) {


        category_recyclerView=(RecyclerView) v.findViewById(R.id.category_recyclerView);

         rr_for_add_tocart=(RelativeLayout)v.findViewById(R.id.rr_for_add_tocart);
        rr_filter=(RelativeLayout)v.findViewById(R.id.rr_filter);
        rr_search=(RelativeLayout)v.findViewById(R.id.rr_search);
        rr_search.setOnClickListener(this);

        txt_price=(CustomTextView)v.findViewById(R.id.txt_price);



            loader=(ProgressBar)v.findViewById(R.id.loader);
        livre_address=(CustomTextView)v.findViewById(R.id.livre_address);
        arrayList.add("Ram");
        arrayList.add("");
        arrayList.add("");
        arrayList.add("");
        arrayList.add("");
        arrayList.add("");

        arrayList2.add("Le plus populaire");
        arrayList2.add("Prix croissant");
        arrayList2.add("Prix décroissant");
        arrayList2.add("Le plus proche de moi");


        rr_filter.setOnClickListener(this);
        rr_for_add_tocart.setVisibility(GONE);
        rr_for_add_tocart.setOnClickListener(this);

        callWebService(v);
        checkLoginornot(v);



    }

    private void checkLoginornot(View view) {

        loggedin=sp.getData(getActivity(),"ldata","null");

        if(loggedin!=null && !loggedin.equalsIgnoreCase("")  &&
                !loggedin.equalsIgnoreCase("null")) {



            livre_address.setText(current_address);


             callWebServiceforCartDetails(view);

        }
        else
        {
            livre_address.setText(current_address);
        }
    }

    public void callWebServiceforCartDetails(View view)
    {
        if(Utils.isConnected(getActivity()))
        {

            try {
              //  getDeliveryAddress(view);
                getCartDetails();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }

    public  void getDeliveryAddress(final View view)
    {

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
      String  userid= GetUserId.getUserInfo(getActivity());


        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        Singlecasemodel singlecasemodel = new Singlecasemodel();
        singlecasemodel.setUserid(userid);

        Call<ResponseBody> resultCall = loadInterface.getallDeliveryAddress(singlecasemodel);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {

                                JSONArray jsonArray = jsonObject22.getJSONArray("record");


                               JSONObject jsonObject33 = jsonArray.getJSONObject(0);

                               String address=jsonObject33.getString("address");

                                livre_address.setText(address);



                            }

                            else
                            {
                                livre_address.setText(current_address);

                                //txt_no_cas_outers.setVisibility(VISIBLE);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });

    }

    public void callWebService(View view)

    {
        if(Utils.isConnected(getActivity()))
        {

            try {

                getBanner(view);
                getComboList(view);
                getCategoryList(view);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }
    private void getCartDetails() {

        String userid= GetUserId.getUserInfo(getActivity());

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        Singlecasemodel singlecasemodel = new Singlecasemodel();
        singlecasemodel.setUserid(userid);
        Call<ResponseBody> resultCall = loadInterface.getUserCart(singlecasemodel);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status ResponseCart is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is Cart@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {

                                rr_for_add_tocart.setVisibility(VISIBLE);
                                if(jsonObject22.getString("final_price")!=null &&
                                        !jsonObject22.getString("final_price").equalsIgnoreCase("") &&
                                        !jsonObject22.getString("final_price").equalsIgnoreCase("null")
                                )
                                {
                                    txt_price.setText(jsonObject22.getString("final_price")+"€");

                                }


                            }
                        else
                            {
                                rr_for_add_tocart.setVisibility(GONE);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });



    }

    private void getComboList(final View view) {
        loader.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        LoadInterface loadInterface = retrofit.create(LoadInterface.class);

        Call<ResponseBody> resultCall = loadInterface.get_ComboList();
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {

                                Gson gson = new Gson();
                                comboList_bean = gson.fromJson(responedata, ComboList_Bean.class);
                                the_burger_recyclerView=(RecyclerView)view.findViewById(R.id.special_burger_recyclerView);
                                _burgerAdapter =new BurgerAdapter(getActivity(),comboList_bean.getList());

                                the_burger_recyclerView.setVisibility(View.VISIBLE);
                                the_burger_recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
                                the_burger_recyclerView.setAdapter(_burgerAdapter);

                                the_burger_recyclerView.setNestedScrollingEnabled(false);




                            }

                            else
                            {
                                //txt_no_cas_outers.setVisibility(VISIBLE);
                                category_recyclerView.setVisibility(GONE);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });


    }

    private void getCategoryList(final View view) {

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        LoadInterface loadInterface = retrofit.create(LoadInterface.class);

        Call<ResponseBody> resultCall = loadInterface.get_Category();
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {


                                Gson gson = new Gson();
                                getProduct_bean = gson.fromJson(responedata, GetProduct_Bean.class);
                                category_recyclerView.setVisibility(VISIBLE);
                                category_recyclerView.setHasFixedSize(true);
                              LinearLayoutManager  linearLayoutManager2 = new LinearLayoutManager(getActivity());

                                category_recyclerView.setLayoutManager(linearLayoutManager2);


                                category_adapter = new Category_Adapter(getActivity(), getProduct_bean.getList());

                                category_recyclerView.setAdapter(category_adapter);
                                category_recyclerView.setNestedScrollingEnabled(false);




                            }

                            else
                            {
                                //txt_no_cas_outers.setVisibility(VISIBLE);
                                category_recyclerView.setVisibility(GONE);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });



    }


    private void getProductList(final RecyclerView best_offer_recyclerVieww, final String cat_namee,
                                final CustomTextViewRegular txt_no_products_available) {

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        ProductModel productBeanModel = new ProductModel();
        productBeanModel.setCat_name(cat_namee);
        productBeanModel.setPrice(cat_price);

        Call<ResponseBody> resultCall = loadInterface.get_Product(productBeanModel);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response Product is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response Product is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {

                                txt_no_products_available.setVisibility(GONE);
                                Gson gson = new Gson();

                                productBeanModell = gson.fromJson(responedata, ProductBeanModel.class);

                                best_offerAdapter=new Best_OfferAdapter(getActivity(),productBeanModell.getRecord()
                                        ,productBeanModell.getImageUrl());
                                 //   best_offer_recyclerView=(RecyclerView)view.findViewById(R.id.best_offer_recyclerView);
                                    best_offer_recyclerVieww.setVisibility(View.VISIBLE);
                                    best_offer_recyclerVieww.setAdapter(best_offerAdapter);
                      //          if(cat_namee.equalsIgnoreCase("Burger"))
//                                {
//                                    System.out.println("Status Response Product is@@@"+productBeanModell.getRecord().getBurger().size());
//
//                                    best_offerAdapter=new Best_OfferAdapter(getActivity(),productBeanModell.getRecord().getBurger(),productBeanModell.getImageUrl());
//                                 //   best_offer_recyclerView=(RecyclerView)view.findViewById(R.id.best_offer_recyclerView);
//                                    best_offer_recyclerVieww.setVisibility(View.VISIBLE);
//                                    best_offer_recyclerVieww.setAdapter(best_offerAdapter);
//                                }
//
//                             else    if(cat_namee.equalsIgnoreCase("BOISSONS"))
//                                {
//                                    System.out.println("Status Response Product is@@@"+productBeanModell.getRecord().getBoisson().size());
//
//                                    best_offerAdapter=new Best_OfferAdapter(getActivity(),productBeanModell.getRecord().getBoisson(),productBeanModell.getImageUrl());
//                              //      best_offer_recyclerView=(RecyclerView)view.findViewById(R.id.best_offer_recyclerView);
//                                    best_offer_recyclerVieww.setVisibility(View.VISIBLE);
//                                    best_offer_recyclerVieww.setAdapter(best_offerAdapter);
//                                }
//                             else    if(cat_namee.equalsIgnoreCase("DESSERT"))
//                                {
//                                    System.out.println("Status Response Product is@@@"+productBeanModell.getRecord().getDESSERT().size());
//
//                                    best_offerAdapter=new Best_OfferAdapter(getActivity(),productBeanModell.getRecord().getDESSERT(),productBeanModell.getImageUrl());
//                                  //  best_offer_recyclerView=(RecyclerView)view.findViewById(R.id.best_offer_recyclerView);
//                                    best_offer_recyclerVieww.setVisibility(View.VISIBLE);
//                                    best_offer_recyclerVieww.setAdapter(best_offerAdapter);
//                                }
//

                            }

                            else
                            {
                                best_offer_recyclerVieww.setVisibility(GONE);
                                txt_no_products_available.setVisibility(VISIBLE);
                                //txt_no_cas_outers.setVisibility(VISIBLE);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });



    }


    private void getCategory(final View view) {

        loader_filter.setVisibility(View.VISIBLE);

            Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            LoadInterface loadInterface = retrofit.create(LoadInterface.class);

         Call<ResponseBody> resultCall = loadInterface.get_Category();
            resultCall.enqueue(new Callback<ResponseBody>() {

                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                    loader_filter.setVisibility(View.GONE);

                    System.out.println("Status Response is!!!"+response);

                    if (response.isSuccessful()) {

                        try {

                            String responedata = response.body().string();
                            System.out.println("Status Response is@@@"+responedata);
                            try {

                                JSONObject jsonObject22 = new JSONObject(responedata);
                                System.out.println("Status is@@@"+jsonObject22);

                                String  status = jsonObject22.getString("error");

                                if (status.equalsIgnoreCase("false")) {


                                    Gson gson = new Gson();
                                    getProduct_bean = gson.fromJson(responedata, GetProduct_Bean.class);
                                    filterHomeAdp = new FilterHomeAdp(getActivity(), getProduct_bean.getList());

                                    trierpar_recyclerView.setAdapter(filterHomeAdp);

//                                    best_offerAdapter=new Best_OfferAdapter(getActivity(),getProduct_bean.getList());
//                                    best_offer_recyclerView=(RecyclerView)view.findViewById(R.id.best_offer_recyclerView);
//                                    best_offer_recyclerView.setVisibility(View.VISIBLE);
//                                    best_offer_recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
//                                    best_offer_recyclerView.setAdapter(best_offerAdapter);


                                }

                                else
                                {
                               //     best_offer_recyclerView.setVisibility(GONE);
                                    //txt_no_cas_outers.setVisibility(VISIBLE);

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
//
                    } else {
                    }
//

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    loader_filter.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


                }
            });



    }



    private void getBanner(final View view) {

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        LoadInterface loadInterface = retrofit.create(LoadInterface.class);

        Call<ResponseBody> resultCall = loadInterface.get_Banner();
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {


                                Gson gson = new Gson();
                                banner_model = gson.fromJson(responedata, Banner_Model.class);
                                myPager = new MyPager(getActivity(),banner_model.getRecord());

                                viewPager = view.findViewById(R.id.view_pager);
                                viewPager.setAdapter(myPager);

                                dotscount = myPager.getCount();
                                dots = new ImageView[dotscount];
                                LinearLayout sliderDotspanel;
                                sliderDotspanel = (LinearLayout) view.findViewById(R.id.SliderDots);

                                for(int i = 0; i < dotscount; i++){

                                    dots[i] = new ImageView(getActivity());
                                    dots[i].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.nonactive_dot));

                                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                                    params.setMargins(8, 0, 8, 0);

                                    sliderDotspanel.addView(dots[i], params);

                                }

                                dots[0].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.active_doc_red));

                                viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                                    @Override
                                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                                    }

                                    @Override
                                    public void onPageSelected(int position) {

                                        System.out.println("dot position:::::"+position);;


                                        for(int i = 0; i< dotscount; i++){
                                            //  dots[i].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.nonactive_dot));
                                        }
                                        if(position==0){
                                            // dots[position].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.active_doc_red));
                                        }else if(position==1){
                                            // dots[position].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.active_dot_yellow));
                                        }else if(position==2){
                                            // dots[position].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.active_doc_green));
                                        }
                                        else if(position==3){
                                            // dots[position].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.active_dot_pink));
                                        }



                                    }

                                    @Override
                                    public void onPageScrollStateChanged(int state) {

                                    }
                                });



                            }

                            else
                            {
                                best_offer_recyclerView.setVisibility(GONE);
                                //txt_no_cas_outers.setVisibility(VISIBLE);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });



    }


    public void open_Filterdialog(final View v)
    {
        filter_dialog=new Dialog(getActivity());

        filter_dialog.getWindow().getAttributes().windowAnimations = R.style.MyAlertDialogStyle;
        // Include dialog.xml file
        filter_dialog.setContentView(R.layout.filter_layout);
        filter_dialog.show();
        trierpar_recyclerView=(RecyclerView)filter_dialog.findViewById(R.id.trierpar_recyclerView);
        loader_filter=(ProgressBar)filter_dialog.findViewById(R.id.loader_filter);
        btn_appliquer=(Button)filter_dialog.findViewById(R.id.btn_appliquer);
   RangeSeekBar rr_range_prix=(RangeSeekBar)filter_dialog.findViewById(R.id.rr_range_prix);

        min_price=(CustomTextView)filter_dialog.findViewById(R.id.min_price);
         max_price=(CustomTextView)filter_dialog.findViewById(R.id.max_price);

        rr_range_prix.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
                min_price.setText(minValue+"");
                max_price.setText(maxValue+"");
                min_pricee=minValue;
                max_pricee=maxValue;

            }
        });
        cat_price=min_pricee+"-"+max_pricee;


   trierpar_recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getActivity());

        trierpar_recyclerView.setLayoutManager(linearLayoutManager);
        getCategory(v);


        btn_appliquer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                filter_dialog.dismiss();
                callWebService(v);
            }
        });



    }



    @Override
    public void onClick(View v) {

        if(v==rr_filter)
        {
            open_Filterdialog(v);

        }
        if(v==rr_search)
        {
            Intent ii=new Intent(getActivity(), CategoriesList_Activity.class);
            startActivity(ii);
        }
        if(v==rr_for_add_tocart)
        {
            Intent ii=new Intent(getActivity(), Cart_PageActivity.class);
            startActivity(ii);
        }

    }

/*
    private void gethome_API(final View v) {

        loader.setVisibility(View.VISIBLE);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder().addHeader("Authorization", "Bearer  affg").build();
                return chain.proceed(request);
            }
        });

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();
        LoadInterface loadInterface = retrofit.create(LoadInterface.class);



        ProfileModel profileModel = new ProfileModel();
        profileModel.setUserid(user_id);


        Call<ResponseBody> resultCall = loadInterface.doctor_getCases(user_id,token,profileModel);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String   status = jsonObject22.getString("status");

                            if (status.equalsIgnoreCase("200")) {

                                txt_no_cas_outers.setVisibility(GONE);

                                Gson gson = new Gson();
                                getCarList = gson.fromJson(responedata, GetCarList.class);
                                List<String> myList = null;



                                casOutersItemAdapter = new CasOutersItemAdapter(getActivity(),getCarList.getResult());
                                recyclerview_casouverts.setAdapter(casOutersItemAdapter);
                                recyclerview_casouverts.setLayoutManager(linearLayoutManager);
                            }

                            else
                            {
                                ll_recyclerview_casouverts.setVisibility(GONE);
                                txt_no_cas_outers.setVisibility(VISIBLE);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });

    }
*/

    public class FilterHomeAdp extends RecyclerView.Adapter<FilterHomeAdp.ViewHolder> {
        Context context;
        List<GetProduct_Bean.GetProductDescp_Bean> arrayList;

        public FilterHomeAdp(Activity homeActivity, List<GetProduct_Bean.GetProductDescp_Bean> arrayList) {
            this.context=homeActivity;
            this.arrayList=arrayList;
        }

        @NonNull
        @Override
        public FilterHomeAdp.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

            View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.filter_item_layout,viewGroup,false);
 ViewHolder viewHolder=new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull final FilterHomeAdp.ViewHolder viewHolder, final int i) {
        viewHolder.filter_cat_name.setText(arrayList.get(i).getName());
cat_name=arrayList.get(i).getName();
            viewHolder.filter_cat_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cat_name=arrayList.get(i).getName();

                }
            });
           viewHolder.radio_filter.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
            {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if(viewHolder.rado_check.isChecked())
                    {
                        viewHolder.rado_check.setChecked(true);

                    }
                    else
                    {
                        viewHolder.rado_check.setChecked(false);

                    }

                }
            });


        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            CustomTextViewRegular filter_cat_name;
            RadioButton rado_check;
            RadioGroup radio_filter;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                filter_cat_name=(CustomTextViewRegular)itemView.findViewById(R.id.filter_cat_name);
                rado_check=(RadioButton)itemView.findViewById(R.id.rado_check);
                radio_filter=(RadioGroup)itemView.findViewById(R.id.radio_filter);
            }
        }
    }
    public class Category_Adapter extends RecyclerView.Adapter<Category_Adapter.ViewHolder> {
        Context context;
        List<GetProduct_Bean.GetProductDescp_Bean> arrayList;


        public Category_Adapter(Activity homeActivity, List<GetProduct_Bean.GetProductDescp_Bean> list) {

            this.context = homeActivity;
            this.arrayList = list;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.home_item, viewGroup, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
            viewHolder.txt_category_name.setText(arrayList.get(i).getName());
            getProductList(viewHolder.best_offer_recyclerView, arrayList.get(i).getName(),viewHolder.txt_no_products_available);

        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            CustomTextView txt_category_name;
            CustomTextViewRegular txt_no_products_available;
          RecyclerView  best_offer_recyclerView;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                txt_category_name = (CustomTextView) itemView.findViewById(R.id.txt_category_name);
                best_offer_recyclerView=(RecyclerView)itemView.findViewById(R.id.best_offer_recyclerView);
                best_offer_recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                txt_no_products_available=(CustomTextViewRegular)itemView.findViewById(R.id.txt_no_products_available);


            }
        }
    }
}
