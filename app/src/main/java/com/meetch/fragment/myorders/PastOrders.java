package com.meetch.fragment.myorders;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.meetch.R;
import com.meetch.activity.OrderDetails;
import com.meetch.bean.allPastOrdersBean.AllPastOrdersBean;
import com.meetch.bean.singlecasedeatial.Singlecasemodel;
import com.meetch.constant.Constants;
import com.meetch.constant.GetUserId;
import com.meetch.constant.Utils;
import com.meetch.picasso.PicassoTrustAll;
import com.meetch.rectrofit.LoadInterface;
import com.meetch.utils.CustomTextView;
import com.meetch.utils.CustomTextViewRegular;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PastOrders extends Fragment {

    private RecyclerView current_list_rcyle;
    private PastOrders_ItemAdapter pastOrders_itemAdapter;
    private ArrayList<String> arrayList=new ArrayList<>();
    AllPastOrdersBean allOrdersModel;
    ProgressBar loader;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.currentorder_frag, container, false);

        init(v);
        return v;
    }

    private void init(View v) {

        current_list_rcyle=(RecyclerView)v.findViewById(R.id.current_list_rcyle);
        loader=(ProgressBar)v.findViewById(R.id.loader);

        callWebService(v);
    }

    public void callWebService(View view)
    {
        if(Utils.isConnected(getActivity()))
        {

            try {
                getPastOrders(view);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }


    public  void getPastOrders(final View view)
    {

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        String  userid= GetUserId.getUserInfo(getActivity());
        String  usertoken= GetUserId.getUserToken(getActivity());

        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        Singlecasemodel singlecasemodel = new Singlecasemodel();
        singlecasemodel.setUserid(userid);

        Call<ResponseBody> resultCall = loadInterface.userpastorders(userid,usertoken,singlecasemodel);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {

                                Gson gson = new Gson();
                                allOrdersModel = gson.fromJson(responedata, AllPastOrdersBean.class);

                                pastOrders_itemAdapter=new PastOrders_ItemAdapter(getActivity(),allOrdersModel.getDetail());
                                current_list_rcyle.setLayoutManager(new LinearLayoutManager(getActivity()));
                                current_list_rcyle.setAdapter(pastOrders_itemAdapter);
                                current_list_rcyle.setNestedScrollingEnabled(true);



                            }

                            else
                            {
                                current_list_rcyle.setVisibility(View.GONE);


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });

    }
    public class PastOrders_ItemAdapter extends RecyclerView.Adapter<PastOrders_ItemAdapter.ViewHolder> {
        Context context;
        List<AllPastOrdersBean.Detail> list;
        String img_base_url;

        public PastOrders_ItemAdapter(Activity activity, List<AllPastOrdersBean.Detail> detail) {
            this.context=activity;
            this.list=detail;
         //   this.img_base_url=image_path;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.currentorders_item,viewGroup,false);
           ViewHolder viewHolder=new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
            PicassoTrustAll.getInstance(context)
                    .load(list.get(i).getProdImg())
                  //  .resize(R.dimen.mrg100,R.dimen.mrg100)
                    .error(R.drawable.bnanner)
                    .into(viewHolder.item_image);

            viewHolder.order_id.setText(getResources().getString(R.string.order_id)+": " +list.get(i).getOrderId());
            viewHolder.product_name.setText(getResources().getString(R.string.order_date)+": "+list.get(i).getOrderDate());
            viewHolder.deliver_date.setText(getResources().getString(R.string.delivery_date)+": "+list.get(i).getInitiatedAt());
            viewHolder.product_price.setText(getResources().getString(R.string.cart_total)+": "+list.get(i).getTotal()+"€");

            viewHolder.view_order.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().finish();

                    Intent ii=new Intent(context, OrderDetails.class);
                    ii.putExtra("order_id",list.get(i).getOrderId());
                    startActivity(ii);
                }
            });

        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            LinearLayout view_order,view_tracking;
            ImageView item_image;
            CustomTextView order_id;
            CustomTextViewRegular product_name,product_price,deliver_date;

            public  ViewHolder(View view){
                super(view);
                view_tracking=(LinearLayout)view.findViewById(R.id.view_tracking);
                view_tracking.setVisibility(View.GONE);
                view_order=(LinearLayout)view.findViewById(R.id.view_order);
                item_image=(ImageView)view.findViewById(R.id.item_image);
                order_id=(CustomTextView)view.findViewById(R.id.order_id);
                product_name=(CustomTextViewRegular)view.findViewById(R.id.product_name);
                product_price=(CustomTextViewRegular)view.findViewById(R.id.product_price);
                deliver_date=(CustomTextViewRegular)view.findViewById(R.id.deliver_date);
            }

        }
    }

}
