package com.meetch.fragment.myorders;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.meetch.R;
import com.meetch.activity.OrderDetails;
import com.meetch.activity.Suivre_TrackOrder_Activity;
import com.meetch.bean.all_orders_model.AllOrdersModel;
import com.meetch.bean.singlecasedeatial.Singlecasemodel;
import com.meetch.constant.Constants;
import com.meetch.constant.GetUserId;
import com.meetch.constant.Utils;
import com.meetch.picasso.PicassoTrustAll;
import com.meetch.rectrofit.LoadInterface;
import com.meetch.utils.CustomTextView;
import com.meetch.utils.CustomTextViewRegular;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CurrentOrders extends Fragment {

    private RecyclerView current_list_rcyle;
    private TextView title;
    private CurrentOrders_ItemAdapter currentOrders_itemAdapter;
    ProgressBar loader;
    AllOrdersModel allOrdersModel;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.currentorder_frag, container, false);

        init(v);
        return v;
    }

    private void init(View v) {
    //    arrayList.add("");
        current_list_rcyle=(RecyclerView)v.findViewById(R.id.current_list_rcyle);
        loader=(ProgressBar)v.findViewById(R.id.loader);

        callWebService(v);

    }
    public void callWebService(View view)
    {
        if(Utils.isConnected(getActivity()))
        {

            try {
                getCurrentOrders(view);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }


    public  void getCurrentOrders(final View view)
    {

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        String  userid= GetUserId.getUserInfo(getActivity());
        String  usertoken= GetUserId.getUserToken(getActivity());

        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        Singlecasemodel singlecasemodel = new Singlecasemodel();
        singlecasemodel.setUserid(userid);

        Call<ResponseBody> resultCall = loadInterface.userCurrentorders(userid,usertoken,singlecasemodel);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {

                                Gson gson = new Gson();
                                allOrdersModel = gson.fromJson(responedata, AllOrdersModel.class);

                                currentOrders_itemAdapter=new CurrentOrders_ItemAdapter(getActivity(),allOrdersModel.getDetail(),jsonObject22.getString("image_path"));
                                current_list_rcyle.setLayoutManager(new LinearLayoutManager(getActivity()));
                                current_list_rcyle.setAdapter(currentOrders_itemAdapter);
                                current_list_rcyle.setNestedScrollingEnabled(false);



                            }

                            else
                            {
                                current_list_rcyle.setVisibility(View.GONE);


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });

    }
    public class CurrentOrders_ItemAdapter extends RecyclerView.Adapter<CurrentOrders_ItemAdapter.ViewHolder> {
        Context context;
        List<AllOrdersModel.Detail> list;
        String img_base_url;

        public CurrentOrders_ItemAdapter(Activity favoritesActivity, List<AllOrdersModel.Detail> arrayList) {
            this.context=favoritesActivity;
            this.list=arrayList;
        }

        public CurrentOrders_ItemAdapter(Activity activity, List<AllOrdersModel.Detail> detail, String image_path) {
            this.context=activity;
            this.list=detail;
            this.img_base_url=image_path;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.currentorders_item,viewGroup,false);
          ViewHolder viewHolder=new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull CurrentOrders_ItemAdapter.ViewHolder viewHolder, final int i) {

            PicassoTrustAll.getInstance(context)
                    .load(img_base_url+list.get(i).getImage())
                  //  .resize(100,100)
                    .error(R.drawable.bnanner)
                    .into(viewHolder.item_image);

            viewHolder.order_id.setText(getResources().getString(R.string.order_id)+": " +list.get(i).getOrderId());
            viewHolder.product_name.setText(getResources().getString(R.string.order_date)+": "+list.get(i).getCreatedAt());
            if(list.get(i).getTimeslot()!=null && !list.get(i).getTimeslot().equalsIgnoreCase("") && !list.get(i).getTimeslot().equalsIgnoreCase("null")) {
                viewHolder.deliver_date.setText(getResources().getString(R.string.delivery_date) + ": " + list.get(i).getOrderDate() + " " + list.get(i).getTimeslot());
            }
            else
            {
                viewHolder.deliver_date.setText(getResources().getString(R.string.delivery_date) + ": " + list.get(i).getOrderDate());

            }
            viewHolder.product_price.setText(getResources().getString(R.string.cart_total)+": "+list.get(i).getTotal()+"€");

            viewHolder.view_tracking.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(list.get(i).getStatus()==2 || list.get(i).getStatus()==5)
                    {
                        getActivity().finish();


                        Intent ii=new Intent(getActivity(), Suivre_TrackOrder_Activity.class);
                ii.putExtra("order_id",list.get(i).getOrderId()) ;
                startActivity(ii);
                    }
                    else if(list.get(i).getStatus()==0)
                    {
                        Toast.makeText(getActivity(),R.string.order_not_assignby_anydriver,Toast.LENGTH_SHORT).show();

                    }
                    else if(list.get(i).getStatus()==1)
                    {
                        Toast.makeText(getActivity(),R.string.order_not_acceptedby_anydriver,Toast.LENGTH_SHORT).show();

                    }
                    else if(list.get(i).getStatus()==3)
                    {
                        Toast.makeText(getActivity(),R.string.order_deliveredby_anydriver,Toast.LENGTH_SHORT).show();

                    }
                    else if(list.get(i).getStatus()==4)
                    {
                        Toast.makeText(getActivity(),R.string.order_rejectedby_anydriver,Toast.LENGTH_SHORT).show();

                    }


                }
            });
            viewHolder.view_order.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    getActivity().finish();
                    Intent ii=new Intent(getActivity(), OrderDetails.class);
                    ii.putExtra("order_id",list.get(i).getOrderId());
                    startActivity(ii);
                }
            });




        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            LinearLayout view_order,view_tracking;
            ImageView item_image;
            CustomTextView order_id;
            CustomTextViewRegular product_name,product_price,deliver_date;

            public  ViewHolder(View view){
                super(view);
                view_order=(LinearLayout)view.findViewById(R.id.view_order);
                item_image=(ImageView)view.findViewById(R.id.item_image);
                order_id=(CustomTextView)view.findViewById(R.id.order_id);
                product_name=(CustomTextViewRegular)view.findViewById(R.id.product_name);
                product_price=(CustomTextViewRegular)view.findViewById(R.id.product_price);
                deliver_date=(CustomTextViewRegular)view.findViewById(R.id.deliver_date);
                view_tracking=(LinearLayout)view.findViewById(R.id.view_tracking);

            }

        }
    }


}
