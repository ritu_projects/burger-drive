package com.meetch.fragment;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.meetch.R;

public class ChildFragment4 extends Fragment implements View.OnClickListener {
    LinearLayout ll_login;

    public ChildFragment4() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.child_fragment_4_layout, container, false);



        return rootView;
    }

    @Override
    public void onClick(View v) {

    }


}