package com.meetch.bean.faq_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FAQModel {

    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("faqs")
    @Expose
    private List<FaqDescp> faqs = null;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<FaqDescp> getFaqs() {
        return faqs;
    }

    public void setFaqs(List<FaqDescp> faqs) {
        this.faqs = faqs;
    }


public class FaqDescp {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("question")
    @Expose
    private String question;
    @SerializedName("answer")
    @Expose
    private String answer;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
}
