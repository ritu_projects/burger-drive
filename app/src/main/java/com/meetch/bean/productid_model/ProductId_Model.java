package com.meetch.bean.productid_model;

import com.google.gson.annotations.SerializedName;

public class ProductId_Model {

    @SerializedName("product_id")
    private String product_id;

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }




}
