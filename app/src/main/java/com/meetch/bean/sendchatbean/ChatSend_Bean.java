package com.meetch.bean.sendchatbean;

public class ChatSend_Bean {
    String sender_id,order_id,message_by,message;

    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getMessage_by() {
        return message_by;
    }

    public void setMessage_by(String message_by) {
        this.message_by = message_by;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
