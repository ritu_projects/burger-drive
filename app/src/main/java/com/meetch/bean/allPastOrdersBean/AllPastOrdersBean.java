package com.meetch.bean.allPastOrdersBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AllPastOrdersBean {
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;
    @SerializedName("detail")
    @Expose
    private List<Detail> detail = null;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<Detail> getDetail() {
        return detail;
    }

    public void setDetail(List<Detail> detail) {
        this.detail = detail;
    }

    public class Detail {

        @SerializedName("order_id")
        @Expose
        private String orderId;
        @SerializedName("delivery_address")
        @Expose
        private String deliveryAddress;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("tax")
        @Expose
        private String tax;
        @SerializedName("total")
        @Expose
        private String total;
        @SerializedName("payment_type")
        @Expose
        private String paymentType;
        @SerializedName("initiated_at")
        @Expose
        private String initiatedAt;
        @SerializedName("order_date")
        @Expose
        private String orderDate;
        @SerializedName("delivered_time")
        @Expose
        private String deliveredTime;
        @SerializedName("prodImg")
        @Expose
        private String prodImg;

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getDeliveryAddress() {
            return deliveryAddress;
        }

        public void setDeliveryAddress(String deliveryAddress) {
            this.deliveryAddress = deliveryAddress;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getTax() {
            return tax;
        }

        public void setTax(String tax) {
            this.tax = tax;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getPaymentType() {
            return paymentType;
        }

        public void setPaymentType(String paymentType) {
            this.paymentType = paymentType;
        }

        public String getInitiatedAt() {
            return initiatedAt;
        }

        public void setInitiatedAt(String initiatedAt) {
            this.initiatedAt = initiatedAt;
        }

        public String getOrderDate() {
            return orderDate;
        }

        public void setOrderDate(String orderDate) {
            this.orderDate = orderDate;
        }

        public String getDeliveredTime() {
            return deliveredTime;
        }

        public void setDeliveredTime(String deliveredTime) {
            this.deliveredTime = deliveredTime;
        }

        public String getProdImg() {
            return prodImg;
        }

        public void setProdImg(String prodImg) {
            this.prodImg = prodImg;
        }

    }
}
