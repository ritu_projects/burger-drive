package com.meetch.bean.ordertracking_bean;

import com.google.gson.annotations.SerializedName;

public class OrderTrackingBean {

    @SerializedName("order_id")
    private String order_id;


    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

}
