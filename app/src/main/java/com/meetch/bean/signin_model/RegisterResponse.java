package com.meetch.bean.signin_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RegisterResponse {

    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;
    @SerializedName("usersDetails")
    @Expose
    private List<RegisterUserDetails> usersDetails = null;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<RegisterUserDetails> getUsersDetails() {
        return usersDetails;
    }

    public void setUsersDetails(List<RegisterUserDetails> usersDetails) {
        this.usersDetails = usersDetails;
    }

}
