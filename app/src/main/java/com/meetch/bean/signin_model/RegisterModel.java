package com.meetch.bean.signin_model;

import com.google.gson.annotations.SerializedName;

public class RegisterModel {
    @SerializedName("username")
    private String username;


    @SerializedName("email")
    private String email;

    @SerializedName("password")
    private String password;

    @SerializedName("telephone")
    private String telephone;

    @SerializedName("deviceToken")
    private String device_token;



    @SerializedName("device_type")
    private String device_type;

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

}
