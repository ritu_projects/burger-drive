package com.meetch.bean.category_allproduct_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetProduct_Bean {

    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("list")
    @Expose
    private java.util.List<GetProductDescp_Bean> list = null;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public java.util.List<GetProductDescp_Bean> getList() {
        return list;
    }

    public void setList(java.util.List<GetProductDescp_Bean> list) {
        this.list = list;
    }

    public class GetProductDescp_Bean {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;

        @SerializedName("catImage")
        @Expose
        private String catImage;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getcatImage() {
            return catImage;
        }

        public void setcatImage(String catImage) {
            this.catImage = catImage;
        }
    }
}


