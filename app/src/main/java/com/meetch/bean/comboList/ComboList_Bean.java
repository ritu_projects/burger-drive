package com.meetch.bean.comboList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ComboList_Bean {
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("list")
    @Expose
    private java.util.List<ComboList_DetailsBean> list = null;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public java.util.List<ComboList_DetailsBean> getList() {
        return list;
    }

    public void setList(java.util.List<ComboList_DetailsBean> list) {
        this.list = list;
    }
    public class ComboList_DetailsBean {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;

        @SerializedName("price")
        @Expose
        private String price;

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }



        public String getComboImg() {
            return comboImg;
        }

        public void setComboImg(String comboImg) {
            this.comboImg = comboImg;
        }

        @SerializedName("comboImg")
        @Expose
        private String comboImg;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

}
