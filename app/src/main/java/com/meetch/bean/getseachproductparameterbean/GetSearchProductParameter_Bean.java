package com.meetch.bean.getseachproductparameterbean;

import com.google.gson.annotations.SerializedName;

public class GetSearchProductParameter_Bean {
    @SerializedName("product_name")
    private String product_name;

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }




}
