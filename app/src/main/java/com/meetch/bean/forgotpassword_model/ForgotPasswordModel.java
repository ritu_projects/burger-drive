package com.meetch.bean.forgotpassword_model;

import com.google.gson.annotations.SerializedName;

public class ForgotPasswordModel {

    @SerializedName("email")
    private String email;
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
