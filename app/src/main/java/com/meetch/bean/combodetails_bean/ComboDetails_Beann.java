package com.meetch.bean.combodetails_bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ComboDetails_Beann {


    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("list")
    @Expose
    private List<ComboDetailAll_Bean> list = null;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<ComboDetailAll_Bean> getList() {
        return list;
    }

    public void setList(List<ComboDetailAll_Bean> list) {
        this.list = list;
    }


    public class ComboDetailAll_Bean {

        @SerializedName("combo_name")
        @Expose
        private String comboName;

        @SerializedName("combo_id")
        @Expose
        private String comboId;
        @SerializedName("combo_price")
        @Expose
        private String comboPrice;
        @SerializedName("combo_desc")
        @Expose
        private String comboDesc;
        @SerializedName("comboImg")
        @Expose
        private String comboImg;
        @SerializedName("prodList")
        @Expose
        private List<ProdList> prodList = null;

        public String getComboName() {
            return comboName;
        }

        public void setComboName(String comboName) {
            this.comboName = comboName;
        }

        public String getComboPrice() {
            return comboPrice;
        }

        public void setComboPrice(String comboPrice) {
            this.comboPrice = comboPrice;
        }

        public String getComboDesc() {
            return comboDesc;
        }

        public void setComboDesc(String comboDesc) {
            this.comboDesc = comboDesc;
        }

        public String getComboImg() {
            return comboImg;
        }

        public void setComboImg(String comboImg) {
            this.comboImg = comboImg;
        }
        public String getComboId() {
            return comboId;
        }

        public void setComboId(String comboId) {
            this.comboId = comboId;
        }
        public List<ProdList> getProdList() {
            return prodList;
        }

        public void setProdList(List<ProdList> prodList) {
            this.prodList = prodList;
        }

        public class ProdList {

            @SerializedName("cat_name")
            @Expose
            private String catName;



            @SerializedName("cat_id")
            @Expose
            private String catId;

            @SerializedName("product_list")
            @Expose
            private List<ProductList> productList = null;

            public String getCatName() {
                return catName;
            }
            public String getCatId() {
                return catId;
            }

            public void setCatId(String catId) {
                this.catId = catId;
            }
            public void setCatName(String catName) {
                this.catName = catName;
            }

            public List<ProductList> getProductList() {
                return productList;
            }

            public void setProductList(List<ProductList> productList) {
                this.productList = productList;
            }

            public class ProductList {

                @SerializedName("name")
                @Expose
                private String name;
                @SerializedName("quantity")
                @Expose
                private Integer quantity;
                @SerializedName("prodId")
                @Expose
                private String prodId;



                public String getProdId() {
                    return prodId;
                }

                public void setProdId(String prodId) {
                    this.prodId = prodId;
                }



                public Integer individula_product_quant_byUser=0;


                public Integer getIndividula_product_quant_byUser() {
                    return individula_product_quant_byUser;
                }


                public void setIndividula_product_quant_byUser(Integer individula_product_quant_byUser) {
                    this.individula_product_quant_byUser = individula_product_quant_byUser;
                }



                public boolean isSelected;
                public boolean isSelected_plus;
                public boolean isSelected_minus;


                public boolean isSelected() {
                    return isSelected;
                }

                public void setSelected(boolean selected) {
                    isSelected = selected;
                }

                public boolean isSelected_plus() {
                    return isSelected_plus;
                }

                public void setSelected_plus(boolean selected_plus) {
                    isSelected_plus = selected_plus;
                }

                public boolean isSelected_minus() {
                    return isSelected_minus;
                }

                public void setSelected_minus(boolean selected_minus) {
                    isSelected_minus = selected_minus;
                }



                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public Integer getQuantity() {
                    return quantity;
                }

                public void setQuantity(Integer quantity) {
                    this.quantity = quantity;
                }
            }

        }
    }
}
