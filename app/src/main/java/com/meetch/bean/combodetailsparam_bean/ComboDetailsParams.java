package com.meetch.bean.combodetailsparam_bean;

import com.google.gson.annotations.SerializedName;

public class ComboDetailsParams {
    @SerializedName("combo_id")
    private String combo_id;

    public String getCombo_id() {
        return combo_id;
    }

    public void setCombo_id(String combo_id) {
        this.combo_id= combo_id;
    }

}
