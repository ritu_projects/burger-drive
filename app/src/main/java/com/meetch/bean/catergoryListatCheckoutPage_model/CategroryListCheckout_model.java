package com.meetch.bean.catergoryListatCheckoutPage_model;

public class CategroryListCheckout_model {
    String id;
    String cat_img;
    String cat_name;
    String cat_id;
    String c_code_name;
    String cat_prize;
    String cat_quantity;
    String cat_product_name;


    public String getCat_product_name() {
        return cat_product_name;
    }

    public void setCat_product_name(String cat_product_name) {
        this.cat_product_name = cat_product_name;
    }



    public String getCat_quantity() {
        return cat_quantity;
    }

    public void setCat_quantity(String cat_quantity) {
        this.cat_quantity = cat_quantity;
    }



    public String getCat_prize() {
        return cat_prize;
    }

    public void setCat_prize(String cat_prize) {
        this.cat_prize = cat_prize;
    }


    public String getC_code_name() {
        return c_code_name;
    }

    public void setC_code_name(String c_code_name) {
        this.c_code_name = c_code_name;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCat_img() {
        return cat_img;
    }

    public void setCat_img(String cat_img) {
        this.cat_img = cat_img;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }
    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

}
