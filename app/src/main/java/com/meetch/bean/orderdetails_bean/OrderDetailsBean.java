package com.meetch.bean.orderdetails_bean;

import com.google.gson.annotations.SerializedName;

public class OrderDetailsBean {
    @SerializedName("user_id")
    private String userid;

    @SerializedName("order_id")
    private String order_id;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }



    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }


}
