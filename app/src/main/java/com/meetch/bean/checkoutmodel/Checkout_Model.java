package com.meetch.bean.checkoutmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Checkout_Model {
    @SerializedName("user_id")
    @Expose
    private String user_id;

    @SerializedName("order_date")
    @Expose
    private String order_date;

    @SerializedName("order_time")
    @Expose
    private String order_time;

    @SerializedName("address_id")
    @Expose
    String address_id;

    @SerializedName("payment_type")
    @Expose
    String payment_type;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getOrder_time() {
        return order_time;
    }

    public void setOrder_time(String order_time) {
        this.order_time = order_time;
    }

    public String getAddress_id() {
        return address_id;
    }

    public void setAddress_id(String address_id) {
        this.address_id = address_id;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }


}
