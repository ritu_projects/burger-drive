package com.meetch.bean.combobean;

import com.meetch.bean.productListatCheckoutPage_model.ProductListatCheckoutPage_model;

import java.util.ArrayList;

public class ComboBean {
    Integer combo_id;
    String combo_price;
    String combo_img;
    String combo_name;
    ArrayList<ProductListatCheckoutPage_model>  productListatCheckoutPage_models;


    public Integer getCombo_id() {
        return combo_id;
    }

    public void setCombo_id(Integer combo_id) {
        this.combo_id = combo_id;
    }

    public String getCombo_img() {
        return combo_img;
    }

    public void setCombo_img(String combo_img) {
        this.combo_img = combo_img;
    }


    public String getCombo_price() {
        return combo_price;
    }

    public void setCombo_price(String combo_price) {
        this.combo_price = combo_price;
    }

    public String getCombo_name() {
        return combo_name;
    }

    public void setCombo_name(String combo_name) {
        this.combo_name = combo_name;
    }

    public ArrayList<ProductListatCheckoutPage_model> getProductListatCheckoutPage_models() {
        return productListatCheckoutPage_models;
    }

    public void setProductListatCheckoutPage_models(ArrayList<ProductListatCheckoutPage_model> productListatCheckoutPage_models) {
        this.productListatCheckoutPage_models = productListatCheckoutPage_models;
    }




}
