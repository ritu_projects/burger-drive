package com.meetch.bean.productbean_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductBeanModel {

    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;
    @SerializedName("record")
    @Expose
    private List<ProductModelDescp> record;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<ProductModelDescp> getRecord() {
        return record;
    }

    public void setRecord(List<ProductModelDescp> record) {
        this.record = record;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }



public class ProductModelDescp {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("cat_id")
    @Expose
    private String catId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("cat_name")
    @Expose
    private String catName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }


    public class Record {


        @SerializedName("Burger")
        @Expose
        private List<ProductModelDescp> burger = null;

        @SerializedName("BOISSONS")
        @Expose
        private List<ProductModelDescp> boisson = null;

        @SerializedName("DESSERT")
        @Expose
        private List<ProductModelDescp> dessert = null;

        public List<ProductModelDescp> getBurger() {
            return burger;
        }

        public void setBurger(List<ProductModelDescp> burger) {
            this.burger = burger;
        }
        public List<ProductModelDescp> getBoisson() {
            return boisson;
        }

        public void setBoisson(List<ProductModelDescp> boisson) {
            this.boisson = boisson;
        }

        public List<ProductModelDescp> getDESSERT() {
            return dessert;
        }

        public void setDessert(List<ProductModelDescp> dessert) {
            this.dessert = dessert;
        }
    }
}



}
