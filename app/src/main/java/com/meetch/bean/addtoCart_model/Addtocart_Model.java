package com.meetch.bean.addtoCart_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Addtocart_Model {


//    @SerializedName("address_id")
//    @Expose
//    private String address_id;

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("cartList")
    @Expose
    private List<CartList> cartList = null;

    @SerializedName("product_type")
    @Expose
    private String productType;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<CartList> getCartList() {
        return cartList;
    }

    public void setCartList(List<CartList> cartList) {
        this.cartList = cartList;
    }

//    public String getAddress_id() {
//        return address_id;
//    }
//
//
//    public void setAddress_id(String address_id) {
//        this.address_id = address_id;
//    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public static class CartList {

        @SerializedName("cat_id")
        @Expose
        private String catId;
        @SerializedName("product_id")
        @Expose
        private String productId;
        @SerializedName("price")
        @Expose
        private String price;
        @SerializedName("quantity")
        @Expose
        private String quantity;

        public String getCatId() {
            return catId;
        }

        public void setCatId(String catId) {
            this.catId = catId;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

    }


}
