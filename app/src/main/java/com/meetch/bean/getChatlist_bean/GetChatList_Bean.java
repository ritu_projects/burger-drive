package com.meetch.bean.getChatlist_bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetChatList_Bean {

    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;
    @SerializedName("DriverOrderChat")
    @Expose
    private List<DriverOrderChat> driverOrderChat = null;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<DriverOrderChat> getDriverOrderChat() {
        return driverOrderChat;
    }

    public void setDriverOrderChat(List<DriverOrderChat> driverOrderChat) {
        this.driverOrderChat = driverOrderChat;
    }

    public class DriverOrderChat {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("message_by")
        @Expose
        private Integer messageBy;
        @SerializedName("order_id")
        @Expose
        private Integer orderId;
        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("time")
        @Expose
        private String time;
        @SerializedName("fullname")
        @Expose
        private String fullname;
        @SerializedName("image")
        @Expose
        private String image;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Integer getMessageBy() {
            return messageBy;
        }

        public void setMessageBy(Integer messageBy) {
            this.messageBy = messageBy;
        }

        public Integer getOrderId() {
            return orderId;
        }

        public void setOrderId(Integer orderId) {
            this.orderId = orderId;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

    }
}
