package com.meetch.bean.getProduct_model;

import com.google.gson.annotations.SerializedName;

public class ProductModel {
    @SerializedName("cat_name")
    private String cat_name;

    @SerializedName("price")
    private String price;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }



    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

}
