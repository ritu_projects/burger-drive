package com.meetch.bean.applypromomodel;

import com.google.gson.annotations.SerializedName;

public class Applypromocode_Model {
    @SerializedName("user_id")
    private String userid;
    @SerializedName("code_name")
    private String code_name;

    public String getCode_name() {
        return code_name;
    }

    public void setCode_name(String code_name) {
        this.code_name = code_name;
    }



    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
}
