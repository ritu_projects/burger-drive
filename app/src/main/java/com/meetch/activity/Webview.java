package com.meetch.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.meetch.MainActivity;
import com.meetch.R;
import com.meetch.storage.MySharedPref;

public class Webview extends AppCompatActivity {

    private WebView webview;
    private ActionBar actionBar;
    private Context context;
    private String mobileId = "",gift="";
    private View mCustomView;
    private ProgressBar progressBar;
    Dialog dialog;
    MySharedPref sp;
    RelativeLayout rr_webview;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_page);

        context = getApplicationContext();
        actionBar = getSupportActionBar();
        //	hideSoftKeyboard(PaymentPage.this);




        initialize();


        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        mCustomView = View.inflate(context, R.layout.payment_header, null);
        actionBar.setCustomView(mCustomView);
        actionBar.setDisplayShowCustomEnabled(true);
        // Show the progress bar
        webview.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int progress) {
                // setProgress(progress * 100);
                if (progress < 100
                        && progressBar.getVisibility() == ProgressBar.GONE) {
                    progressBar.setVisibility(ProgressBar.VISIBLE);
                }
                progressBar.setProgress(progress);
                if (progress == 100) {
                    progressBar.setVisibility(ProgressBar.GONE);
                }
            }
        });

        webview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                System.out.println("------payment shouldOverrideUrlLoading url-----------"+url);
                view.loadUrl(url);
                return true;
            }
            // Show loader on url load
            @Override
            public void onLoadResource(WebView view, String url) {
            }

            @Override
            public void onPageFinished(WebView view, String url) {

                System.out.println("------payment onPageFinished url-----------"+url);


                try {


                    if (url.contains("https://www.curzenncloud.fr/burger/orderPaymentSuccess")) {

                        System.out.println("------payment complete url-----------"+url);

                        finish();

                        Intent ii=new Intent(Webview.this, MainActivity.class);
                        startActivity(ii);
                        MySharedPref sp=new MySharedPref();
                        sp.saveData(getApplicationContext(),"address_id","");
                        sp.saveData(getApplicationContext(),"address_name","");
                    }
                    else if (url.equalsIgnoreCase("https://www.curzenncloud.fr/burger/orderPaymentCancel")) {

                        System.out.println("------payment Cancel url-----------"+url);

                        finish();

                        Intent i1 = new Intent(Webview.this, Cart_PageActivity.class);
                        startActivity(i1);
                    }
                    else if (url.equalsIgnoreCase("https://www.curzenncloud.fr/burger/orderPaymentCancel")) {

                        System.out.println("------payment failed url-----------"+url);

                        finish();

                        Intent i1 = new Intent(Webview.this, Cart_PageActivity.class);
                        startActivity(i1);
                    }

                                   } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

        });



    }

    private void initialize() {

        rr_webview=(RelativeLayout)findViewById(R.id.rr_webview);

        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(rr_webview.getWindowToken(), 0);		// Locate the WebView in webview.xml


        webview = (WebView) findViewById(R.id.payment_webview);
        progressBar = (ProgressBar) findViewById(R.id.payment_progressbar);

        // Enable Javascript to run in WebView
        webview.getSettings().setJavaScriptEnabled(true);

        // Allow Zoom in/out controls
        webview.getSettings().setBuiltInZoomControls(true);

        // Zoom out the best fit your screen
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setUseWideViewPort(true);

        Intent i = getIntent();
      String  order_id = i.getStringExtra("order_id");
      String  amount = i.getStringExtra("amount");

            webview.loadUrl("https://www.curzenncloud.fr/burger/api/order_final_payment?order_id="+order_id+"&amount="+amount);
            System.out.println("--Payment URL---"+ "https://www.curzenncloud.fr/burger/api/order_final_payment?order_id="+order_id+"&amount="+amount);





        // Call private class InsideWebViewClient
        webview.setWebViewClient(new InsideWebViewClient());
    }

    private class InsideWebViewClient extends WebViewClient {
        @Override
        // Force links to be opened inside WebView and not in Default Browser
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    // ------------------------code for Back Press----------------------------
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                PaymentCancel();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    private void PaymentCancel() {
        onBackPressed();
    }

    @Override
    // Detect when the back button is pressed
    public void onBackPressed() {
        if(webview.canGoBack()) {
            webview.goBack();
        } else {
            // Let the system handle the back button
            super.onBackPressed();
        }
    }
}
