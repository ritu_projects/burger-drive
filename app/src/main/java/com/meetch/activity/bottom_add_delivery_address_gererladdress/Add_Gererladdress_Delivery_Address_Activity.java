package com.meetch.activity.bottom_add_delivery_address_gererladdress;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.meetch.R;
import com.meetch.bean.deletedeliver_bean.DeleteDeliveryAdd_Bean;
import com.meetch.bean.getDeliveryAddress_model.GetDeliveryAddressModel;
import com.meetch.bean.singlecasedeatial.Singlecasemodel;
import com.meetch.constant.Constants;
import com.meetch.constant.GetUserId;
import com.meetch.constant.Utils;
import com.meetch.rectrofit.LoadInterface;
import com.meetch.utils.CustomTextView;
import com.meetch.utils.CustomTextViewRegular;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Add_Gererladdress_Delivery_Address_Activity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView add_gererladdress_delivery_recyclerView;
    ArrayList<String> arrayList=new ArrayList<>();
    Add_Gererladdress_Delivery_Address_Adp add_gererladdress_delivery_address_adp;
    ProgressBar loader;
    GetDeliveryAddressModel getDeliveryAddressModel;
    RelativeLayout rr_ajourte_crd,rr_back;
    CustomTextView title;
    Dialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_gererladdress_delivery_address_activity);
        inti();
    }



    public  void  inti(){

        arrayList.add("Big Max");
        arrayList.add("Big Max");
        arrayList.add("Big Max");
        arrayList.add("Big Max");
        arrayList.add("Big Max");
        arrayList.add("Big Max");
        arrayList.add("Big Max");
        rr_ajourte_crd=(RelativeLayout)findViewById(R.id.rr_ajourte_crd);

        loader=(ProgressBar)findViewById(R.id.loader);

        title=(CustomTextView)findViewById(R.id.title);

        title.setText(getResources().getString(R.string.add_livr_dlivered));
        rr_back=(RelativeLayout)findViewById(R.id.rr_back);
        rr_back.setOnClickListener(this);



        rr_ajourte_crd.setOnClickListener(this);

         callWebServiceforGetDeliveryAddress();

    }

    public void callWebServiceforGetDeliveryAddress()
    {
        if(Utils.isConnected(getApplicationContext()))
        {

            try {
                getDeliveryAddress();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }

    public  void showDialogforCancelCart(final String str_cart_id) {

        dialog = new Dialog(Add_Gererladdress_Delivery_Address_Activity.this);
        dialog.setContentView(R.layout.dlg_cacl_cart);
       CustomTextViewRegular txt_dlte_cart=(CustomTextViewRegular)dialog.findViewById(R.id.txt_dlte_cart);

        txt_dlte_cart.setText(getResources().getString(R.string.dlte_dlivy_addrs));

     LinearLayout   ll_ok=(LinearLayout)dialog.findViewById(R.id.ll_ok);
      LinearLayout  ll_cancel=(LinearLayout)dialog.findViewById(R.id.ll_cancel);

        ll_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                callWebserviceforDeleterDeliveryAddress(str_cart_id);

            }
        });
ll_cancel.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        dialog.dismiss();
    }
});

dialog.show();
    }


    public void callWebserviceforDeleterDeliveryAddress(String deliver_add)
    {
        if(Utils.isConnected(getApplicationContext()))
        {

            try {
                deleteDeliveryAddress(deliver_add);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }




    public  void deleteDeliveryAddress(String deliver_add) {

        loader.setVisibility(View.VISIBLE);


        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        DeleteDeliveryAdd_Bean deleteDeliveryAdd_bean = new DeleteDeliveryAdd_Bean();
        deleteDeliveryAdd_bean.setId(deliver_add);

        Call<ResponseBody> resultCall = loadInterface.deleteDeliveryAddress(deleteDeliveryAdd_bean);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!" + response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@" + responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@" + jsonObject22);

                            String status = jsonObject22.getString("error");
                            String errorMessage = jsonObject22.getString("errorMessage");

                            if (status.equalsIgnoreCase("false")) {
                                Toast.makeText(getApplicationContext(),errorMessage,Toast.LENGTH_SHORT).show();
callWebServiceforGetDeliveryAddress();
                            } else {
                                Toast.makeText(getApplicationContext(),errorMessage,Toast.LENGTH_SHORT).show();


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(Add_Gererladdress_Delivery_Address_Activity.this, getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });

    }


        public  void getDeliveryAddress()
    {

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        String  userid= GetUserId.getUserInfo(getApplicationContext());


        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        Singlecasemodel singlecasemodel = new Singlecasemodel();
        singlecasemodel.setUserid(userid);

        Call<ResponseBody> resultCall = loadInterface.getallDeliveryAddress(singlecasemodel);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {
                                Gson gson = new Gson();
                                getDeliveryAddressModel = gson.fromJson(responedata, GetDeliveryAddressModel.class);


                                add_gererladdress_delivery_address_adp=new Add_Gererladdress_Delivery_Address_Adp(Add_Gererladdress_Delivery_Address_Activity.this,getDeliveryAddressModel.getRecord());
                                add_gererladdress_delivery_recyclerView=(RecyclerView)findViewById(R.id.add_gererladdress_delivery_recyclerView);
                                add_gererladdress_delivery_recyclerView.setVisibility(View.VISIBLE);
                                add_gererladdress_delivery_recyclerView.setLayoutManager(new LinearLayoutManager(Add_Gererladdress_Delivery_Address_Activity.this, LinearLayoutManager.VERTICAL, false));
                                add_gererladdress_delivery_recyclerView.setAdapter(add_gererladdress_delivery_address_adp);
                                add_gererladdress_delivery_recyclerView.setNestedScrollingEnabled(false);

                                }

                            else
                            {
                                add_gererladdress_delivery_recyclerView.setVisibility(View.GONE);
                                //txt_no_cas_outers.setVisibility(VISIBLE);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });

    }



    @Override
    public void onClick(View v) {
        if(v==rr_ajourte_crd)
        {

                    Intent ii=new Intent(getApplicationContext(), Ajouter_Une_Adresse_De_Livraison_Activity.class);
                    startActivity(ii);

        }
        if (v == rr_back
        ) {

            onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {

        finish();


    }
//    public void open_Order_Confrmdialog()
//    {
//        ordr_cnfrm_dialog=new Dialog(Add_Gererladdress_Delivery_Address_Activity.this);
//
//        ordr_cnfrm_dialog.getWindow().getAttributes().windowAnimations = R.style.MyAlertDialogStyle;
//        // Include dialog.xml file
//        ordr_cnfrm_dialog.setContentView(R.layout.order_cnfrm_screen);
//        ordr_cnfrm_dialog.show();
//        btn_suivi_de_commande=(Button) ordr_cnfrm_dialog.findViewById(R.id.btn_suivi_de_commande);
//        btn_suivi_de_commande.setOnClickListener(this);
//    }


    public class Add_Gererladdress_Delivery_Address_Adp extends RecyclerView.Adapter<Add_Gererladdress_Delivery_Address_Adp.ViewHolder> {
        Context context;
        List<GetDeliveryAddressModel.DeliverAddressDetailsModel> arrayList;

        public Add_Gererladdress_Delivery_Address_Adp(Activity homeActivity, List<GetDeliveryAddressModel.DeliverAddressDetailsModel> arrayList) {
            this.context=homeActivity;
            this.arrayList=arrayList;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

            View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.add_gererladdress_delivery_addredd_item,viewGroup,false);
            ViewHolder viewHolder=new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

           viewHolder.txt_crd_vale.setText(arrayList.get(i).getAddress());
           viewHolder.rr_delete.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {


                   showDialogforCancelCart(arrayList.get(i).getId());
               }
           });


        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            CustomTextViewRegular txt_crd_vale;
            CardView crd_vew;
            ImageView img_delete;
            RelativeLayout rr_delete;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                crd_vew=(CardView)findViewById(R.id.card);

                txt_crd_vale=(CustomTextViewRegular)itemView.findViewById(R.id.txt_crd_vale);
                img_delete=(ImageView)itemView.findViewById(R.id.img_delete);
                rr_delete=(RelativeLayout) itemView.findViewById(R.id.rr_delete);

            }
        }
    }



}
