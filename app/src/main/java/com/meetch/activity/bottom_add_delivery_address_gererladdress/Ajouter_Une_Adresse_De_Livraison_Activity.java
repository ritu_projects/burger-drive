package com.meetch.activity.bottom_add_delivery_address_gererladdress;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.meetch.R;
import com.meetch.activity.Cart_PageActivity;
import com.meetch.activity.My_CommandActivity;
import com.meetch.activity.combodetails.COmboDetails_Activity;
import com.meetch.bean.addDeliveryAddressmodel.AddtoDeliveryAddressModel;
import com.meetch.constant.Constants;
import com.meetch.constant.GetUserId;
import com.meetch.constant.Utils;
import com.meetch.rectrofit.LoadInterface;
import com.meetch.utils.CustomTextView;
import com.meetch.utils.CustomTextViewRegular;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Ajouter_Une_Adresse_De_Livraison_Activity extends AppCompatActivity implements View.OnClickListener {

    Button btn_sauver_ajouter_une_add_de_livration;
    ProgressBar loader;
LinearLayout ll_crd_point_de_repère;
    public Ajouter_Une_Adresse_De_Livraison_Activity()
    {
        super();
    }
    Bundle b1;
CustomTextViewRegular txt_crd_point_de_repère,txt_crd_pays,txt_crd_payss,txt_crd_rue;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 101;
String product_id="",combo_id="",cart_id="";
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE_FILTER = 102;
    GoogleApiClient mGoogleApiClient;
    double dub_latitude,dub_longitude;
    private CustomTextView title;
    RelativeLayout rr_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ajouter_une_adresse_de_livraison_activity);
        getBundleData();
        inti();
    }

    private void getBundleData() {
        b1=getIntent().getExtras();
        if(b1!=null)
        {
           try {
               combo_id=b1.getString("combo_id");

           }
           catch (Exception e)
           {
               e.printStackTrace();
           }

           try {
               product_id=b1.getString("product_id");

           }
           catch (Exception e)
           {
               e.printStackTrace();
           }
            try {
                cart_id=b1.getString("cart_id");

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }



        }
    }

    public  void  inti(){
        Places.initialize(getApplicationContext(),"AIzaSyCN8uO3iBy6K92NYOFIaLFrIfNJ7X_A8Xc");

        PlacesClient placesClient = Places.createClient(this);

        loader=(ProgressBar)findViewById(R.id.loader);
        btn_sauver_ajouter_une_add_de_livration=(Button)findViewById(R.id.btn_sauver_ajouter_une_add_de_livration);
        btn_sauver_ajouter_une_add_de_livration.setOnClickListener(this);

        ll_crd_point_de_repère=(LinearLayout) findViewById(R.id.ll_crd_point_de_repère);

        txt_crd_pays=(CustomTextViewRegular)findViewById(R.id.txt_crd_pays);
        txt_crd_point_de_repère=(CustomTextViewRegular)findViewById(R.id.txt_crd_point_de_repère);
        txt_crd_payss=(CustomTextViewRegular)findViewById(R.id.txt_crd_payss);
        txt_crd_rue=(CustomTextViewRegular)findViewById(R.id.txt_crd_rue);
      //  txt_crd_point_de_repère.setOnClickListener(this);
        ll_crd_point_de_repère.setOnClickListener(this);

        title=(CustomTextView)findViewById(R.id.title);

        title.setText(getResources().getString(R.string.add_livr_dlivered));
        rr_back=(RelativeLayout)findViewById(R.id.rr_back);
        rr_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if(v==btn_sauver_ajouter_une_add_de_livration)
        {

            callWebServiceforAddDeliveryAddress();


        }
        if(v==rr_back)
        {
            onBackPressed();
        }

        if(v==ll_crd_point_de_repère)
        {


            List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG, Place.Field.TYPES);

            // Start the autocomplete intent.
            Intent intent = new Autocomplete.IntentBuilder(
                    AutocompleteActivityMode.OVERLAY, fields)
                    .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);



        }
    }
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                try {
                    if(place!=null)
                    {

                        hideKeyboard(Ajouter_Une_Adresse_De_Livraison_Activity.this);

                        Log.i("TAG", "Place: " + place.getName() + ", " + place);
LatLng dublatlng=place.getLatLng();

                         dub_latitude=dublatlng.latitude;
                        dub_longitude=dublatlng.longitude;

                        txt_crd_pays.setText(place.getName());
                        txt_crd_payss.setText(place.getAddress());
                        txt_crd_rue.setText(place.getName()+"");
                        txt_crd_point_de_repère.setText(place.getAddress());


                    }
                }
                catch (Exception e)
                {
                    System.out.println("Exception is   "+e);
                }

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i("TAG", status.getStatusMessage());
                hideKeyboard(Ajouter_Une_Adresse_De_Livraison_Activity.this);

            } else if (resultCode == RESULT_CANCELED) {
                hideKeyboard(Ajouter_Une_Adresse_De_Livraison_Activity.this);
                // The user canceled the operation.
            }
        }
    }
    public void callWebServiceforAddDeliveryAddress()
    {
        if(Utils.isConnected(getApplicationContext()))
        {

            try {
               validate();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }
    private void validate() {

        boolean isError=false;
        String str_address=txt_crd_point_de_repère.getText().toString();


        if(str_address==null || str_address.equalsIgnoreCase("") || str_address.equalsIgnoreCase("null") || str_address.equalsIgnoreCase("Adresse"))
        {
            isError=true;
            txt_crd_point_de_repère.setError(getResources().getString(R.string.ce_champ_est_requis));


        }

        else
        {
            txt_crd_point_de_repère.setError(null);
        }


        if(!isError)
        {
            addDeliveryAddress();
        }

    }

    @Override
    public void onBackPressed() {

        finish();
        Intent ii=new Intent(Ajouter_Une_Adresse_De_Livraison_Activity.this,Add_Gererladdress_Delivery_Address_Activity.class);
        startActivity(ii);


    }
    public  void addDeliveryAddress()
   {

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
       String  userid= GetUserId.getUserInfo(getApplicationContext());


        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
       AddtoDeliveryAddressModel addtoDeliveryAddress = new AddtoDeliveryAddressModel();
        addtoDeliveryAddress.setUser_id(userid);
       addtoDeliveryAddress.setAddress(txt_crd_point_de_repère.getText().toString());
       addtoDeliveryAddress.setLandmark(txt_crd_pays.getText().toString());
       addtoDeliveryAddress.setLatitude(dub_latitude);
       addtoDeliveryAddress.setLongitude(dub_longitude);

        Call<ResponseBody> resultCall = loadInterface.addDeliveryAddress(addtoDeliveryAddress);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {

                            if(b1!=null)
                            {
                                if(product_id!=null && !product_id.equalsIgnoreCase("") &&
                                        !product_id.equalsIgnoreCase("null")) {
                                    Intent ii = new Intent(Ajouter_Une_Adresse_De_Livraison_Activity.this, My_CommandActivity.class);
                                    ii.putExtra("product_id", product_id);
                                    startActivity(ii);
                                }


                                if(combo_id!=null && !combo_id.equalsIgnoreCase("") &&
                                        !combo_id.equalsIgnoreCase("null")) {
                                    finish();

                                    Intent ii = new Intent(Ajouter_Une_Adresse_De_Livraison_Activity.this, COmboDetails_Activity.class);
                                    ii.putExtra("combo_id", combo_id);
                                    startActivity(ii);
                                }

                                if(cart_id!=null && !cart_id.equalsIgnoreCase("") &&
                                        !cart_id.equalsIgnoreCase("null")) {
                                    finish();
                                    Intent ii = new Intent(Ajouter_Une_Adresse_De_Livraison_Activity.this, Cart_PageActivity.class);
                                  //  ii.putExtra("combo_id", combo_id);
                                    startActivity(ii);
                                }

                            }

                            else {
                              onBackPressed();
                            }
                    }

                            else
                            {
                                String  errorMessage = jsonObject22.getString("errorMessage");
                                Toast.makeText(getApplicationContext(),errorMessage,Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });

    }

}
