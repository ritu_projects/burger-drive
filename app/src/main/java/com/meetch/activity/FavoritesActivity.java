package com.meetch.activity;

import android.os.Bundle;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.meetch.R;
import com.meetch.adapter.Special_BurgerAdapter;
import com.meetch.bean.myfavorite_bean.MyFavorite_Bean;
import com.meetch.bean.singlecasedeatial.Singlecasemodel;
import com.meetch.constant.Constants;
import com.meetch.constant.GetUserId;
import com.meetch.constant.Utils;
import com.meetch.rectrofit.LoadInterface;
import com.meetch.utils.CustomTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FavoritesActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView favi_list_rcyle;
    private RelativeLayout rr_back;
    private Special_BurgerAdapter favo_itemAdapter;
    private ArrayList<String>arrayList=new ArrayList<>();
    ProgressBar loader;
    MyFavorite_Bean myFavorite_bean;
    CustomTextView txt_no_order_found,title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);
        inti();
    }

    public  void  inti(){
        arrayList.clear();
        arrayList.add("");
        favi_list_rcyle=(RecyclerView)findViewById(R.id.favi_list_rcyle);
        rr_back=(RelativeLayout)findViewById(R.id.rr_back);
        title=(CustomTextView)findViewById(R.id.title);
        title.setText(getResources().getString(R.string.special_burger));

        rr_back.setOnClickListener(this);
        txt_no_order_found=(CustomTextView)findViewById(R.id.txt_no_order_found);

        loader=(ProgressBar)findViewById(R.id.loader);
        callWebService();

    }

    public void callWebService()
    {
        if(Utils.isConnected(getApplicationContext()))
        {

            try {
                getfavProduct();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }

    public  void getfavProduct()
    {

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        String  userid= GetUserId.getUserInfo(getApplicationContext());
        String  usertoken= GetUserId.getUserToken(getApplicationContext());

        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        Singlecasemodel singlecasemodel = new Singlecasemodel();
        singlecasemodel.setUserid(userid);

        Call<ResponseBody> resultCall = loadInterface.getSpecialBurger(singlecasemodel);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {

                                txt_no_order_found.setVisibility(View.GONE);

                                Gson gson = new Gson();
                                myFavorite_bean = gson.fromJson(responedata, MyFavorite_Bean.class);

                                favo_itemAdapter=new Special_BurgerAdapter(FavoritesActivity.this,myFavorite_bean.getOrderList());
                                favi_list_rcyle.setLayoutManager(new LinearLayoutManager(FavoritesActivity.this));
                                favi_list_rcyle.setAdapter(favo_itemAdapter);


                            }

                            else
                            {
                                favi_list_rcyle.setVisibility(View.GONE);
                                txt_no_order_found.setVisibility(View.VISIBLE);

                                txt_no_order_found.setText(getResources().getString(R.string.no_favproduct));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });

    }
    @Override
    public void onClick(View v) {
        if(v==rr_back){
            finish();
        }

    }
}
