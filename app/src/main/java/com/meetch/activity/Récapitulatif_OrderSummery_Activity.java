package com.meetch.activity;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.meetch.R;

import java.util.ArrayList;

public class Récapitulatif_OrderSummery_Activity extends AppCompatActivity {

    RecyclerView cart_item_list_recyle;
    Cart_Item_listAdp cart_item_listAdp;
    ArrayList<String> arrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recapitulatif_ordersummery);
        inti();
    }

    public void inti() {

        arrayList.add("Burger");
        arrayList.add("Coco Cola");
        arrayList.add("");
        arrayList.add("");
        arrayList.add("");
        arrayList.add("");

        cart_item_listAdp = new Cart_Item_listAdp(Récapitulatif_OrderSummery_Activity.this, arrayList);
        cart_item_list_recyle = (RecyclerView) findViewById(R.id.cart_item_list_recyle);
        cart_item_list_recyle.setVisibility(View.VISIBLE);
        cart_item_list_recyle.setLayoutManager(new LinearLayoutManager(Récapitulatif_OrderSummery_Activity.this, LinearLayoutManager.HORIZONTAL, false));
        cart_item_list_recyle.setAdapter(cart_item_listAdp);
    }


    public class Cart_Item_listAdp extends RecyclerView.Adapter<Cart_Item_listAdp.ViewHolder> {
        Context context;
        ArrayList<String> arrayList;

        public Cart_Item_listAdp(Activity homeActivity, ArrayList<String> arrayList) {
            this.context = homeActivity;
            this.arrayList = arrayList;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cart_item, viewGroup, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        }


        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public ViewHolder(@NonNull View itemView) {
                super(itemView);
            }
        }
    }
}