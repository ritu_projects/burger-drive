package com.meetch.activity.mode_de_paiement.ajoyter_de_paeiment_activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.meetch.R;
import com.meetch.utils.CustomTextView;

public class Ajoyter_de_Paeiment_Activity extends AppCompatActivity implements View.OnClickListener {
Button btn_continuez;
    private CustomTextView title;
    RelativeLayout rr_back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ajoyter_de_paeiment_activity);
        inti();
    }
    @Override
    public void onBackPressed() {

        finish();
//        Intent ii=new Intent(Ajoyter_de_Paeiment_Activity.this, Mode_De_Paiement_Activity.class);
//        startActivity(ii);

    }

    private void inti() {
        btn_continuez=(Button)findViewById(R.id.btn_continuez);

        btn_continuez.setOnClickListener(this);

        title=(CustomTextView)findViewById(R.id.title);

        rr_back=(RelativeLayout)findViewById(R.id.rr_back);
        rr_back.setOnClickListener(this);
        title.setText(getResources().getString(R.string.mode_de_paiement));

    }

    @Override
    public void onClick(View v) {

        if(v==btn_continuez)
        {
            Intent ii=new Intent(Ajoyter_de_Paeiment_Activity.this,Ajouter_Une_Carte_Activity.class);
            startActivity(ii);
        }
        if(v==rr_back)
        {
            onBackPressed();
        }
    }
}
