package com.meetch.activity.mode_de_paiement.ajoyter_de_paeiment_activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.meetch.R;
import com.meetch.utils.CustomTextView;

public class Ajouter_Une_Carte_Activity extends AppCompatActivity implements View.OnClickListener {

        RelativeLayout rr_back;
        CustomTextView title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ajouter_une_carte_activity);
        inti();
    }
    @Override
    public void onBackPressed() {

        finish();
        Intent ii=new Intent(Ajouter_Une_Carte_Activity.this, Ajoyter_de_Paeiment_Activity.class);
        startActivity(ii);

    }

    private void inti() {

        rr_back=(RelativeLayout)findViewById(R.id.rr_back);
        rr_back.setOnClickListener(this);
        title=(CustomTextView)findViewById(R.id.title);
        title.setText(getResources().getString(R.string.ajouter_un_moyen_de_paiement));
    }

    @Override
    public void onClick(View view) {
            if(view==rr_back)
            {
                onBackPressed();
            }
    }
}
