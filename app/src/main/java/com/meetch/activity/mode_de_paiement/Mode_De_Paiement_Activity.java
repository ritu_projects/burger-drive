package com.meetch.activity.mode_de_paiement;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.meetch.R;
import com.meetch.activity.mode_de_paiement.ajoyter_de_paeiment_activity.Ajoyter_de_Paeiment_Activity;
import com.meetch.utils.CustomTextView;

public class Mode_De_Paiement_Activity extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout rr_ajourte_crd,rr_back;
    private CustomTextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mode_de_paiement_activity);
        inti();
    }

    public  void  inti(){
        rr_ajourte_crd=(RelativeLayout)findViewById(R.id.rr_ajourte_crd);
        title=(CustomTextView)findViewById(R.id.title);

        rr_back=(RelativeLayout)findViewById(R.id.rr_back);
        rr_back.setOnClickListener(this);

        title.setText(getResources().getString(R.string.mode_de_paiement));


        rr_ajourte_crd.setOnClickListener(this);
    }
    @Override
    public void onBackPressed() {

        finish();


    }

    @Override
    public void onClick(View v) {

        if(v==rr_ajourte_crd)
        {
            Intent ii=new Intent(Mode_De_Paiement_Activity.this, Ajoyter_de_Paeiment_Activity.class);
            startActivity(ii);
        }
        if(v==rr_back)
        {
                onBackPressed();
        }

    }
}
