package com.meetch.activity.categorylist;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.meetch.MainActivity;
import com.meetch.R;
import com.meetch.activity.My_CommandActivity;
import com.meetch.activity.ProductList_Activity;
import com.meetch.bean.category_allproduct_model.GetProduct_Bean;
import com.meetch.bean.getseachproductparameterbean.GetSearchProductParameter_Bean;
import com.meetch.bean.getsearchproductbean.GetSearchProduct_Bean;
import com.meetch.constant.Constants;
import com.meetch.constant.Utils;
import com.meetch.expandablelist.ExpandableHeightGridView;
import com.meetch.picasso.PicassoTrustAll;
import com.meetch.rectrofit.LoadInterface;
import com.meetch.utils.CustomTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class CategoriesList_Activity extends AppCompatActivity implements View.OnClickListener {
    ExpandableHeightGridView category_grid;
ProgressBar loader;
    GetSearchProduct_Bean getSearchProduct_bean;
CategoriesAdapter category_adapter;
AutoCompleteTextView txt_livrer;
ArrayList<String> arr_srch;
GetProduct_Bean getProduct_bean;
    ArrayAdapter adapter;
    RelativeLayout rr_location,rr_delivering_to,rr_filter,rr_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_list);

        inti();
    }

    public  void  inti(){
        category_grid=(ExpandableHeightGridView)findViewById(R.id.category_grid);
        loader=(ProgressBar)findViewById(R.id.loader);
        txt_livrer=(AutoCompleteTextView)findViewById(R.id.txt_livrer);
        rr_location=(RelativeLayout)findViewById(R.id.rr_location);
        rr_location.setVisibility(VISIBLE);



        rr_back=(RelativeLayout)findViewById(R.id.rr_back);
        rr_back.setOnClickListener(this);
    CustomTextView    title=(CustomTextView)findViewById(R.id.title);

        title.setText(getResources().getString(R.string.categories));


        rr_delivering_to=(RelativeLayout)findViewById(R.id.rr_delivering_to);
        rr_delivering_to.setVisibility(VISIBLE);

        rr_filter=(RelativeLayout)findViewById(R.id.rr_filter);
        rr_filter.setVisibility(GONE);



        txt_livrer.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                callWebServiceTxtChange(s.toString());
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        txt_livrer.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int position, long arg3) {
                String product_idd=getSearchProduct_bean.getRecord().get(position).getId();
                System.out.println("Product Id   "+product_idd);
                Intent ii=new Intent(CategoriesList_Activity.this, My_CommandActivity.class);
                ii.putExtra("product_id",product_idd);
                startActivity(ii);
            }
        });

        callWebService();

        category_grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent ii=new Intent(CategoriesList_Activity.this, ProductList_Activity.class);
                ii.putExtra("category_name",getProduct_bean.getList().get(i).getName());
                startActivity(ii);

            }
        });

    }

    public void callWebService()

    {
        if(Utils.isConnected(getApplicationContext()))
        {

            try {

                getCategoryList();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }

    public void callWebServiceTxtChange(String s)

    {
        if(Utils.isConnected(getApplicationContext()))
        {

            try {

                getSearchList(s);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }
    private void getSearchList(String str_product) {

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        final GetSearchProductParameter_Bean getSearchProductParameter_bean = new GetSearchProductParameter_Bean();
        getSearchProductParameter_bean.setProduct_name(str_product);

        Call<ResponseBody> resultCall = loadInterface.get_SearchProduct(getSearchProductParameter_bean);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response Product is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response Product is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {

                                Gson gson = new Gson();
                                getSearchProduct_bean= gson.fromJson(responedata, GetSearchProduct_Bean.class);
                                arr_srch=new ArrayList<>();

                                for(int k=0;k<getSearchProduct_bean.getRecord().size();k++)
                                {
                                    arr_srch.add(getSearchProduct_bean.getRecord().get(k).getProductName());
                                }
                                System.out.println("Srch List Size  "+arr_srch.size());
                                 adapter = new ArrayAdapter(CategoriesList_Activity.this, android.R.layout.simple_list_item_1, arr_srch);
                                //txt_livrer.setThreshold(1);//start searching from 1 character
                             //   txt_livrer.setAdapter(adapter);

                                txt_livrer.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                             //   txt_livrer.setThreshold(1);//start searching from 1 character
                            //   adapter.notifyDataSetChanged();
                            //    txt_livrer.setAdapter(adapter);
                              //  adapter.notifyDataSetChanged();

                            }

                            else
                            {
                                //txt_no_cas_outers.setVisibility(VISIBLE);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });

    }

    private void getCategoryList() {

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        LoadInterface loadInterface = retrofit.create(LoadInterface.class);

        Call<ResponseBody> resultCall = loadInterface.get_Category();
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {


                                Gson gson = new Gson();
                                getProduct_bean = gson.fromJson(responedata, GetProduct_Bean.class);
                                category_grid.setVisibility(VISIBLE);

                                category_adapter = new CategoriesAdapter(CategoriesList_Activity.this, getProduct_bean.getList());
                                category_grid.setAdapter(category_adapter);
                                category_grid.setNumColumns(2);
                             }

                            else
                            {
                                category_grid.setVisibility(GONE);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });



    }

    @Override
    public void onBackPressed() {
        finish();

        Intent ii=new Intent(CategoriesList_Activity.this, MainActivity.class);
        startActivity(ii);
    }

    @Override
    public void onClick(View view) {

        if(view==rr_back)
        {
            onBackPressed();
        }
    }

    public class CategoriesAdapter extends BaseAdapter {
        private Context mContext;
        List<GetProduct_Bean.GetProductDescp_Bean> arrayList;
        CustomTextView txt_canme;
        FrameLayout rr_catgry;
        ImageView img_cat;

        // Constructor
        public CategoriesAdapter(Context c,List<GetProduct_Bean.GetProductDescp_Bean> list) {
            this.mContext = c;
            this.arrayList=list;
        }

        public int getCount() {
            return arrayList.size();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return arrayList.size();
        }

        // create a new ImageView for each item referenced by the Adapter
        public View getView(final int position, View convertView, ViewGroup parent) {
            View grid;
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            grid = new View(mContext);
            grid = inflater.inflate(R.layout.category_list_item, null);
            txt_canme=(CustomTextView) grid.findViewById(R.id.txt_canme);
            txt_canme.setText(arrayList.get(position).getName());
            rr_catgry=(FrameLayout)grid.findViewById(R.id.rr_catgry);
            img_cat=(ImageView)grid.findViewById(R.id.img_cat);

            PicassoTrustAll.getInstance(mContext)
                    .load(arrayList.get(position).getcatImage())
                    .error(R.drawable.bnanner)
                    .into(img_cat);

            rr_catgry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent ii=new Intent(CategoriesList_Activity.this, ProductList_Activity.class);
                    ii.putExtra("category_name",getProduct_bean.getList().get(position).getName());
                    startActivity(ii);
                }
            });

            return grid;
        }


    }
}
