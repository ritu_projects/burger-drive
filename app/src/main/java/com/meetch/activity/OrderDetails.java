package com.meetch.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.meetch.R;
import com.meetch.adapter.CartComboProduct_ItemAdapter;
import com.meetch.bean.catergoryListatCheckoutPage_model.CategroryListCheckout_model;
import com.meetch.bean.combobean.ComboBean;
import com.meetch.bean.orderdetails_bean.OrderDetailsBean;
import com.meetch.bean.productListatCheckoutPage_model.ProductListatCheckoutPage_model;
import com.meetch.constant.Constants;
import com.meetch.constant.GetUserId;
import com.meetch.constant.Utils;
import com.meetch.picasso.PicassoTrustAll;
import com.meetch.rectrofit.LoadInterface;
import com.meetch.utils.CustomEditTextRegular;
import com.meetch.utils.CustomTextView;
import com.meetch.utils.CustomTextViewRegular;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class OrderDetails extends AppCompatActivity implements View.OnClickListener {
    RecyclerView cart_item_list_recyle,list_item_recyl,list_item_recyl_combo,cart_item_list_alldescp_recyle;
    Card_ItemAdapter card_itemAdapter;
   CardDetail_ItemAdapter cardDetail_itemAdapter;

    ArrayList<CategroryListCheckout_model> arrayList =new ArrayList<>();
    ArrayList<CategroryListCheckout_model> arrayList2 =new ArrayList<>();

    CustomTextView txt_tax_value,txt_shippig_cost;
LinearLayout ll_combo;
    private CustomTextView title;
    Button btn_checkout;
    ProgressBar loader;
    RelativeLayout rr_layout_promocode,rr_layout_node,rr_back;
    String  userid,product_base_url,token;
    CustomTextViewRegular txt_addressz;
    Button btn_add_promo;
    CustomTextView txt_total_price,txt_related_products,txt_votre_cavalier,txt_orderid,txt_orderdate,txt_deliverydate,txt_orderstatus,txt_driver_name,txt_driver_phonenmber;
    CustomEditTextRegular edt_promo_code;
TextView txt_add_articales;
Bundle b1;
Button btn_add_delivery_address;
RelativeLayout rr_delivery_add,rr_profile;
String order_id;
ImageView img_edt;
    CircleImageView img_profile;
    ArrayList<ComboBean> arrayList_combo =new ArrayList<>();

    ArrayList<ProductListatCheckoutPage_model> arrayList_combo_product =new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_details);
        userid= GetUserId.getUserInfo(getApplicationContext());
        token= GetUserId.getUserToken(getApplicationContext());

        getBundleData();
        inti();
    }

    private void getBundleData() {
        b1=getIntent().getExtras();
        if(b1!=null)
        {
            order_id=b1.getString("order_id");
        }
    }


    public  void  inti(){

        rr_layout_promocode=(RelativeLayout)findViewById(R.id.rr_layout_promocode);
        rr_profile=(RelativeLayout)findViewById(R.id.rr_profile);

        img_edt=(ImageView)findViewById(R.id.img_edt);
        txt_tax_value=(CustomTextView)findViewById(R.id.txt_tax_value);
        txt_shippig_cost=(CustomTextView)findViewById(R.id.txt_shippig_cost);

        txt_total_price=(CustomTextView)findViewById(R.id.txt_total_price);
        txt_votre_cavalier=(CustomTextView)findViewById(R.id.txt_votre_cavalier);
        txt_related_products=(CustomTextView)findViewById(R.id.txt_related_products);


        txt_orderid=(CustomTextView)findViewById(R.id.txt_orderid);
        txt_orderdate=(CustomTextView)findViewById(R.id.txt_orderdate);
        txt_deliverydate=(CustomTextView)findViewById(R.id.txt_deliverydate);
        txt_orderstatus=(CustomTextView)findViewById(R.id.txt_orderstatus);
        txt_driver_name=(CustomTextView)findViewById(R.id.txt_driver_name);
        txt_driver_phonenmber=(CustomTextView)findViewById(R.id.txt_driver_phonenmber);
        img_profile=(CircleImageView)findViewById(R.id.img_profile);


        edt_promo_code=(CustomEditTextRegular)findViewById(R.id.edt_promo_code);
        btn_add_promo=(Button)findViewById(R.id.btn_add_promo);

        txt_related_products.setVisibility(GONE);

        loader=(ProgressBar)findViewById(R.id.loader);
        rr_layout_node=(RelativeLayout)findViewById(R.id.rr_layout_node);

        txt_addressz=(CustomTextViewRegular)findViewById(R.id.txt_addressz);
        rr_back=(RelativeLayout)findViewById(R.id.rr_back);



        txt_add_articales=(TextView)findViewById(R.id.txt_add_articales);
        txt_add_articales.setVisibility(GONE);
        title=(CustomTextView)findViewById(R.id.title);
        title.setText(getResources().getString(R.string.orderdetails));

        btn_add_delivery_address=(Button)findViewById(R.id.btn_add_delivery_address);

        rr_delivery_add=(RelativeLayout)findViewById(R.id.rr_delivery_add);
list_item_recyl=(RecyclerView)findViewById(R.id.list_item_recyl);
    list_item_recyl.setVisibility(GONE);

        ll_combo =(LinearLayout)findViewById(R.id.ll_combo);
        ll_combo.setVisibility(GONE);

        list_item_recyl_combo = (RecyclerView)findViewById(R.id.list_item_recyl_combo);
        list_item_recyl_combo.setVisibility(GONE);


        rr_layout_promocode.setVisibility(GONE);
        rr_layout_node.setVisibility(GONE);
        rr_back.setOnClickListener(this);

        btn_add_delivery_address.setVisibility(GONE);
        rr_delivery_add.setVisibility(View.VISIBLE);
        img_edt.setVisibility(GONE);

        btn_checkout=(Button)findViewById(R.id.btn_checkout);
        btn_checkout.setVisibility(GONE);
        btn_add_promo.setVisibility(GONE);
        btn_checkout.setOnClickListener(this);

        callWebService();
    }
    public void callWebService()

    {
        if(Utils.isConnected(getApplicationContext()))
        {

            try {
                getOrderDetails();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }




    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onBackPressed() {
        finish();

        Intent ii=new Intent(OrderDetails.this, MyOrders.class);
        startActivity(ii);
    }
    @Override
    public void onClick(View v) {

        if(v==rr_back){

onBackPressed();
        }
        if(v==btn_checkout)
        {
            Intent ii=new Intent(OrderDetails.this,Checkout_Activity.class);
            startActivity(ii);

        }



    }



    private void getOrderDetails() {

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        OrderDetailsBean orderDetailsBean = new OrderDetailsBean();
        orderDetailsBean.setUserid(userid);
        orderDetailsBean.setOrder_id(order_id);

        Call<ResponseBody> resultCall = loadInterface.getUserOrderSummary(userid,token,orderDetailsBean);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status ResponseCart is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is Cart@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {

                              String  result = jsonObject22.getString("result");
                              JSONObject jsonObject33=new JSONObject(result);

                                String  address = jsonObject33.getString("delivery_address");
                                txt_addressz.setText(address);

                                String str_tax=jsonObject33.getString("order_tax");
                                txt_tax_value.setText(str_tax+"€");

                                String str_order_id=jsonObject33.getString("order_id");
                                txt_orderid.setText("Commande Id: "+str_order_id);

                                String str_order_date=jsonObject33.getString("order_date");
                                txt_orderdate.setText("date de commande: "+str_order_date);

                                String str_deliverydate=jsonObject33.getString("delivery_date");
                                txt_deliverydate.setText("Date de livraison de la commande: "+str_deliverydate);

                                String str_orderStatus=jsonObject33.getString("orderStatus");
                                txt_orderstatus.setText("État de la commande: "+str_orderStatus);



                                String str_driver_name=jsonObject33.getString("driver_name");

                                if(str_driver_name!=null && !str_driver_name.equalsIgnoreCase("") && !str_driver_name.equalsIgnoreCase("null")) {
                                   rr_profile.setVisibility(View.VISIBLE);

                                                                    String driver_img=jsonObject33.getString("driver_img");
                                PicassoTrustAll.getInstance(OrderDetails.this)
                                        .load(driver_img)
                                        .into(img_profile);
                                    txt_driver_name.setText(str_driver_name);

                                    String str_driver_no = jsonObject33.getString("driver_no");
                                    txt_driver_phonenmber.setText(str_driver_no);
                                }
                                else
                                {
                                    rr_profile.setVisibility(GONE);

                                }


                                String str_shipping_charge=jsonObject33.getString("shipping_charge");
                                txt_shippig_cost.setText(str_shipping_charge+"€");

                                txt_total_price.setText(jsonObject33.getString("final_price")+"€");

                                String  record = jsonObject33.getString("record");
                                JSONObject jsonObject44=new JSONObject(record);

                                JSONArray jsonArray_category=jsonObject44.getJSONArray("single");
                                if(jsonArray_category!=null && jsonArray_category.length()>0) {
                                    list_item_recyl.setVisibility(VISIBLE);


                                    for (int k = 0; k < jsonArray_category.length(); k++) {
                                        JSONObject jsonObject_category = jsonArray_category.getJSONObject(k);
                                        System.out.println("Json Category   " + jsonObject_category);
                                        CategroryListCheckout_model categroryListCheckout_model = new CategroryListCheckout_model();

                                        categroryListCheckout_model.setCat_img(jsonObject_category.getString("image"));
                                        categroryListCheckout_model.setCat_prize(jsonObject_category.getString("price"));
                                        categroryListCheckout_model.setCat_name(jsonObject_category.getString("product_name"));

                                        arrayList.add(categroryListCheckout_model);
                                    }
                                }

                             card_itemAdapter=new Card_ItemAdapter( OrderDetails.this,arrayList);
                                cart_item_list_recyle=(RecyclerView)findViewById(R.id.cart_item_list_recyle);
                                cart_item_list_recyle.setLayoutManager(new LinearLayoutManager(OrderDetails.this,LinearLayoutManager.VERTICAL,false));
                                cart_item_list_recyle.setAdapter(card_itemAdapter);
                                cart_item_list_recyle.setNestedScrollingEnabled(false);


                                list_item_recyl=(RecyclerView)findViewById(R.id.list_item_recyl);
                                list_item_recyl.setVisibility(GONE);



                                JSONArray jsonArray_combo=jsonObject44.getJSONArray("combo");


                                if(jsonArray_combo!=null && jsonArray_combo.length()>0) {


                                  ll_combo.setVisibility(View.VISIBLE);


                                    for (int k = 0; k < jsonArray_combo.length(); k++) {
                                        JSONObject jsonObject_product = jsonArray_combo.getJSONObject(k);

                                        ComboBean comboBean = new ComboBean();
                                        comboBean.setCombo_id(jsonObject_product.getInt("combo_id"));
                                        comboBean.setCombo_name(jsonObject_product.getString("combo_name"));
                                        comboBean.setCombo_img(jsonObject_product.getString("combo_img"));
                                        comboBean.setCombo_price(jsonObject_product.getString("combo_price"));

                                        JSONArray jsonArray_combo2 = jsonObject_product.getJSONArray("cartCombo");

                                        arrayList_combo_product = new ArrayList<>();

                                        for (int l = 0; l < jsonArray_combo2.length(); l++) {

                                            JSONObject jsonObject_productt = jsonArray_combo2.getJSONObject(l);


                                            ProductListatCheckoutPage_model productListatCheckoutPage_model = new ProductListatCheckoutPage_model();
                                            productListatCheckoutPage_model.setProduct_name(jsonObject_productt.getString("product_name"));
                                            productListatCheckoutPage_model.setQuantity(jsonObject_productt.getString("quantity"));

                                            productListatCheckoutPage_model.setCat_name(jsonObject_productt.getString("cat_name"));
                                            // productListatCheckoutPage_model.setId(jsonObject_product.getString("id"));


                                            arrayList_combo_product.add(productListatCheckoutPage_model);
                                        }


                                        comboBean.setProductListatCheckoutPage_models(arrayList_combo_product);
                                        arrayList_combo.add(comboBean);


                                    }


                                    System.out.println("Combo List Size  " + arrayList_combo.size());
                                    //  String str_combo_img=jsonObject22.getString("combo_img");

                                    list_item_recyl_combo.setHasFixedSize(true);
                                    LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(getApplicationContext());

                                    list_item_recyl_combo.setLayoutManager(linearLayoutManager2);

                                  Cart_ComboItemAdapter cart_comprendItemAdapter2 =
                                            new Cart_ComboItemAdapter(OrderDetails.this, arrayList_combo);

                                    list_item_recyl_combo.setAdapter(cart_comprendItemAdapter2);
                                    list_item_recyl_combo.setNestedScrollingEnabled(false);


//                                    cardDetail_itemAdapter = new CardDetail_ItemAdapter(OrderDetails.this, arrayList2);
//                                    cart_item_list_alldescp_recyle = (RecyclerView) findViewById(R.id.cart_item_list_alldescp_recyle);
//                                    cart_item_list_alldescp_recyle.setVisibility(GONE);
//
//                                    cart_item_list_alldescp_recyle.setLayoutManager(new LinearLayoutManager(OrderDetails.this, LinearLayoutManager.VERTICAL, false));
//                                    cart_item_list_alldescp_recyle.setAdapter(cardDetail_itemAdapter);

                                }




                            }

                            else
                            {
                                list_item_recyl=(RecyclerView)findViewById(R.id.list_item_recyl);
                                list_item_recyl.setVisibility(GONE);
                                //txt_no_cas_outers.setVisibility(VISIBLE);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });


    }

    public class CardDetail_ItemAdapter extends RecyclerView.Adapter<CardDetail_ItemAdapter.ViewHolder> {
        Context context;
        ArrayList<CategroryListCheckout_model> arrayList;

        public CardDetail_ItemAdapter(Activity cart_pageActivity, ArrayList<CategroryListCheckout_model> arrayList) {
            this.context=cart_pageActivity;
            this.arrayList=arrayList;
        }

        @NonNull
        @Override
        public CardDetail_ItemAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cart_category_item,viewGroup,false);
            ViewHolder viewHolder=new ViewHolder(view);
            return viewHolder;

        }

        @Override
        public void onBindViewHolder(@NonNull CardDetail_ItemAdapter.ViewHolder viewHolder, final int i) {


            viewHolder.txt_productname.setText(arrayList.get(i).getCat_name());
            viewHolder.txt_prize.setText(arrayList.get(i).getCat_prize()+"€");


        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView img_banner;
            CustomTextViewRegular  txt_productname,txt_prize;
            ImageView cancel_image;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                img_banner=(ImageView)itemView.findViewById(R.id.img_banner);
                txt_productname=(CustomTextViewRegular)itemView.findViewById(R.id.txt_productname);
                txt_prize=(CustomTextViewRegular)itemView.findViewById(R.id.txt_prize);
                cancel_image=(ImageView)itemView.findViewById(R.id.cancel_image);
                cancel_image.setVisibility(GONE);
            }
        }
    }

    public class Card_ItemAdapter extends RecyclerView.Adapter<Card_ItemAdapter.ViewHolder> {
        Context context;
        ArrayList<CategroryListCheckout_model> arrayList;

        public Card_ItemAdapter(Activity cart_pageActivity, ArrayList<CategroryListCheckout_model> arrayList) {
            this.context=cart_pageActivity;
            this.arrayList=arrayList;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cart_item,viewGroup,false);
            ViewHolder viewHolder=new ViewHolder(view);
            return viewHolder;

        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

            viewHolder.txt_cat_quantity.setText(arrayList.get(i).getCat_quantity());

            viewHolder.txt_cat_price.setText(arrayList.get(i).getCat_prize()+"€");
            viewHolder.cat_name.setText(arrayList.get(i).getCat_name());
            PicassoTrustAll.getInstance(context)

                    .load(arrayList.get(i).getCat_img())
                    .resize(100,100)
                    .error(R.drawable.bnanner)
                    .into(viewHolder.img_banner);


        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView img_banner;
            CustomTextView cat_name;
            CustomTextViewRegular txt_cat_price,txt_cat_quantity;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                img_banner=(ImageView)itemView.findViewById(R.id.img_banner);
                cat_name=(CustomTextView)itemView.findViewById(R.id.cat_name);
                txt_cat_price=(CustomTextViewRegular)itemView.findViewById(R.id.txt_cat_price);
                txt_cat_quantity=(CustomTextViewRegular)itemView.findViewById(R.id.txt_cat_quantity);


                txt_cat_price.setVisibility(View.VISIBLE);
                txt_cat_quantity.setVisibility(View.VISIBLE);

            }
        }
    }
    public class Cart_ComboItemAdapter extends RecyclerView.Adapter<Cart_ComboItemAdapter.ViewHolder> {
        Context context;
        ArrayList<ComboBean> arrayListt;
        String product_base_url,str_combo_img;




        public Cart_ComboItemAdapter(AppCompatActivity cart_pageActivity, ArrayList<ComboBean> arrayList_product) {
            this.context=cart_pageActivity;
            this.arrayListt=arrayList_product;
        }

        @NonNull
        @Override
        public Cart_ComboItemAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.combo_name_item,viewGroup,false);
            Cart_ComboItemAdapter.ViewHolder viewHolder=new Cart_ComboItemAdapter.ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull Cart_ComboItemAdapter.ViewHolder viewHolder, final int i) {

            viewHolder.txt_comboname.setText(getResources().getString(R.string.combo_name)+": " +arrayListt.get(i).getCombo_name());
            viewHolder.txt_comboprice.setText(getResources().getString(R.string.combo_price)+": " +arrayListt.get(i).getCombo_price());


            System.out.println("Combo Product List Size "+arrayListt.get(i).getProductListatCheckoutPage_models().size());


            CartComboProduct_ItemAdapter cart_comprendItemAdapter2=
                    new CartComboProduct_ItemAdapter(OrderDetails.this,arrayListt.get(i).getProductListatCheckoutPage_models(),
                            arrayListt.get(i).getCombo_img());

            viewHolder.list_item_recyl_combo_product.setLayoutManager(new LinearLayoutManager(OrderDetails.this,LinearLayoutManager.HORIZONTAL,false));
            viewHolder.list_item_recyl_combo_product.setAdapter(cart_comprendItemAdapter2);




        }

        @Override
        public int getItemCount() {
            return arrayListt.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView img_cancel_combo;
            CustomTextView txt_comboname,txt_comboprice;
            RecyclerView list_item_recyl_combo_product;
            RelativeLayout rr_img_cncl_combo;

            public ViewHolder(@NonNull View itemView) {

                super(itemView);
                img_cancel_combo=(ImageView)itemView.findViewById(R.id.img_cancel_combo);
                txt_comboname=(CustomTextView) itemView.findViewById(R.id.txt_comboname);
                txt_comboprice=(CustomTextView) itemView.findViewById(R.id.txt_comboprice);
                list_item_recyl_combo_product=(RecyclerView) itemView.findViewById(R.id.list_item_recyl_combo_product);
                rr_img_cncl_combo=(RelativeLayout)itemView.findViewById(R.id.rr_img_cncl_combo);

            }
        }
    }


}


