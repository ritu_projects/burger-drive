package com.meetch.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.meetch.R;
import com.meetch.fragment.myorders.CurrentOrders;
import com.meetch.fragment.myorders.PastOrders;
import com.meetch.utils.CustomTextView;

import java.util.ArrayList;
import java.util.List;


public class MyOrders extends AppCompatActivity implements View.OnClickListener  {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private RelativeLayout rr_back;
    CustomTextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_orders_main);

      //  toolbar = (Toolbar) findViewById(R.id.toolbarr);
       // setSupportActionBar(toolbar);

        rr_back=(RelativeLayout)findViewById(R.id.rr_back);
        rr_back.setOnClickListener(this);

        title=(CustomTextView) findViewById(R.id.title);
        title.setText(getResources().getString(R.string.my_orders));



//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new CurrentOrders(), "Commandes en cours");
        adapter.addFragment(new PastOrders(), "Commandes passées");
        viewPager.setAdapter(adapter);
    }
    @Override
    public void onBackPressed() {
        finish();

//        Intent ii=new Intent(MyOrders.this, MainActivity.class);
//        startActivity(ii);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Override
    public void onClick(View v) {
        if(v==rr_back){
            onBackPressed();

        }

    }
}
