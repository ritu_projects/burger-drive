package com.meetch.activity.bottom_history_des_commandes;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.meetch.R;
import com.meetch.activity.OrderDetails;
import com.meetch.bean.allPastOrdersBean.AllPastOrdersBean;
import com.meetch.bean.singlecasedeatial.Singlecasemodel;
import com.meetch.constant.Constants;
import com.meetch.constant.GetUserId;
import com.meetch.constant.Utils;
import com.meetch.picasso.PicassoTrustAll;
import com.meetch.rectrofit.LoadInterface;
import com.meetch.utils.CustomTextView;
import com.meetch.utils.CustomTextViewRegular;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Historique_Des_Commandes_Activities extends AppCompatActivity implements View.OnClickListener {

    Historique_Des_Commandes_Adp historique_des_commandes_adp;
    RecyclerView historique_des_commandes_recyclerView;
    ArrayList<String> arrayList=new ArrayList<>();
    ProgressBar loader;
AllPastOrdersBean allOrdersModel;
CustomTextView txt_no_order_found,title;
RelativeLayout rr_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.historique_des_commandes_activities);
        inti();
    }

    public  void  inti(){

        arrayList.add("Big Max");
        arrayList.add("Big Max");
        arrayList.add("Big Max");
        arrayList.add("Big Max");
        arrayList.add("Big Max");
        arrayList.add("Big Max");
        arrayList.add("Big Max");

        historique_des_commandes_recyclerView=(RecyclerView)findViewById(R.id.historique_des_commandes_recyclerView);

        txt_no_order_found=(CustomTextView)findViewById(R.id.txt_no_order_found);
        title=(CustomTextView)findViewById(R.id.title);
        title.setText(getResources().getString(R.string.historique_des_commandes));
        loader=(ProgressBar)findViewById(R.id.loader);

        rr_back=(RelativeLayout)findViewById(R.id.rr_back);
        rr_back.setOnClickListener(this);
        callWebService();

    }
    public void callWebService()
    {
        if(Utils.isConnected(getApplicationContext()))
        {

            try {
                getPastOrders();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }

    public  void getPastOrders()
    {

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        String  userid= GetUserId.getUserInfo(getApplicationContext());
        String  usertoken= GetUserId.getUserToken(getApplicationContext());

        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        Singlecasemodel singlecasemodel = new Singlecasemodel();
        singlecasemodel.setUserid(userid);

        Call<ResponseBody> resultCall = loadInterface.userpastorders(userid,usertoken,singlecasemodel);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {

                                txt_no_order_found.setVisibility(View.GONE);

                                Gson gson = new Gson();
                                allOrdersModel = gson.fromJson(responedata, AllPastOrdersBean.class);

                                historique_des_commandes_adp=new Historique_Des_Commandes_Adp(Historique_Des_Commandes_Activities.this,
                                        allOrdersModel.getDetail());
                                historique_des_commandes_recyclerView.setVisibility(View.VISIBLE);
                                historique_des_commandes_recyclerView.setLayoutManager(new LinearLayoutManager(Historique_Des_Commandes_Activities.this, LinearLayoutManager.VERTICAL, false));
                                historique_des_commandes_recyclerView.setAdapter(historique_des_commandes_adp);



                            }

                            else
                            {
                                historique_des_commandes_recyclerView.setVisibility(View.GONE);
                                txt_no_order_found.setVisibility(View.VISIBLE);

                                txt_no_order_found.setText(getResources().getString(R.string.no_past_orders));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });

    }
    @Override
    public void onBackPressed() {

    finish();
    }

    @Override
    public void onClick(View view) {
        if(view==rr_back){

            onBackPressed();
        }
    }

    public class Historique_Des_Commandes_Adp extends RecyclerView.Adapter<Historique_Des_Commandes_Adp.ViewHolder> {
        Context context;
        List<AllPastOrdersBean.Detail> arrayList;
        String img_base_url;

        public Historique_Des_Commandes_Adp(Activity homeActivity, List<AllPastOrdersBean.Detail> arrayList) {
            this.context=homeActivity;
            this.arrayList=arrayList;

        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

            View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.history_list_item,viewGroup,false);
           ViewHolder viewHolder=new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
            PicassoTrustAll.getInstance(context)
                    .load(arrayList.get(i).getProdImg())
                    //  .resize(R.dimen.mrg100,R.dimen.mrg100)
                    .error(R.drawable.bnanner)
                    .into(viewHolder.item_image);

            viewHolder.order_id.setText(getResources().getString(R.string.order_id)+": " +arrayList.get(i).getOrderId());
            viewHolder.product_name.setText(getResources().getString(R.string.order_date)+": "+arrayList.get(i).getOrderDate());
            viewHolder.deliver_date.setText(getResources().getString(R.string.delivery_date)+": "+arrayList.get(i).getInitiatedAt());
            viewHolder.product_price.setText(getResources().getString(R.string.cart_total)+": "+arrayList.get(i).getTotal()+"€");

            viewHolder.btn_voir_le_reçu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent ii=new Intent(context, OrderDetails.class);
                    ii.putExtra("order_id",arrayList.get(i).getOrderId());
                    startActivity(ii);
                }
            });
        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            CardView crd_vew;
            Button btn_voir_le_reçu;
            ImageView item_image;
            CustomTextView order_id;
            CustomTextViewRegular product_name,product_price,deliver_date;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                crd_vew=(CardView)itemView.findViewById(R.id.crd_vew);
                item_image=(ImageView)itemView.findViewById(R.id.img_best_offer);
                order_id=(CustomTextView)itemView.findViewById(R.id.txt_favoris);
                product_name=(CustomTextViewRegular)itemView.findViewById(R.id.txt_address);
                product_price=(CustomTextViewRegular)itemView.findViewById(R.id.txt_time);
                deliver_date=(CustomTextViewRegular)itemView.findViewById(R.id.deliver_date);
                btn_voir_le_reçu=(Button)itemView.findViewById(R.id.btn_voir_le_reçu);
            }
        }
    }

}
