package com.meetch.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.meetch.R;
import com.meetch.bean.orderdetails_bean.OrderDetailsBean;
import com.meetch.bean.ordertracking_bean.OrderTrackingBean;
import com.meetch.constant.Constants;
import com.meetch.constant.GetUserId;
import com.meetch.constant.Utils;
import com.meetch.directionjson.DirectionsJSONParser;
import com.meetch.gpstracker.GPSTracker;
import com.meetch.picasso.PicassoTrustAll;
import com.meetch.rectrofit.LoadInterface;
import com.meetch.utils.CustomTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Suivre_TrackOrder_Activity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {

    Button btn_resume_commandanture_summary;
    RelativeLayout rr_annulaer_cancel, rr_msg,rr_call;
    Bundle b1;
    String order_id,driver_contact_number;
    ProgressBar loader;
    GoogleMap mMap;
     LatLng originLatLng;
     LatLng destiLatLng;
     LatLng driverLatLng;
    double destination_lat, destination_lng, cusLat, cusLon, driverlat, driverlon;
    ImageView orderReceivedTick,orderReceivedNotTick,orderConfirmedTick,orderConfirmedNotTick;
CustomTextView txt_name,txt_temps_liveration_estimate_delivery_timee,title;
CircleImageView img_profile;
Dialog order_cancel_annulercnfrm_dlg;
RelativeLayout rr_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suivre_trackorder_activity);
        getBundleData();
        getLatlongGps();

        inti();
        callWebService();

    }

    private void getLatlongGps() {
        GPSTracker gps = new GPSTracker(Suivre_TrackOrder_Activity.this);
        cusLat = gps.getLatitude();
        cusLon = gps.getLongitude();
        System.out.println("Latitude Track  " + cusLat);
        System.out.println("Longitude Track " + cusLon);

    }

    private void getBundleData() {

        b1 = getIntent().getExtras();
        if (b1 != null) {
            order_id = b1.getString("order_id");
        }
    }


    public void callWebService() {
        if (Utils.isConnected(getApplicationContext())) {

            try {
                getTrackingDetails();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }

    public void getTrackingDetails() {
        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        String userid = GetUserId.getUserInfo(Suivre_TrackOrder_Activity.this);
        String usertoken = GetUserId.getUserToken(Suivre_TrackOrder_Activity.this);

        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        OrderTrackingBean orderTrackingBean = new OrderTrackingBean();
        orderTrackingBean.setOrder_id(order_id);

        Call<ResponseBody> resultCall = loadInterface.trackOrder(userid, usertoken, orderTrackingBean);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!" + response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@" + responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@" + jsonObject22);

                            String status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {
                                String result = jsonObject22.getString("result");

                                JSONObject jsonObject33 = new JSONObject(result);

                                destination_lat = Double.parseDouble(jsonObject33.getString("destination_lat"));
                                destination_lng = Double.parseDouble(jsonObject33.getString("destination_lng"));

                                driverlat = Double.parseDouble(jsonObject33.getString("driver_lat"));
                                driverlon = Double.parseDouble(jsonObject33.getString("driver_lng"));
                                String str_ord = jsonObject33.getString("orderStatus");

                                String orderStatus=str_ord.replace("\u00e9","");

                                if(orderStatus=="Accept" || orderStatus.equalsIgnoreCase("Accept") || orderStatus.equalsIgnoreCase("Accepted"))
                                {
                                    orderReceivedTick.setVisibility(View.VISIBLE);
                                    orderReceivedNotTick.setVisibility(View.GONE);

                                }

                                else if(orderStatus=="On the way" || orderStatus.equalsIgnoreCase("On the way") || orderStatus.equalsIgnoreCase("En route"))
                                {

                                    orderReceivedTick.setVisibility(View.VISIBLE);
                                    orderReceivedNotTick.setVisibility(View.GONE);
                                    orderConfirmedTick.setVisibility(View.VISIBLE);
                                    orderConfirmedNotTick.setVisibility(View.GONE);
                                }
                                String driver_img=jsonObject33.getString("driver_image");
                                driver_contact_number  = jsonObject33.getString("driverContact");
                                txt_name.setText(jsonObject33.getString("driverName"));
                                txt_temps_liveration_estimate_delivery_timee.setText(jsonObject33.getString("timeslot")+"");
                                System.out.println("Driver Image  "+driver_img);
                                PicassoTrustAll.getInstance(Suivre_TrackOrder_Activity.this)
                                        .load(driver_img)
                                        .into(img_profile);

                                setMapView();
                            } else {


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(Suivre_TrackOrder_Activity.this, getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });
    }

    public void inti() {

        btn_resume_commandanture_summary = (Button) findViewById(R.id.btn_resume_commandanture_summary);
        title=(CustomTextView)findViewById(R.id.title);
        rr_annulaer_cancel = (RelativeLayout) findViewById(R.id.rr_annulaer_cancel);
        rr_msg = (RelativeLayout) findViewById(R.id.rr_msg);
        rr_back=(RelativeLayout)findViewById(R.id.rr_back);
        loader = (ProgressBar) findViewById(R.id.loader);
        orderReceivedNotTick=(ImageView)findViewById(R.id.orderReceivedNotTick);
        orderReceivedTick=(ImageView)findViewById(R.id.orderReceivedTick);

        orderConfirmedTick=(ImageView)findViewById(R.id.orderConfirmedTick);
        orderConfirmedNotTick=(ImageView)findViewById(R.id.orderConfirmedNotTick);
        rr_call=(RelativeLayout)findViewById(R.id.rr_call);
        txt_name=(CustomTextView)findViewById(R.id.txt_name);
        rr_back=(RelativeLayout)findViewById(R.id.rr_back);
        rr_back.setOnClickListener(this);
        txt_temps_liveration_estimate_delivery_timee=(CustomTextView)findViewById(R.id.txt_temps_liveration_estimate_delivery_timee);
        img_profile=(CircleImageView)findViewById(R.id.img_profile);


        title.setText(getResources().getString(R.string.suivi_de_commende));
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        btn_resume_commandanture_summary.setOnClickListener(this);
        rr_annulaer_cancel.setOnClickListener(this);
        rr_msg.setOnClickListener(this);
        rr_call.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == btn_resume_commandanture_summary) {
            Intent ii = new Intent(Suivre_TrackOrder_Activity.this, OrderDetails.class);
          ii.putExtra("order_id",order_id);
            startActivity(ii);
        }
        if (v == rr_annulaer_cancel) {

            open_Order_Confrm_cancleannnulerdialog();
        }
        if (v == rr_msg) {
            Intent ii = new Intent(Suivre_TrackOrder_Activity.this, Chat_Bavarder_Activity.class);
            ii.putExtra("order_id",order_id);

            startActivity(ii);

        }
        if(v==rr_call)
        {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:"+driver_contact_number));
            startActivity(intent);
        }
        if(v==rr_back)
        {
            onBackPressed();
        }
    }
    @Override
    public void onBackPressed() {
        finish();

        Intent ii=new Intent(Suivre_TrackOrder_Activity.this, MyOrders.class);
        startActivity(ii);
    }
    public void open_Order_Confrm_cancleannnulerdialog()
    {
        order_cancel_annulercnfrm_dlg=new Dialog(Suivre_TrackOrder_Activity.this);

        order_cancel_annulercnfrm_dlg.getWindow().getAttributes().windowAnimations = R.style.MyAlertDialogStyle;
        // Include dialog.xml file
        order_cancel_annulercnfrm_dlg.setContentView(R.layout.annuler_retur_cancel_dlg);
        order_cancel_annulercnfrm_dlg.show();
      Button  btn_etour=(Button) order_cancel_annulercnfrm_dlg.findViewById(R.id.btn_etour);
      //  btn_etour.setOnClickListener(this);
        btn_etour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                order_cancel_annulercnfrm_dlg.dismiss();

                callWebServicecacelOrder();

            }
        });


      ImageView  img_order_cnfrm=(ImageView)order_cancel_annulercnfrm_dlg.findViewById(R.id.img_order_cnfrm);
   //     img_order_cnfrm.setOnClickListener(this);
        img_order_cnfrm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                order_cancel_annulercnfrm_dlg.dismiss();
            }
        });

     Button   btn_retour=(Button) order_cancel_annulercnfrm_dlg.findViewById(R.id.btn_retour);
      //  btn_retour.setOnClickListener(this);
        btn_retour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                order_cancel_annulercnfrm_dlg.dismiss();
            }
        });
    }
    public void callWebServicecacelOrder() {
        if (Utils.isConnected(getApplicationContext())) {

            try {
                cacelOrder();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }

    public void cacelOrder() {
        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        String userid = GetUserId.getUserInfo(Suivre_TrackOrder_Activity.this);
        String usertoken = GetUserId.getUserToken(Suivre_TrackOrder_Activity.this);

        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        OrderDetailsBean orderDetailsBean = new OrderDetailsBean();
        orderDetailsBean.setOrder_id(order_id);
        orderDetailsBean.setUserid(userid);

        Call<ResponseBody> resultCall = loadInterface.userCancelOrder(orderDetailsBean);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!" + response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@" + responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@" + jsonObject22);

                            String status = jsonObject22.getString("error");
                            String errorMessage = jsonObject22.getString("errorMessage");

                            if (status.equalsIgnoreCase("false")) {
Toast.makeText(getApplicationContext(),errorMessage,Toast.LENGTH_SHORT).show();
Intent ii=new Intent(Suivre_TrackOrder_Activity.this,MyOrders.class);
startActivity(ii);

                            } else {
                                Toast.makeText(getApplicationContext(),errorMessage,Toast.LENGTH_SHORT).show();


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(Suivre_TrackOrder_Activity.this, getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        if (mMap != null) {
            mMap.clear();
        }
        mMap = googleMap;


       // setMapView();

    }

    private void setMapView() {
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.getUiSettings().setScrollGesturesEnabled(true);
        Log.e("LATLONG", "Customer: " + cusLat + " long : " + cusLon);
        Log.e("LATLONG", "Restaurant: " + destiLatLng + " long : " + destination_lng);

        originLatLng = new LatLng(cusLat, cusLon);
        destiLatLng = new LatLng(destination_lat, destination_lng);
        driverLatLng = new LatLng(driverlat, driverlon);

        Bitmap restaurantIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.deliver_icon);
        Bitmap resizedRestaurantIcon = Bitmap.createScaledBitmap(restaurantIcon, 50, 65, false);
        BitmapDescriptor restaurantMarker = BitmapDescriptorFactory.fromBitmap(resizedRestaurantIcon);

        Bitmap customerIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.deliver_icon);
        Bitmap resizedCustomerIcon = Bitmap.createScaledBitmap(customerIcon, 50, 65, false);
        BitmapDescriptor customerMarker = BitmapDescriptorFactory.fromBitmap(resizedCustomerIcon);

        Bitmap driverIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.truck_icon);
        Bitmap resizedDriverIcon = Bitmap.createScaledBitmap(driverIcon, 50, 90, false);
        BitmapDescriptor driverMarker = BitmapDescriptorFactory.fromBitmap(resizedDriverIcon);


        MarkerOptions markerOptions2 = new MarkerOptions().position(destiLatLng).draggable(false).visible(true).icon(restaurantMarker);
        MarkerOptions markerOptions1 = new MarkerOptions().position(driverLatLng).draggable(false).visible(true).icon(driverMarker);

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return true;
            }
        });

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(cusLat, cusLon), 17.0f));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(destination_lat, destination_lng), 17.0f));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(driverlat, driverlon), 17.0f));

        mMap.addMarker(markerOptions2);
        mMap.addMarker(markerOptions1);
//        System.out.println("Origin Latitide  "+originLatLng);
//        System.out.println("OriginDriver Latitide  "+originLatLng);

        String url = getDirectionsUrl(driverLatLng, destiLatLng);

        DownloadTask downloadTask = new DownloadTask();

// Start downloading json data from Google Directions API
        downloadTask.execute(url);


    }


    private String getDirectionsUrl(LatLng origin, LatLng dest) {


        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
       // String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
        String url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + origin.latitude + "," + origin.longitude + "&destination=" + dest.latitude + "," + dest.longitude + "&key=" +"AIzaSyCN8uO3iBy6K92NYOFIaLFrIfNJ7X_A8Xc";
        return url;
    }


    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exceptiondownloadingurl", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(5);
                lineOptions.color(Color.RED);
            }

            try {
                mMap.addPolyline(lineOptions);

            }
catch (Exception e)
{
    System.out.println("Exception  "+e);
}
            // Drawing polyline in the Google Map for the i-th route
        }
    }
}