package com.meetch.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.messaging.FirebaseMessaging;
import com.meetch.MainActivity;
import com.meetch.R;
import com.meetch.apiclient.ApiClient;
import com.meetch.bean.SignInResponseModelResponse.SignInResponseModel;
import com.meetch.bean.loginmodel.LoginModel;
import com.meetch.config.Config;
import com.meetch.constant.Utils;
import com.meetch.rectrofit.LoadInterface;
import com.meetch.storage.MySharedPref;
import com.meetch.utils.CustomEditTextRegular;
import com.meetch.utils.CustomTextViewRegular;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    LinearLayout ll_register;
    Button btn_login;
CustomTextViewRegular txt_forgptpassword;
SignInResponseModel signInResponseModel;
CustomEditTextRegular txt_email,txt_password;
ProgressBar loader;
    MySharedPref sp=new MySharedPref();
    String fcm_id="";
    private static BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getFirebaseid();
        displayFirebaseRegId(getApplicationContext());

        inti();
    }

    private void getFirebaseid() {

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {

                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId(getApplicationContext());

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {

                    String message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                }
            }
        };

        displayFirebaseRegId(getApplicationContext());
    }

    public  void inti(){
        ll_register=(LinearLayout)findViewById(R.id.ll_register);
        btn_login=(Button)findViewById(R.id.btn_login);
        txt_forgptpassword=(CustomTextViewRegular)findViewById(R.id.txt_forgptpassword);
        txt_email=(CustomEditTextRegular)findViewById(R.id.txt_email);

        txt_password=(CustomEditTextRegular)findViewById(R.id.txt_password);

        loader=(ProgressBar)findViewById(R.id.loader);
        ll_register.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        txt_forgptpassword.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v==ll_register){
            Intent intent = new Intent(this, RegisterActivity.class);
            startActivity(intent);
        }
        if (v==btn_login){

hideKeyboard(LoginActivity.this);

        validate();
        }
        if(v==txt_forgptpassword)
        {
            Intent intent = new Intent(this, ForgotPassword_Activity.class);
            startActivity(intent);
        }

    }
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void validate() {

        boolean isError=false;
        String str_email_address=txt_email.getText().toString();
        String str_password=txt_password.getText().toString();


        if(str_email_address==null || str_email_address.equalsIgnoreCase("") || str_email_address.equalsIgnoreCase("null"))
        {
            isError=true;
            txt_email.setError(getResources().getString(R.string.ce_champ_est_requis));


        }
        else if(!isValidEmail(str_email_address)){

            isError=true;
            txt_email.setError(getResources().getString(R.string.sil_vous_plaite_entr_eml_valid));

        }
        else
        {
            txt_email.setError(null);
        }

        if(str_password==null || str_password.equalsIgnoreCase("") || str_password.equalsIgnoreCase("null"))
        {
            isError=true;
            txt_password.setError(getResources().getString(R.string.ce_champ_est_requis));


        }
        else
        {
            txt_password.setError(null);
        }


        if(!isError)
        {
callWebService();
        }

    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
    public void callWebService()

    {
        if(Utils.isConnected(getApplicationContext()))
        {

            try {
                login_User();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }

    public String displayFirebaseRegId(Context context) {


        fcm_id=sp.getData(context,"fcmtoken","null");
        Log.e("FCM Id",fcm_id);
        System.out.println("Firebase Id at Login is@@@"+fcm_id);
        return  fcm_id;
    }
    @Override
    public void onBackPressed() {

        Intent ii=new Intent(LoginActivity.this,MainActivity.class);
        startActivity(ii);
    }


    private void login_User()  {


        loader.setVisibility(View.VISIBLE);


        System.out.println("API EmailId@@@"+txt_email.getText().toString());
        System.out.println("API Password@@@"+txt_password.getText().toString());

   /*     LoginModel loginModel = new LoginModel();
        loginModel.setUsername(txt_email.getText().toString());
        loginModel.setPassword(txt_password.getText().toString());
        loginModel.setDevice_token("");
               resultCall = loadInterface.user_login(loginModel);

        */
        Retrofit retrofit= ApiClient.getClient(getApplicationContext());
        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        System.out.println("Firebase Login Click "+fcm_id);

        LoginModel loginModel = new LoginModel();
        loginModel.setUsername(txt_email.getText().toString());
        loginModel.setPassword(txt_password.getText().toString());
        loginModel.setDevice_token(fcm_id);
        loginModel.setDevice_type("android");
        Call<SignInResponseModel> resultCall = loadInterface.user_login(loginModel);

        resultCall.enqueue(new Callback<SignInResponseModel>() {

            @Override
            public void onResponse(Call<SignInResponseModel> call, retrofit2.Response<SignInResponseModel> response) {


                signInResponseModel = response.body();

                System.out.println("Status Response is!!!"+signInResponseModel.getErrorMessage());

                if (response.isSuccessful()) {
                    loader.setVisibility(View.GONE);

                    try {

                        boolean responedata = signInResponseModel.getError();

                        if(responedata==true)
                        {
                           String errormessage= signInResponseModel.getErrorMessage();

                           Toast.makeText(getApplicationContext(),errormessage+"",Toast.LENGTH_SHORT).show();
                        }
                        else if(responedata==false)
                        {
                            System.out.println("Status UserDetials is!!!"+signInResponseModel.getUserdetails());


                            sp.saveData(getApplicationContext(),"ldata",signInResponseModel.getUserdetails()+"");
                            sp.saveData(getApplicationContext(),"token",signInResponseModel.getAccessToken()+"");
                            sp.saveData(getApplicationContext(),"user_id",signInResponseModel.getUserdetails().getId()+"");
                            sp.saveData(getApplicationContext(),"email",signInResponseModel.getUserdetails().getEmail()+"");

                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                        }



                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                    loader.setVisibility(View.GONE);

                }
//

            }

            @Override
            public void onFailure(Call<SignInResponseModel> call, Throwable t) {

                loader.setVisibility(View.GONE);

                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });

    }

}
