package com.meetch.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.meetch.R;
import com.meetch.apiclient.ApiClient;
import com.meetch.bean.forgotpassword_model.ForgotPasswordModel;
import com.meetch.bean.forgotpassword_model.ForgotPasswordResponse;
import com.meetch.constant.Utils;
import com.meetch.rectrofit.LoadInterface;
import com.meetch.utils.CustomEditTextRegular;

import org.json.JSONException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

public class ForgotPassword_Activity extends AppCompatActivity implements View.OnClickListener{
    Button btn_ok;

    ForgotPasswordResponse forgotPasswordResponse;
    CustomEditTextRegular txt_email;
    ProgressBar loader;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password);
        inti();
    }

    public  void inti(){
        btn_ok=(Button)findViewById(R.id.btn_ok);
        txt_email=(CustomEditTextRegular)findViewById(R.id.txt_email);


        loader=(ProgressBar)findViewById(R.id.loader);
        btn_ok.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v==btn_ok){



            validate();
        }

    }
    private void validate() {

        boolean isError=false;
        String str_email_address=txt_email.getText().toString();


        if(str_email_address==null || str_email_address.equalsIgnoreCase("") || str_email_address.equalsIgnoreCase("null"))
        {
            isError=true;
            txt_email.setError(getResources().getString(R.string.ce_champ_est_requis));


        }
        else if(!isValidEmail(str_email_address)){

            isError=true;
            txt_email.setError(getResources().getString(R.string.sil_vous_plaite_entr_eml_valid));

        }
        else
        {
            txt_email.setError(null);
        }




        if(!isError)
        {
            hideKeyboard(ForgotPassword_Activity.this);
            callWebService();
        }

    }
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
    public void callWebService()

    {
        if(Utils.isConnected(getApplicationContext()))
        {

            try {
                forgotpassword_User();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }

    private void forgotpassword_User() throws JSONException {


        loader.setVisibility(View.VISIBLE);


        System.out.println("API EmailId@@@"+txt_email.getText().toString());


        Retrofit retrofit= ApiClient.getClient(getApplicationContext());
        LoadInterface loadInterface = retrofit.create(LoadInterface.class);


        ForgotPasswordModel forgotPasswordModel = new ForgotPasswordModel();
        forgotPasswordModel.setEmail(txt_email.getText().toString());
        Call<ForgotPasswordResponse> resultCall = loadInterface.user_forgotpassword(forgotPasswordModel);

        resultCall.enqueue(new Callback<ForgotPasswordResponse>() {

            @Override
            public void onResponse(Call<ForgotPasswordResponse> call,
                                   retrofit2.Response<ForgotPasswordResponse> response) {


                forgotPasswordResponse = response.body();

                System.out.println("Status Response Forgot is!!!"+forgotPasswordResponse.getErrorMessage());

                System.out.println("Status Response Forgot is!!!"+forgotPasswordResponse.getError());

                if (response.isSuccessful()) {
                    loader.setVisibility(View.GONE);

                    try {

                        String responedata = forgotPasswordResponse.getError();

                        if(responedata.equalsIgnoreCase("true"))
                        {
                            String errormessage= forgotPasswordResponse.getErrorMessage();

                            Toast.makeText(getApplicationContext(),errormessage+"",Toast.LENGTH_SHORT).show();
                        }
                        else if(responedata.equalsIgnoreCase("false"))
                        {
                            System.out.println("Status UserDetials is!!!"+forgotPasswordResponse.getErrorMessage());
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.pls_chk_eml),Toast.LENGTH_SHORT).show();

                            Intent intent = new Intent(ForgotPassword_Activity.this, LoginActivity.class);
                            startActivity(intent);
                        }



                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                    loader.setVisibility(View.GONE);

                }
//

            }

            @Override
            public void onFailure(Call<ForgotPasswordResponse> call, Throwable t) {

                loader.setVisibility(View.GONE);

                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });

    }
}
