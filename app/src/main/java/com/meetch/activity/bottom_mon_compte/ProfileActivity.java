package com.meetch.activity.bottom_mon_compte;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.meetch.MainActivity;
import com.meetch.R;
import com.meetch.apiclient.ApiClient;
import com.meetch.bean.singlecasedeatial.Singlecasemodel;
import com.meetch.constant.GetUserId;
import com.meetch.constant.Utils;
import com.meetch.rectrofit.LoadInterface;
import com.meetch.utils.CustomTextView;
import com.meetch.utils.CustomTextViewRegular;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout rr_edt_le_profil,rr_changer_le_mot_de_passe;
    CustomTextView modifier,title;
    ProgressBar loader;
    String userid,token;
    CustomTextViewRegular txt_username,txt_mobnumber,txt_email,txt_address;
CircleImageView profile_icon;
RelativeLayout rr_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);
        userid= GetUserId.getUserInfo(getApplicationContext());
        token= GetUserId.getUserToken(getApplicationContext());
        Log.e("UserId",userid+"");
        Log.e("UserToken",token+"");

        inti();
    }

    public  void  inti(){
        rr_edt_le_profil=(RelativeLayout)findViewById(R.id.rr_edt_le_profil);
        rr_changer_le_mot_de_passe=(RelativeLayout)findViewById(R.id.rr_changer_le_mot_de_passe);
        loader=(ProgressBar) findViewById(R.id.loader);
        txt_username=(CustomTextViewRegular)findViewById(R.id.txt_username);
        txt_mobnumber=(CustomTextViewRegular)findViewById(R.id.txt_mobnumber);
        txt_email=(CustomTextViewRegular)findViewById(R.id.txt_email);
        txt_address=(CustomTextViewRegular)findViewById(R.id.txt_address);
        profile_icon=(CircleImageView)findViewById(R.id.profile_icon);

        title=(CustomTextView)findViewById(R.id.title);
        title.setText(getResources().getString(R.string.profil));

        rr_back=(RelativeLayout)findViewById(R.id.rr_back);
        rr_back.setOnClickListener(this);

        modifier=(CustomTextView)findViewById(R.id.modifier);
        modifier.setOnClickListener(this);
        rr_edt_le_profil.setOnClickListener(this);
        rr_changer_le_mot_de_passe.setOnClickListener(this);

        callWebService();

    }
    public void callWebService()

    {
        if(Utils.isConnected(getApplicationContext()))
        {

            try {
                getProfile();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }
  public void getProfile()
    {
        loader.setVisibility(View.VISIBLE);
        Retrofit retrofit= ApiClient.getClient(getApplicationContext());

        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        // System.out.println("API EmailId@@@"+email_id.getText().toString());
        Singlecasemodel singlecasemodel = new Singlecasemodel();
        singlecasemodel.setUserid(userid);
        Call<ResponseBody> resultCall = loadInterface.get_profile_detail(userid,token,singlecasemodel);
        resultCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                System.out.println("Status Response is!!!"+response);
                if (response.isSuccessful()) {
                    try {
                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonobject1 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonobject1);
                            String status22 = jsonobject1.getString("error");
                            System.out.println("Status is@@@"+status22);

                            if (status22.equalsIgnoreCase("false")) {
                                loader.setVisibility(View.GONE);
                                String usersDetails=jsonobject1.getString("usersDetails");
                                System.out.println("ResultGetProfile***"+usersDetails);
                                JSONObject jsonObject=new JSONObject(usersDetails);
                                txt_username.setText(jsonObject.getString("username"));
                                txt_mobnumber.setText(jsonObject.getString("telephone"));
                                txt_email.setText(jsonObject.getString("email"));
                                txt_address.setText(jsonObject.getString("address"));

                                Picasso.with(ProfileActivity.this)
                                        .load(jsonObject.getString("fullimage"))
                                        .placeholder(R.drawable.progress_animation2)
                                        .error(getResources().getDrawable(R.drawable.nouserimg))
                                        .into(profile_icon);
                            }
                          else   if (status22.equalsIgnoreCase("true")) {
                                loader.setVisibility(View.GONE);
                                String message = jsonobject1.getString("errorMessage");
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loader.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Veuillez vérifier la connexion ", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {

        finish();
        Intent ii=new Intent(ProfileActivity.this, MainActivity.class);
        ii.putExtra("profile","profile");
        startActivity(ii);

    }

    @Override
    public void onClick(View v) {


        if(v == rr_edt_le_profil)
        {
            finish();
            Intent ii=new Intent(ProfileActivity.this,Edit_le_profile_Activity.class);
            startActivity(ii);
        }
        if(v==modifier)
        {
            finish();
            Intent ii=new Intent(ProfileActivity.this,Edit_le_profile_Activity.class);
            startActivity(ii);
        }

        if(v==rr_changer_le_mot_de_passe)
        {
            finish();
            Intent ii=new Intent(ProfileActivity.this,Changer_Password_Activity.class);
            startActivity(ii);
        }

            if(v==rr_back){

                onBackPressed();
            }

    }
}
