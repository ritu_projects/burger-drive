package com.meetch.activity.bottom_mon_compte;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.meetch.R;
import com.meetch.activity.LoginActivity;
import com.meetch.bean.changepasswordbean.ChangePasswordBean;
import com.meetch.constant.Constants;
import com.meetch.constant.GetUserId;
import com.meetch.constant.Utils;
import com.meetch.rectrofit.LoadInterface;
import com.meetch.storage.MySharedPref;
import com.meetch.utils.CustomEditTextRegular;
import com.meetch.utils.CustomTextView;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Changer_Password_Activity extends AppCompatActivity implements View.OnClickListener {

    Button btn_save;
    CustomEditTextRegular mot_de_passe_actuel,mobile_num,confirmer_le_nouveau_mot_de_passe;
ProgressBar loader;
    String  useremail;
    RelativeLayout rr_back;
    CustomTextView title;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.changer_password_activity);
        useremail= GetUserId.getUserEmail(getApplicationContext());
        inti();
    }

    public  void  inti(){
        mot_de_passe_actuel=(CustomEditTextRegular)findViewById(R.id.mot_de_passe_actuel);
        mobile_num=(CustomEditTextRegular)findViewById(R.id.mobile_num);
        confirmer_le_nouveau_mot_de_passe=(CustomEditTextRegular)findViewById(R.id.confirmer_le_nouveau_mot_de_passe);
        loader=(ProgressBar)findViewById(R.id.loader);

        title=(CustomTextView)findViewById(R.id.title);
        title.setText(getResources().getString(R.string.changer_le_mot_de_passe));

        rr_back=(RelativeLayout)findViewById(R.id.rr_back);
        rr_back.setOnClickListener(this);

        btn_save=(Button)findViewById(R.id.btn_save);
        btn_save.setOnClickListener(this);


    }

    @Override
    public void onBackPressed() {

        finish();
        Intent ii=new Intent(Changer_Password_Activity.this,ProfileActivity.class);
        startActivity(ii);
    }

    @Override
    public void onClick(View v) {

        if(v==btn_save)
        {
            validate();
        }

        if(v==rr_back){

            onBackPressed();
        }
    }

    private void validate() {
//        Intent ii=new Intent(Changer_Password_Activity.this, LoginActivity.class);
//        startActivity(ii);

        boolean isError=false;
        String str_password=mot_de_passe_actuel.getText().toString();
        String str_newpassword=mobile_num.getText().toString();
        String str_newconfirmpassword=  confirmer_le_nouveau_mot_de_passe.getText().toString();

        if(str_password==null || str_password.equalsIgnoreCase("") || str_password.equalsIgnoreCase("null"))
        {
            isError=true;
            mot_de_passe_actuel.setError(getResources().getString(R.string.ce_champ_est_requis));


        }

        else
        {
            mot_de_passe_actuel.setError(null);
        }

        if(str_newpassword==null || str_newpassword.equalsIgnoreCase("") || str_newpassword.equalsIgnoreCase("null"))
        {
            isError=true;
            mobile_num.setError(getResources().getString(R.string.ce_champ_est_requis));


        }
        else
        {
            mobile_num.setError(null);
        }
        if(str_newconfirmpassword==null || str_newconfirmpassword.equalsIgnoreCase("") || str_newconfirmpassword.equalsIgnoreCase("null"))
        {
            isError=true;
            confirmer_le_nouveau_mot_de_passe.setError(getResources().getString(R.string.ce_champ_est_requis));


        }
        else
        {
            confirmer_le_nouveau_mot_de_passe.setError(null);
        }
        if(!str_newconfirmpassword.equalsIgnoreCase(str_newpassword))
        {
            isError=true;
            confirmer_le_nouveau_mot_de_passe.setError(getResources().getString(R.string.confrm_pass_sameas_newpass));


        }
        else
        {
            confirmer_le_nouveau_mot_de_passe.setError(null);
        }

        if(!isError)
        {
            callWebService();
        }
    }

    public void callWebService()

    {
        if(Utils.isConnected(getApplicationContext()))
        {

            try {
                change_password();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }
    private void change_password() {
        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        ChangePasswordBean changePasswordBean = new ChangePasswordBean();
        changePasswordBean.setEmail(useremail);
        changePasswordBean.setPassword(confirmer_le_nouveau_mot_de_passe.getText().toString());
        changePasswordBean.setOld_password(mot_de_passe_actuel.getText().toString());


        Call<ResponseBody> resultCall = loadInterface.uchangePassword(changePasswordBean);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {
                                MySharedPref sp=new MySharedPref();

                                sp.saveData(getApplicationContext(),"ldata","");
                                sp.saveData(getApplicationContext(),"token","");
                                sp.saveData(getApplicationContext(),"user_id","");
                                sp.saveData(getApplicationContext(),"email","");


                                Intent ii=new Intent(Changer_Password_Activity.this, LoginActivity.class);
                                startActivity(ii);


                            }

                            else
                            {
                                String  errorMessage = jsonObject22.getString("errorMessage");
                                Toast.makeText(getApplicationContext(),errorMessage,Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });

    }
    }
