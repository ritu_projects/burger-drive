package com.meetch.activity.bottom_mon_compte;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.meetch.R;
import com.meetch.apiclient.ApiClient;
import com.meetch.bean.singlecasedeatial.Singlecasemodel;
import com.meetch.constant.Constants;
import com.meetch.constant.GetUserId;
import com.meetch.constant.Utils;
import com.meetch.rectrofit.LoadInterface;
import com.meetch.storage.MySharedPref;
import com.meetch.utils.CustomEditTextRegular;
import com.meetch.utils.CustomTextView;
import com.squareup.picasso.Picasso;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickCancel;
import com.vansuita.pickimage.listeners.IPickResult;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class Edit_le_profile_Activity extends AppCompatActivity implements View.OnClickListener {
Button btn_save;
    CustomEditTextRegular txt_username,txt_mobnumber,txt_email,txt_address;
    CustomTextView title;
    CircleImageView profile_icon;
    ProgressBar loader;
    String userid,token;
MySharedPref sharedPref=new MySharedPref();
RelativeLayout image_layout;
    String bitmat_image,path;
    File file_image;
    RelativeLayout rr_back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userid= GetUserId.getUserInfo(getApplicationContext());
        token= GetUserId.getUserToken(getApplicationContext());
        setContentView(R.layout.activity_edit__profile);
        inti();
    }

    public  void  inti(){
        btn_save=(Button)findViewById(R.id.btn_save);

        btn_save.setOnClickListener(this);
        title=(CustomTextView)findViewById(R.id.title);
        title.setText(getResources().getString(R.string.editer_le_profil));

        image_layout=(RelativeLayout)findViewById(R.id.image_layout);
        txt_username=(CustomEditTextRegular)findViewById(R.id.txt_username);
        txt_mobnumber=(CustomEditTextRegular)findViewById(R.id.txt_mobnumber);
        txt_email=(CustomEditTextRegular)findViewById(R.id.txt_email);
        txt_address=(CustomEditTextRegular)findViewById(R.id.txt_address);
        profile_icon=(CircleImageView)findViewById(R.id.profile_icon);
        loader=(ProgressBar)findViewById(R.id.loader);
        rr_back=(RelativeLayout)findViewById(R.id.rr_back);
        rr_back.setOnClickListener(this);
        image_layout.setOnClickListener(this);
        callWebService();
    }

    @Override
    public void onBackPressed() {

        finish();
        Intent ii=new Intent(Edit_le_profile_Activity.this,ProfileActivity.class);
        startActivity(ii);
    }
    public void callWebService()

    {
        if(Utils.isConnected(getApplicationContext()))
        {

            try {
                getProfile();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }
    public void photoGalleryCamera() {



        final PickImageDialog dialog = PickImageDialog.build(new PickSetup());
        dialog.setOnPickCancel(new IPickCancel() {

            @Override
            public void onCancelClick() {
                dialog.dismiss();
            }
        })

                .setOnPickResult(new IPickResult() {
                    @Override
                    public void onPickResult(PickResult r) {

                        if (r.getError() == null) {

                            Bitmap bitmap=r.getBitmap();
                            path = r.getPath();
                            file_image = new File(path);

                            System.out.println("Path is###"+path);
                            String bitmap_str=BitMapToString(bitmap);

                            sharedPref=new MySharedPref();
                            System.out.println("Image OnClick Bitmap###"+bitmap_str);
                            sharedPref.saveData(Edit_le_profile_Activity.this,"pro_image",""+bitmap_str);
                            profile_icon.setImageBitmap(bitmap);



                        } else {
                            //Handle possible errors
                            //TODO: do what you have to do with r.getError();
                            Toast.makeText(Edit_le_profile_Activity.this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
                        }

                    }
                }).show(Edit_le_profile_Activity.this);
    }
    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b=baos.toByteArray();
        String temp= Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }
    private void validate() {

        boolean isError=false;
        String str_username=txt_username.getText().toString();
        String str_email_address=txt_email.getText().toString();
        String str_phonenumber=txt_mobnumber.getText().toString();

        if(str_username==null || str_username.equalsIgnoreCase("") || str_username.equalsIgnoreCase("null"))
        {
            isError=true;
            txt_username.setError(getResources().getString(R.string.ce_champ_est_requis));


        }
        else
        {
            txt_username.setError(null);
        }
        if(str_email_address==null || str_email_address.equalsIgnoreCase("") || str_email_address.equalsIgnoreCase("null"))
        {
            isError=true;
            txt_email.setError(getResources().getString(R.string.ce_champ_est_requis));


        }

        else if(!isValidEmail(str_email_address)){

            isError=true;
            txt_email.setError(getResources().getString(R.string.sil_vous_plaite_entr_eml_valid));

        }
        else
        {
            txt_email.setError(null);
        }




        if(str_phonenumber==null || str_phonenumber.equalsIgnoreCase("") || str_phonenumber.equalsIgnoreCase("null"))
        {
            isError=true;
            txt_mobnumber.setError(getResources().getString(R.string.ce_champ_est_requis));


        }
        else
        {
            txt_mobnumber.setError(null);
        }

        if(!isError)
        {
            hideSoftKeyboard();
            callWebService_update();
        }

    }
    public void callWebService_update()

    {
        if(Utils.isConnected(getApplicationContext()))
        {

            try {
                Update_profile_AsnyTask2 update_profile_asnyTask = new Update_profile_AsnyTask2();
                update_profile_asnyTask.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }

    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }


    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
    public void getProfile()
    {
        loader.setVisibility(View.VISIBLE);
        Retrofit retrofit= ApiClient.getClient(getApplicationContext());

        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        // System.out.println("API EmailId@@@"+email_id.getText().toString());
        Singlecasemodel singlecasemodel = new Singlecasemodel();
        singlecasemodel.setUserid(userid);
        Call<ResponseBody> resultCall = loadInterface.get_profile_detail(userid,token,singlecasemodel);
        resultCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                System.out.println("Status Response is!!!"+response);
                if (response.isSuccessful()) {
                    try {
                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {
                            JSONObject jsonobject1 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonobject1);
                            String status22 = jsonobject1.getString("error");
                            System.out.println("Status is@@@"+status22);

                            if (status22.equalsIgnoreCase("false")) {
                                loader.setVisibility(View.GONE);
                                String usersDetails=jsonobject1.getString("usersDetails");
                                System.out.println("ResultGetProfile***"+usersDetails);
                                JSONObject jsonObject=new JSONObject(usersDetails);
                                System.out.println("UserName   "+jsonObject.getString("username"));
                                txt_username.setText(jsonObject.getString("username"));
                                txt_mobnumber.setText(jsonObject.getString("telephone"));
                                txt_email.setText(jsonObject.getString("email"));
                                txt_address.setText(jsonObject.getString("address"));
                           //   bitmat_image=  jsonObject.getString("fullimage");
                                Picasso.with(Edit_le_profile_Activity.this)
                                        .load(jsonObject.getString("fullimage"))
                                        .placeholder(R.drawable.progress_animation2)
                                        .error(getResources().getDrawable(R.drawable.nouserimg))
                                        .into(profile_icon);






                            }
                            else   if (status22.equalsIgnoreCase("true")) {
                                loader.setVisibility(View.GONE);
                                String message = jsonobject1.getString("errorMessage");
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loader.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Veuillez vérifier la connexion ", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private class Update_profile_AsnyTask2 extends AsyncTask<String, Void, String> {

        String Msg, web_response = "",message;
        boolean iserror = false;

        JSONObject jsonobject;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();


            try {

                loader.setVisibility(VISIBLE);

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... params) {

            try {

                MultipartEntity m = new MultipartEntity();

                System.out.println("S_Userid1" + userid);
                m.addPart("user_id", new StringBody(userid));

                System.out.println("username" +txt_username.getText().toString());
                m.addPart("username", new StringBody(txt_username.getText().toString()));


                System.out.println("email" +txt_email.getText().toString());
                m.addPart("email", new StringBody(txt_email.getText().toString()));



                System.out.println("telephone ###" +txt_mobnumber.getText().toString());
                m.addPart("telephone", new StringBody(txt_mobnumber.getText().toString()));

                System.out.println("Address###" +txt_address.getText().toString());
                m.addPart("address", new StringBody(txt_address.getText().toString()));

                if(file_image!=null) {
                    m.addPart("image", new FileBody(file_image));
                }

                else {
                //    m.addPart("image", new StringBody(bitmat_image));

                }
                web_response = multipost(Constants.PROFILE_UPDATE_URL, m);

                Log.d("EditProfileActivity", "Response :"+web_response);
                Log.d("web_response", "" + web_response);
                System.out.println("Web Response Update" + web_response);
                jsonobject = new JSONObject(web_response);
                Msg = jsonobject.getString("error");
                message = jsonobject.getString("errorMessage");

                Log.d("Get Msg", "" + Msg);

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("#####Exception=" + e);
                iserror = true;
            }

            return web_response;
        }

        protected void onPostExecute(String result1) {
            super.onPostExecute(result1);
            Log.d("---Msg---", "--------------- " + Msg + " ------------------------");
            loader.setVisibility(GONE);
            try {

                if (Msg.equals("false")) {

                    Toast.makeText(Edit_le_profile_Activity.this,message,Toast.LENGTH_SHORT).show();
                    finish();
                    Intent intent=new Intent(Edit_le_profile_Activity.this,ProfileActivity.class);
                    startActivity(intent);

                }
                else {
                    Toast.makeText(Edit_le_profile_Activity.this, message, Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private  String multipost(String urlString, MultipartEntity reqEntity) {

        try {
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setRequestMethod("POST");
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);

            conn.setRequestProperty("User-ID",userid);
            conn.setRequestProperty("token",token);

            conn.addRequestProperty("Content-length", reqEntity.getContentLength()+"");


            conn.setRequestProperty("Connection", "Keep-Alive");

            conn.addRequestProperty(reqEntity.getContentType().getName(), reqEntity.getContentType().getValue());

            OutputStream os = conn.getOutputStream();
            reqEntity.writeTo(conn.getOutputStream());
            os.close();
            conn.connect();

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                return readStream(conn.getInputStream());
            }

        } catch (Exception e) {
            Log.e("MainActivity", "multipart post error " + e + "(" + urlString + ")");
        }
        return null;
    }

    private static String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuilder builder = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return builder.toString();
    }

    @Override
    public void onClick(View v) {
        if(v==btn_save)
        {
//            Intent ii=new Intent(Edit_le_profile_Activity.this,ProfileActivity.class);
//            startActivity(ii);

            validate();
        }

        if(v==image_layout)
        {
            photoGalleryCamera();
        }
        if(v==rr_back){

            onBackPressed();
        }
    }
}
