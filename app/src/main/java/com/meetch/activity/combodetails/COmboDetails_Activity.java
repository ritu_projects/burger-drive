package com.meetch.activity.combodetails;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.meetch.MainActivity;
import com.meetch.R;
import com.meetch.activity.LoginActivity;
import com.meetch.activity.bottom_add_delivery_address_gererladdress.Ajouter_Une_Adresse_De_Livraison_Activity;
import com.meetch.bean.addtocart_combo.Addtocart_Combo_Bean;
import com.meetch.bean.combodetails_bean.ComboDetails_Beann;
import com.meetch.bean.combodetailsparam_bean.ComboDetailsParams;
import com.meetch.bean.getDeliveryAddress_model.GetDeliveryAddressModel;
import com.meetch.bean.singlecasedeatial.Singlecasemodel;
import com.meetch.constant.Constants;
import com.meetch.constant.GetUserId;
import com.meetch.constant.Utils;
import com.meetch.picasso.PicassoTrustAll;
import com.meetch.rectrofit.LoadInterface;
import com.meetch.storage.MySharedPref;
import com.meetch.utils.CustomTextView;
import com.meetch.utils.CustomTextViewRegular;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.view.View.GONE;

public class COmboDetails_Activity extends AppCompatActivity implements View.OnClickListener {
    ProgressBar loader;
    Bundle b1;
    String str_combo_id;
    ComboDetails_Beann comboDetails_beann;
    RecyclerView list_recyl_category;
    ImageView combo_img;
    CustomTextView txt_combo_name;
    RelativeLayout rr_for_add_tocart;
    ArrayList<Integer> arr_quantity,arr_selected_quant;
    MySharedPref sp=new MySharedPref();
    String loggedin;
    GetDeliveryAddressModel getDeliveryAddressModel;
    Add_Gererladdress_Delivery_Address_Adp add_gererladdress_delivery_address_adp;
    Dialog dialog;
    String address_id;
    CustomTextView title,txt_quantity;
    RelativeLayout rr_back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.combodetails);
        getBundleData();
        inti();
    }

    private void getBundleData() {
        b1=getIntent().getExtras();
        if(b1!=null)
        {
            str_combo_id=b1.getString("combo_id");
        }
    }

    private void inti() {
        loader=(ProgressBar)findViewById(R.id.loader);
        combo_img=(ImageView)findViewById(R.id.combo_img);
        txt_combo_name=(CustomTextView)findViewById(R.id.txt_combo_name);
        title=(CustomTextView)findViewById(R.id.title);
        txt_quantity=(CustomTextView)findViewById(R.id.txt_quantity);

        rr_back=(RelativeLayout)findViewById(R.id.rr_back);
        rr_for_add_tocart=(RelativeLayout)findViewById(R.id.rr_for_add_tocart);

        rr_for_add_tocart.setOnClickListener(this);
        rr_back.setOnClickListener(this);
        callWebService();

    }

    public void callWebService()

    {
        if(Utils.isConnected(getApplicationContext()))
        {

            try {
                getSingleComboDetails();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }

    private void getSingleComboDetails() {
        loader.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ComboDetailsParams comboDetailsParams = new ComboDetailsParams();
        comboDetailsParams.setCombo_id(str_combo_id);
        LoadInterface loadInterface = retrofit.create(LoadInterface.class);

        Call<ResponseBody> resultCall = loadInterface.get_comboDetail(comboDetailsParams);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {

                                Gson gson = new Gson();
                                comboDetails_beann = gson.fromJson(responedata, ComboDetails_Beann.class);
                                list_recyl_category=(RecyclerView)findViewById(R.id.list_recyl_category);

                                PicassoTrustAll.getInstance(getApplicationContext())
                                        .load(comboDetails_beann.getList().get(0).getComboImg())
                                        //.resize(520,320)
                                        .error(R.drawable.bnanner)
                                        .into(combo_img);
                                String combo_name= comboDetails_beann.getList().get(0).getComboName();
                                String combo_desc= comboDetails_beann.getList().get(0).getComboDesc();
                                title.setText(comboDetails_beann.getList().get(0).getComboName());
                                txt_quantity.setText(comboDetails_beann.getList().get(0).getComboPrice()+"€");



                                txt_combo_name.setText(combo_desc);
                                String[] separated = combo_desc.split("\\+");

                                if(separated!=null && separated.length>0)
                                {
                                    arr_quantity=new ArrayList<>();
                                    for(int k=0;k<separated.length;k++)
                                    {
                                        String str=separated[k];
                                        String intValue = str.replaceAll("[^0-9]", ""); // returns 123

                                        Integer int_vl= Integer.parseInt(intValue);
                                        System.out.println("Separated Part  "+int_vl);

                                        arr_quantity.add(int_vl);

                                    }
                                }
                                else
                                {
                                    String str=separated[0];

                                    String intValue = str.replaceAll("[^0-9]", ""); // returns 123
                                    Integer int_vl= Integer.parseInt(intValue);
                                    System.out.println("Separated Part  "+int_vl);
                                    arr_quantity=new ArrayList<>();
                                    arr_quantity.add(int_vl);



                                }


                                ComboDetail_ItemAdapter  comboDetail_itemAdapter=new
                                        ComboDetail_ItemAdapter(COmboDetails_Activity.this,
                                        comboDetails_beann.getList().get(0).getProdList(),arr_quantity);
                                list_recyl_category.setLayoutManager(new LinearLayoutManager
                                        (COmboDetails_Activity.this,LinearLayoutManager.VERTICAL,false));
                                list_recyl_category.setAdapter(comboDetail_itemAdapter);
                                list_recyl_category.setHasFixedSize(true);

                                list_recyl_category.setNestedScrollingEnabled(false);





                            }

                            else
                            {
                                //txt_no_cas_outers.setVisibility(VISIBLE);
                                list_recyl_category.setVisibility(GONE);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });

    }

    @Override
    public void onClick(View view) {

        if(view==rr_for_add_tocart)
        {
            loggedin=sp.getData(getApplicationContext(),"ldata","null");

            if(loggedin!=null && !loggedin.equalsIgnoreCase("")  &&
                    !loggedin.equalsIgnoreCase("null")) {

                callWebService_foraddtocart();
            }
            else
            {
                Intent ii=new Intent(COmboDetails_Activity.this, LoginActivity.class);
                startActivity(ii);
            }
        }

        if(view==rr_back)
        {
            onBackPressed();
        }
    }
    @Override
    public void onBackPressed() {

        Intent ii=new Intent(COmboDetails_Activity.this,MainActivity.class);
        startActivity(ii);
    }
    public  void callWebService_foraddtocart()
    {

        if(Utils.isConnected(getApplicationContext()))
        {

            try {
                //getDeliveryAddress();
                validation();
                //  addtoCart();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }

    public  void getDeliveryAddress()
    {

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        String  userid= GetUserId.getUserInfo(getApplicationContext());


        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        Singlecasemodel singlecasemodel = new Singlecasemodel();
        singlecasemodel.setUserid(userid);

        Call<ResponseBody> resultCall = loadInterface.getallDeliveryAddress(singlecasemodel);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {
                                //    addtoCart();

                                Gson gson = new Gson();
                                getDeliveryAddressModel = gson.fromJson(responedata, GetDeliveryAddressModel.class);
                                add_gererladdress_delivery_address_adp=new Add_Gererladdress_Delivery_Address_Adp(COmboDetails_Activity.this,getDeliveryAddressModel.getRecord());


                                showDialogforAddress();


                            }

                            else
                            {
                                Intent ii=new Intent(getApplicationContext(), Ajouter_Une_Adresse_De_Livraison_Activity.class);
                                ii.putExtra("combo_id",str_combo_id);
                                startActivity(ii);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });

    }

    private void validation() {

        boolean isError=false;
arr_selected_quant=new ArrayList<>();
        for(int i=0;i<comboDetails_beann.getList().get(0).getProdList().size();i++)
        {
            Integer sub_totl=0;


            for(int k=0;k<comboDetails_beann.getList().get(0).getProdList().get(i).getProductList().size();k++) {

                        Integer int_txt=comboDetails_beann.getList().get(0).getProdList().get(i).getProductList().get(k).getIndividula_product_quant_byUser();

                sub_totl=sub_totl+int_txt;


                System.out.println("Sub Int  "+sub_totl);


            }
            System.out.println("Sub Total  "+arr_selected_quant.size());
            arr_selected_quant.add(sub_totl);


            int quantum= arr_quantity.get(i);
//           if(sub_totl<quantum)
//            {
//                isError=true;
//                Toast.makeText(getApplicationContext(),getResources().getString(R.string.pls_slct)+comboDetails_beann.getList().get(0).getComboDesc(),Toast.LENGTH_SHORT).show();
//           }

        }
        for(int k=0;k<arr_quantity.size();k++)
        {

            System.out.println("K Selected "+k);
            System.out.println("K arr Quantity"+arr_quantity.get(k));
            System.out.println("K arr SelectedQuantity"+arr_selected_quant.get(k));

            if(arr_quantity.get(k)!=arr_selected_quant.get(k))
            {
                isError=true;

                Toast.makeText(getApplicationContext(),getResources().getString(R.string.pls_slct)+": "+arr_quantity.get(k)+" "
                        +comboDetails_beann.getList().get(0).getProdList().get(k).getCatName(),Toast.LENGTH_SHORT).show();

            }


        }

        if(!isError)
        {

            //getDeliveryAddress();
            addtoCart();

        }








    }

    private void showDialogforAddress() {

        dialog = new Dialog(COmboDetails_Activity.this);
        dialog.setContentView(R.layout.add_gererladdress_delivery_address_activity);

        // set the custom dialog components - text, image and button
        RelativeLayout rr_ajourte_crd = (RelativeLayout) dialog.findViewById(R.id.rr_ajourte_crd);
        rr_ajourte_crd.setVisibility(GONE);

        RecyclerView    add_gererladdress_delivery_recyclerView=(RecyclerView)dialog.findViewById(R.id.add_gererladdress_delivery_recyclerView);
        add_gererladdress_delivery_recyclerView.setVisibility(View.VISIBLE);
        add_gererladdress_delivery_recyclerView.setLayoutManager(new LinearLayoutManager(COmboDetails_Activity.this, LinearLayoutManager.VERTICAL, false));
        add_gererladdress_delivery_recyclerView.setAdapter(add_gererladdress_delivery_address_adp);


        dialog.show();
    }



    public class ComboDetail_ItemAdapter extends RecyclerView.Adapter<ComboDetail_ItemAdapter.ViewHolder> {
        Context context;
        List<ComboDetails_Beann.ComboDetailAll_Bean.ProdList> arrayList;
        ArrayList<Integer> arr_quant_ct;
        String product_base_url;


        public ComboDetail_ItemAdapter(Activity cart_pageActivity,
                                       List<ComboDetails_Beann.ComboDetailAll_Bean.ProdList> arrayList,ArrayList<Integer> arr_quant) {
            this.context=cart_pageActivity;
            this.arrayList=arrayList;
            this.arr_quant_ct=arr_quant;
        }

        public ComboDetail_ItemAdapter(COmboDetails_Activity cOmboDetails_activity, List<ComboDetails_Beann.ComboDetailAll_Bean.ProdList> list) {
            this.context=cOmboDetails_activity;
            this.arrayList=list;

        }

        @NonNull
        @Override
        public ComboDetail_ItemAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.combo_details_item,viewGroup,false);
            ComboDetail_ItemAdapter.ViewHolder viewHolder=new ComboDetail_ItemAdapter.ViewHolder(view);
            return viewHolder;

        }

        @Override
        public void onBindViewHolder(@NonNull ComboDetail_ItemAdapter.ViewHolder viewHolder, final int i) {


            viewHolder.txt_choix.setText(arrayList.get(i).getCatName());


            for(int k=0;k<arrayList.get(i).getProductList().size();k++)
            {
                arrayList.get(i).getProductList().get(k).setIndividula_product_quant_byUser(0);
                arrayList.get(i).getProductList().get(k).setSelected_plus(true);
                arrayList.get(i).getProductList().get(k).setSelected_minus(true);

            }

            ComboProductDetail_ItemAdapter  comboDetail_itemAdapter=new
                    ComboProductDetail_ItemAdapter(COmboDetails_Activity.this,arrayList.get(i).getProductList(),
                    arr_quant_ct.get(i));
           viewHolder.list_recyl_comboproduct.setLayoutManager(new LinearLayoutManager(COmboDetails_Activity.this,LinearLayoutManager.VERTICAL,false));
            viewHolder.list_recyl_comboproduct.setAdapter(comboDetail_itemAdapter);
         viewHolder.list_recyl_comboproduct.setHasFixedSize(true);

            viewHolder.list_recyl_comboproduct.setNestedScrollingEnabled(false);
        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }
        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            CustomTextView txt_choix;
            RecyclerView list_recyl_comboproduct;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                txt_choix=(CustomTextView) itemView.findViewById(R.id.txt_choix);
                list_recyl_comboproduct=(RecyclerView) itemView.findViewById(R.id.list_recyl_comboproduct);
            }
        }
    }

    public class ComboProductDetail_ItemAdapter extends RecyclerView.Adapter<ComboProductDetail_ItemAdapter.ViewHolder> {
        Context context;
        List<ComboDetails_Beann.ComboDetailAll_Bean.ProdList.ProductList> arrayList_combo_product;
        Integer int_val;
        Integer index=0;
        RelativeLayout rr_sub_valve,rr_add_value;



        public ComboProductDetail_ItemAdapter(COmboDetails_Activity cOmboDetails_activity,
                                              List<ComboDetails_Beann.ComboDetailAll_Bean.ProdList.ProductList> list, Integer int_quant_comboproduct) {
            this.context=cOmboDetails_activity;
            this.arrayList_combo_product=list;
            this.int_val=int_quant_comboproduct;

        }

        @NonNull
        @Override
        public ComboProductDetail_ItemAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.combodetails_products_item,viewGroup,false);
            ViewHolder viewHolder=new ViewHolder(view);
            return viewHolder;

        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {


            viewHolder.txt_product_name.setText(arrayList_combo_product.get(i).getName());

            viewHolder.rr_unchecked.setVisibility(GONE);


            //    viewHolder.txt_total_value.setText(index+"");


//            System.out.println("Index is  "+index);
//            System.out.println("Int Value  "+int_val);
            viewHolder.txt_total_value.setText(arrayList_combo_product.get(i).getIndividula_product_quant_byUser()+"");




                System.out.println("Is Selected  Plus  "+arrayList_combo_product.get(i).isSelected_plus());

            System.out.println("Is Selected  Minus  "+arrayList_combo_product.get(i).isSelected_minus());

                viewHolder.rr_unchecked.setVisibility(GONE);
                viewHolder.rr_checked.setVisibility(GONE);
//            viewHolder.txt_plus.setTextColor(context.getResources().getColor(R.color.black));
//            viewHolder.txt_minus.setTextColor(context.getResources().getColor(R.color.black));






             rr_sub_valve.setFocusable(true);
             rr_add_value.setFocusable(true);


            if(arrayList_combo_product.get(i).isSelected_plus())
            {
                viewHolder.txt_plus.setTextColor(context.getResources().getColor(R.color.black));

            }

             if(arrayList_combo_product.get(i).isSelected_plus()==false)
            {
                viewHolder.txt_plus.setTextColor(context.getResources().getColor(R.color.light_gray));

            }
            if(arrayList_combo_product.get(i).isSelected_minus())
            {
                viewHolder.txt_minus.setTextColor(context.getResources().getColor(R.color.black));

            }

             if(arrayList_combo_product.get(i).isSelected_minus()==false)
            {
                viewHolder.txt_minus.setTextColor(context.getResources().getColor(R.color.light_gray));


            }

            rr_add_value.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {



                    if (index == int_val) {
                        viewHolder.txt_plus.setTextColor(context.getResources().getColor(R.color.light_gray));
                        viewHolder.txt_minus.setTextColor(context.getResources().getColor(R.color.black));

                    }
//                        else if(arrayList_combo_product.get(i).getIndividula_product_quant_byUser()==int_val)
//                        {
//
//                        }

                    else {

                        String add = String.valueOf(index + 1);
                        index = Integer.parseInt(add);

//
                            int txt_in = arrayList_combo_product.get(i).getIndividula_product_quant_byUser() + 1;
                            arrayList_combo_product.get(i).setIndividula_product_quant_byUser(txt_in);

                        viewHolder.txt_total_value.setText(add + "");
                        if (index == int_val) {
                            System.out.println("add:::::" + index);
//                                arrayList_combo_product.get(i).setSelected_plus(false);
//                                arrayList_combo_product.get(i).setSelected_minus(true);
                            for(int k=0;k<arrayList_combo_product.size();k++)
                            {
                                arrayList_combo_product.get(k).setSelected_plus(false);

                                if(arrayList_combo_product.get(k).getIndividula_product_quant_byUser()!= 0)
                                {
                                    System.out.println("Quantity  "+arrayList_combo_product.get(k).getIndividula_product_quant_byUser());
                                    arrayList_combo_product.get(k).setSelected_minus(true);
                                }
                                else if(arrayList_combo_product.get(k).getIndividula_product_quant_byUser()== 0)
                                {
                                    System.out.println("Quantity Plus  "+arrayList_combo_product.get(k).getIndividula_product_quant_byUser());
                                    arrayList_combo_product.get(k).setSelected_minus(false);
                                }

                               notifyItemRangeChanged(i, getItemCount());
                                notifyDataSetChanged();


                            }



                        }
                        else
                        {


//                                    arrayList_combo_product.get(i).setSelected_plus(true);
//                                    arrayList_combo_product.get(i).setSelected_minus(true);


                            for(int k=0;k<arrayList_combo_product.size();k++)
                            {
                                arrayList_combo_product.get(k).setSelected_plus(true);
                              notifyItemRangeChanged(i, getItemCount());
                                notifyDataSetChanged();

                            }


                        }

                    }
                }

            });




             rr_sub_valve.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        System.out.println("Index is "+arrayList_combo_product.get(i).getIndividula_product_quant_byUser());


                        if(index == 0)
                        {
                            System.out.println("sub user:::::"+index);

                        }

                        else if(arrayList_combo_product.get(i).getIndividula_product_quant_byUser()==0)
                        {



                        }

                        else {

                            String add = String.valueOf(index - 1);
                            index = Integer.parseInt(add);

                            System.out.println("sub:::::" + index);

                            int txt_in = arrayList_combo_product.get(i).getIndividula_product_quant_byUser() - 1;
                            arrayList_combo_product.get(i).setIndividula_product_quant_byUser(txt_in);

                            viewHolder.txt_total_value.setText(txt_in + "");

                            if (index == 0) {


                                System.out.println("sub index:::::" + index);

                               rr_sub_valve.setFocusable(false);
                            rr_add_value.setFocusable(false);


                                arrayList_combo_product.get(i).setSelected(false);
//
//                                arrayList_combo_product.get(i).setSelected_plus(true);
//                                arrayList_combo_product.get(i).setSelected_minus(false);

                               viewHolder.txt_plus.setTextColor(context.getResources().getColor(R.color.black));
                               viewHolder.txt_minus.setTextColor(context.getResources().getColor(R.color.light_gray));
                                for(int k=0;k<arrayList_combo_product.size();k++)
                                {
                                    arrayList_combo_product.get(k).setSelected_plus(true);
                                    arrayList_combo_product.get(k).setSelected_minus(false);

                                   notifyItemRangeChanged(i, getItemCount());
                                    notifyDataSetChanged();

                                }
                            }

                            else
                            {
//                                arrayList_combo_product.get(i).setSelected_minus(true);
//
//                                arrayList_combo_product.get(i).setSelected_minus(true);

                                for(int k=0;k<arrayList_combo_product.size();k++)
                                {
                                    arrayList_combo_product.get(k).setSelected_plus(true);

                                notifyItemRangeChanged(i, getItemCount());
                                    notifyDataSetChanged();


                                }

                            }


                            //  viewHolder.txt_total_value.setText(add);

                        }

                    }
                });








                //   Drawable new_image= context.getResources().getDrawable(R.drawable.button_unselected_bg);

                viewHolder.rr_unchecked.setVisibility(GONE);
                viewHolder.rr_checked.setVisibility(GONE);





        }

        @Override
        public int getItemCount() {
            return arrayList_combo_product.size();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }
        public class ViewHolder extends RecyclerView.ViewHolder {
            CustomTextView txt_product_name,txt_total_value;
            RelativeLayout rr_unchecked,rr_checked;
            CustomTextView txt_minus,txt_plus;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                txt_product_name=(CustomTextView) itemView.findViewById(R.id.txt_product_name);
                rr_unchecked=(RelativeLayout)itemView.findViewById(R.id.rr_unchecked);
                rr_checked=(RelativeLayout)itemView.findViewById(R.id.rr_checked);
                rr_sub_valve=(RelativeLayout)itemView.findViewById(R.id.rr_sub_valve);
                rr_add_value=(RelativeLayout)itemView.findViewById(R.id.rr_add_value);
                txt_total_value=(CustomTextView)itemView.findViewById(R.id.txt_total_value);
                txt_minus=(CustomTextView)itemView.findViewById(R.id.txt_minus);
                txt_plus=(CustomTextView)itemView.findViewById(R.id.txt_plus);

            }
        }
    }

    private void addtoCart() {

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        String  userid= GetUserId.getUserInfo(getApplicationContext());
        List<Addtocart_Combo_Bean.CartList> cartLists=new ArrayList<>();


        for(int k=0;k<comboDetails_beann.getList().get(0).getProdList().size();k++)
        {
            for(int l=0;l<comboDetails_beann.getList().get(0).getProdList().get(k).getProductList().size();l++)
            {



                if(
                        //comboDetails_beann.getList().get(0).getProdList().get(k).getProductList().get(l).isSelected()==true &&
                        comboDetails_beann.getList().get(0).getProdList().get(k).getProductList().get(l).getIndividula_product_quant_byUser()!=0)

                {
                    Addtocart_Combo_Bean.CartList cartList=new Addtocart_Combo_Bean.CartList();
                    System.out.println("Cat Id  "+comboDetails_beann.getList().get(0).getProdList().get(k).getCatId());
                    System.out.println("Product Id  "+comboDetails_beann.getList().get(0).getProdList().get(k).getProductList().get(l).getProdId());
                    System.out.println("Product Quantity  "+comboDetails_beann.getList().get(0).getProdList().get(k).getProductList().get(l).getIndividula_product_quant_byUser());


                    cartList.setCatId(comboDetails_beann.getList().get(0).getProdList().get(k).getCatId());
                    cartList.setPrice("");
                    cartList.setProductId(comboDetails_beann.getList().get(0).getProdList().get(k).getProductList().get(l).getProdId());
                    cartList.setQuantity(comboDetails_beann.getList().get(0).getProdList().get(k).getProductList().get(l).getIndividula_product_quant_byUser());

                    cartLists.add(cartList);


                }



            }
        }

        System.out.println("CartList Size "+cartLists.size());
        System.out.println("CartList Userid "+userid);
        //     System.out.println("CartList addressid "+address_id);
        System.out.println("CartList ComboId  "+comboDetails_beann.getList().get(0).getComboId());

        LoadInterface loadInterface = retrofit.create(LoadInterface.class);

        Addtocart_Combo_Bean addtocart_combo_bean=new Addtocart_Combo_Bean();
        addtocart_combo_bean.setUserId(userid);
        addtocart_combo_bean.setComboid(comboDetails_beann.getList().get(0).getComboId());

        addtocart_combo_bean.setProductType("2");
//addtocart_combo_bean.setAddress_id(address_id);
        addtocart_combo_bean.setCartList(cartLists);




        Call<ResponseBody> resultCall = loadInterface.addtoCartCombo(addtocart_combo_bean);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {

                                Intent ii=new Intent(COmboDetails_Activity.this, MainActivity.class);
                                startActivity(ii);


                            }

                            else
                            {
                                String errmsg=jsonObject22.getString("errorMessage");
                                Toast.makeText(getApplicationContext(),errmsg,Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });

    }


    public class Add_Gererladdress_Delivery_Address_Adp extends RecyclerView.Adapter<Add_Gererladdress_Delivery_Address_Adp.ViewHolder> {
        Context context;
        List<GetDeliveryAddressModel.DeliverAddressDetailsModel> arrayList;

        public Add_Gererladdress_Delivery_Address_Adp(Activity homeActivity, List<GetDeliveryAddressModel.DeliverAddressDetailsModel> arrayList) {
            this.context=homeActivity;
            this.arrayList=arrayList;
        }

        @NonNull
        @Override
        public Add_Gererladdress_Delivery_Address_Adp.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

            View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.add_gererladdress_delivery_addredd_item,viewGroup,false);
            Add_Gererladdress_Delivery_Address_Adp.ViewHolder viewHolder=new Add_Gererladdress_Delivery_Address_Adp.ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull Add_Gererladdress_Delivery_Address_Adp.ViewHolder viewHolder, final int i) {

            viewHolder.txt_crd_vale.setText(arrayList.get(i).getAddress());
            viewHolder.crd_vew.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialog.dismiss();
                    address_id=arrayList.get(i).getId();
                    addtoCart();
                }
            });

        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            CustomTextViewRegular txt_crd_vale;
            CardView crd_vew;
            ImageView img_delete;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                crd_vew=(CardView)itemView.findViewById(R.id.card);

                txt_crd_vale=(CustomTextViewRegular)itemView.findViewById(R.id.txt_crd_vale);
                img_delete=(ImageView)itemView.findViewById(R.id.img_delete);
                img_delete.setVisibility(GONE);
            }
        }
    }


}
