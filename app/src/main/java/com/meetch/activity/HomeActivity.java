package com.meetch.activity;

import android.content.Intent;

import android.os.Bundle;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.meetch.R;
import com.meetch.adapter.Best_OfferAdapter;
import com.meetch.adapter.Classical_BurgerAdapter;
import com.meetch.adapter.MyPager;
import com.meetch.adapter.Special_BurgerAdapter;
import com.meetch.adapter.BurgerAdapter;
import com.meetch.storage.MySharedPref;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    private ViewPager viewPager;
    private MyPager myPager;
    private int dotscount;
    private ImageView[] dots;
    String itemp = "";
    RecyclerView best_offer_recyclerView,the_burger_recyclerView,classical_burger_recyclerView,special_burger_recyclerView;
    Best_OfferAdapter best_offerAdapter;
    ArrayList<String> arrayList=new ArrayList<>();
    BurgerAdapter _burgerAdapter;
    Classical_BurgerAdapter classical_burgerAdapter;
    Special_BurgerAdapter special_burgerAdapter;
    RelativeLayout rr_test_best;
    TextView burger_txt;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        inti();
    }

    public  void  inti(){

        myPager = new MyPager(this);
        viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(myPager);
        LinearLayout sliderDotspanel;
        sliderDotspanel = (LinearLayout) findViewById(R.id.SliderDots);
        rr_test_best=(RelativeLayout)findViewById(R.id.rr_test_best);
        burger_txt=(TextView)findViewById(R.id.burger_txt);
        burger_txt.setOnClickListener(this);

        rr_test_best.setOnClickListener(this);
        //circleIndicator = findViewById(R.id.circle);
      //  circleIndicator.setViewPager(viewPager);
        dotscount = myPager.getCount();
        dots = new ImageView[dotscount];

        for(int i = 0; i < dotscount; i++){

            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.nonactive_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(8, 0, 8, 0);

            sliderDotspanel.addView(dots[i], params);

        }

        dots[0].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_doc_red));

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                System.out.println("dot position:::::"+position);;


                for(int i = 0; i< dotscount; i++){
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.nonactive_dot));
                }
                if(position==0){
                    dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_doc_red));
                }else if(position==1){
                    dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot_yellow));
                }else if(position==2){
                    dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_doc_green));
                }
                else if(position==3){
                    dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot_pink));
                }



            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        arrayList.add("Ram");
        arrayList.add("");
        arrayList.add("");
        arrayList.add("");
        arrayList.add("");
        arrayList.add("");



//        _burgerAdapter =new BurgerAdapter(this,arrayList);
//        the_burger_recyclerView=(RecyclerView)findViewById(R.id.the_burger_recyclerView);
//        the_burger_recyclerView.setVisibility(View.VISIBLE);
//        the_burger_recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
//        the_burger_recyclerView.setAdapter(_burgerAdapter);

        classical_burgerAdapter=new Classical_BurgerAdapter(this,arrayList);
        classical_burger_recyclerView=(RecyclerView)findViewById(R.id.classical_burger_recyclerView);
        classical_burger_recyclerView.setVisibility(View.VISIBLE);
        classical_burger_recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        classical_burger_recyclerView.setAdapter(classical_burgerAdapter);
//
//        special_burgerAdapter=new Special_BurgerAdapter(this,arrayList);
//        special_burger_recyclerView=(RecyclerView)findViewById(R.id.special_burger_recyclerView);
//        special_burger_recyclerView.setVisibility(View.VISIBLE);
//        special_burger_recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
//        special_burger_recyclerView.setAdapter(special_burgerAdapter);
    }

    @Override
    public void onClick(View v) {
        if(v==rr_test_best){
            Intent intent=new Intent(this,My_CommandActivity.class);
            startActivity(intent);
        }
        if(v==burger_txt){
            MySharedPref sp=new MySharedPref();
         String   loggedin=sp.getData(getApplicationContext(),"ldata","null");
            if(loggedin!=null && !loggedin.equalsIgnoreCase("")  &&
                    !loggedin.equalsIgnoreCase("null")) {
                Intent intent=new Intent(HomeActivity.this,FavoritesActivity.class);
                startActivity(intent);
            }
            else
            {
                Intent ii = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(ii);
            }

        }
    }
}
