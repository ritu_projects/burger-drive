package com.meetch.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.meetch.R;
import com.meetch.apiclient.ApiClient;
import com.meetch.bean.signin_model.RegisterModel;
import com.meetch.bean.signin_model.RegisterResponse;
import com.meetch.constant.Utils;
import com.meetch.rectrofit.LoadInterface;
import com.meetch.storage.MySharedPref;
import com.meetch.utils.CustomEditTextRegular;

import org.json.JSONException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    LinearLayout ll_login_page;
    Button button_register;
    CustomEditTextRegular username,email,password,phonenumber;
    ProgressBar loader;
    RegisterResponse registerResponse;
MySharedPref sp=new MySharedPref();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        inti();
    }

    public  void inti(){
        ll_login_page=(LinearLayout)findViewById(R.id.ll_login_page);
        button_register=(Button)findViewById(R.id.button_register);
        loader=(ProgressBar)findViewById(R.id.loader);
        username=(CustomEditTextRegular)findViewById(R.id.username);
        email=(CustomEditTextRegular)findViewById(R.id.email);
        password=(CustomEditTextRegular)findViewById(R.id.password);
        phonenumber=(CustomEditTextRegular)findViewById(R.id.phonenumber);

        ll_login_page.setOnClickListener(this);
        button_register.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        if(v==ll_login_page){
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }

        if(v==button_register)
        {
validate();
        }

    }
    private void validate() {

        boolean isError=false;
        String str_username=username.getText().toString();
        String str_email_address=email.getText().toString();
        String str_password=password.getText().toString();
        String str_phonenumber=phonenumber.getText().toString();

        if(str_username==null || str_username.equalsIgnoreCase("") || str_username.equalsIgnoreCase("null"))
        {
            isError=true;
            username.setError(getResources().getString(R.string.ce_champ_est_requis));


        }
        else
        {
            username.setError(null);
        }
        if(str_email_address==null || str_email_address.equalsIgnoreCase("") || str_email_address.equalsIgnoreCase("null"))
        {
            isError=true;
            email.setError(getResources().getString(R.string.ce_champ_est_requis));


        }

        else if(!isValidEmail(str_email_address)){

            isError=true;
            email.setError(getResources().getString(R.string.sil_vous_plaite_entr_eml_valid));

        }
        else
        {
            email.setError(null);
        }


        if(str_password==null || str_password.equalsIgnoreCase("") || str_password.equalsIgnoreCase("null"))
        {
            isError=true;
            password.setError(getResources().getString(R.string.ce_champ_est_requis));


        }
        else
        {
            password.setError(null);
        }

        if(str_phonenumber==null || str_phonenumber.equalsIgnoreCase("") || str_phonenumber.equalsIgnoreCase("null"))
        {
            isError=true;
            phonenumber.setError(getResources().getString(R.string.ce_champ_est_requis));


        }
        else
        {
            phonenumber.setError(null);
        }

        if(!isError)
        {

            hideKeyboard(RegisterActivity.this);
            callWebService();
        }

    }
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
    public void callWebService()

    {
        if(Utils.isConnected(getApplicationContext()))
        {

            try {
                register_User();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }

    private void register_User() throws JSONException {


        loader.setVisibility(View.VISIBLE);


        System.out.println("API EmailId@@@"+username.getText().toString());
        System.out.println("API Password@@@"+password.getText().toString());

        Retrofit retrofit= ApiClient.getClient(getApplicationContext());
        LoadInterface loadInterface = retrofit.create(LoadInterface.class);


        RegisterModel registerModel = new RegisterModel();
        registerModel.setUsername(username.getText().toString());
        registerModel.setEmail(email.getText().toString());
        registerModel.setTelephone(phonenumber.getText().toString());

        registerModel.setPassword(password.getText().toString());
        registerModel.setDevice_token("");
        registerModel.setDevice_type("android");
        Call<RegisterResponse> resultCall = loadInterface.user_registration(registerModel);

        resultCall.enqueue(new Callback<RegisterResponse>() {

            @Override
            public void onResponse(Call<RegisterResponse> call, retrofit2.Response<RegisterResponse> response) {


                registerResponse = response.body();

                System.out.println("Status Response is!!!"+registerResponse.getErrorMessage());

                if (response.isSuccessful()) {

                    try {

                        String responedata = registerResponse.getError();
                        System.out.println("Status responedata is!!!"+responedata);


                        if(responedata.equalsIgnoreCase("true"))
                        {
                            loader.setVisibility(View.GONE);

                            String errormessage= registerResponse.getErrorMessage();

                            Toast.makeText(getApplicationContext(),errormessage+"",Toast.LENGTH_SHORT).show();

                        }
                        else if(responedata.equalsIgnoreCase("false"))
                        {
                            loader.setVisibility(View.GONE);

                            System.out.println("Status UserDetials is!!!"+registerResponse.getUsersDetails());


//                            sp.saveData(getApplicationContext(),"ldata",registerResponse.getUsersDetails()+"");

                            Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                            startActivity(intent);
                        }



                    } catch (Exception e) {
                        e.printStackTrace();
                        loader.setVisibility(View.GONE);

                    }
//
                } else {
                    loader.setVisibility(View.GONE);

                }
//

            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {

                loader.setVisibility(View.GONE);

                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });

    }

}
