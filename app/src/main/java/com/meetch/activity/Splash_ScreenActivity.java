package com.meetch.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.meetch.MainActivity;
import com.meetch.R;
import com.meetch.gpstracker.GPSTracker;
import com.meetch.storage.MySharedPref;

import java.util.ArrayList;
import java.util.List;

public class Splash_ScreenActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 2000;
    MySharedPref sp=new MySharedPref();
    String loggedin;
    public static final int RequestPermissionCode = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash__screen);
        checkPermission();

      //  inti();
    }


    private boolean checkPermission() {

        List<String> permissionsList = new ArrayList<String>();

        if (ContextCompat.checkSelfPermission(Splash_ScreenActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }

        if (ContextCompat.checkSelfPermission(Splash_ScreenActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (ContextCompat.checkSelfPermission(Splash_ScreenActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (ContextCompat.checkSelfPermission(Splash_ScreenActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (permissionsList.size() > 0) {
            ActivityCompat.requestPermissions(Splash_ScreenActivity.this, permissionsList.toArray(new String[permissionsList.size()]),
                    1);
            return false;
        }

        else {

         inti();

        }
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (permissions.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED ||
                        (permissions.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                                grantResults[1] == PackageManager.PERMISSION_GRANTED)){


               inti();
                    //list is still empty
                }
                else {
                    // Permission Denied
               //     Toast.makeText(getApplicationContext(), "permission_deny", Toast.LENGTH_LONG).show();
                    GPSTracker gps = new GPSTracker(Splash_ScreenActivity.this);
                    double latitude = gps.getLatitude();
                    double longitude = gps.getLongitude();
                    System.out.println("Latitude  "+latitude);
                    System.out.println("Longitude  "+longitude);
                    checkPermission();
//                    finish();
//                    Intent ii=new Intent(Splash_ScreenActivity.this, MainActivity.class);
//                    startActivity(ii);
                }
                break;
        }
    }

    public  void  inti(){
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                //finish();
                Intent ii=new Intent(Splash_ScreenActivity.this, MainActivity.class);
                startActivity(ii);

            }

            }, SPLASH_TIME_OUT);

    }

}
