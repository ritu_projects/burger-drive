package com.meetch.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.meetch.R;
import com.meetch.activity.bottom_add_delivery_address_gererladdress.Add_Gererladdress_Delivery_Address_Activity;
import com.meetch.activity.mode_de_paiement.Mode_De_Paiement_Activity;
import com.meetch.bean.Time_Selection_Bean;
import com.meetch.bean.applypromomodel.Applypromocode_Model;
import com.meetch.bean.checkoutmodel.Checkout_Model;
import com.meetch.bean.singlecasedeatial.Singlecasemodel;
import com.meetch.constant.Constants;
import com.meetch.constant.GetUserId;
import com.meetch.constant.Utils;
import com.meetch.rectrofit.LoadInterface;
import com.meetch.storage.MySharedPref;
import com.meetch.utils.CustomDialogVerifyAccount;
import com.meetch.utils.CustomTextView;
import com.meetch.utils.CustomTextViewRegular;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.view.View.GONE;

public class Checkout_Activity extends AppCompatActivity implements View.OnClickListener {

    ArrayList<Time_Selection_Bean> arrayList=new ArrayList<>();
Time_Selection_Bean time_selection_bean;

   RecyclerView rcyl_selcn_un_timeslot;
   RelativeLayout rr_back;
     CustomTextView title;

   Button btn_checkout,btn_suivi_de_commande;
    Dialog ordr_cnfrm_dialog;
    Selct_TimeslotAdp selct_timeslotAdp;
    RelativeLayout rr_slct_dte;
    private Calendar calendar;

    private int day;
    private int month;
    private int year;
    private String userDate;
    private long fromDateSetTime;
    private boolean isFromDateSet;
CustomTextView tvFromDate;
    private DatePickerDialog.OnDateSetListener fromDateSet;
    CustomTextViewRegular txt_ajouterr,txt_ajouter,txt_dish_name,txt_prize,txt_shipping_cost,txt_deliveryaddress,txt_totl_partial_subtotal_value;
    private CustomDialogVerifyAccount promoDialog;
    ProgressBar loader;
    String  userid,address_id,selected_time="";
   // ArrayList<>

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        inti();
    }

    public  void  inti(){
        userid= GetUserId.getUserInfo(getApplicationContext());

        loader=(ProgressBar)findViewById(R.id.loader);
        rr_back=(RelativeLayout)findViewById(R.id.rr_back);
        title=(CustomTextView)findViewById(R.id.title);
        title.setText(getResources().getString(R.string.text_check_out));
        rr_back.setOnClickListener(this);

//         for(int i=0;i<6;i++) {
//
//
//
//             time_selection_bean.setStr_time("1-2PM");
//             time_selection_bean.setStr_time("2-3PM");
//             time_selection_bean.setStr_time("3-4PM");
//             time_selection_bean.setStr_time("7-8PM");
//             time_selection_bean.setStr_time("8-9PM");
//             time_selection_bean.setStr_time("10-11PM");
//
//             time_selection_bean.setSelected(false);
//             time_selection_bean.setSelected(false);
//             time_selection_bean.setSelected(false);
//             time_selection_bean.setSelected(false);
//             time_selection_bean.setSelected(false);
//             time_selection_bean.setSelected(false);
//
//             arrayList.add(time_selection_bean);
//         }

        btn_checkout=(Button)findViewById(R.id.btn_checkout);

        rr_slct_dte=(RelativeLayout)findViewById(R.id.rr_slct_dte);

        rcyl_selcn_un_timeslot=(RecyclerView)findViewById(R.id.rcyl_selcn_un_timeslot);
        txt_ajouterr=(CustomTextViewRegular)findViewById(R.id.txt_ajouterr);
        txt_ajouter=(CustomTextViewRegular)findViewById(R.id.txt_ajouter);
        txt_dish_name=(CustomTextViewRegular)findViewById(R.id.txt_dish_name);
        txt_prize=(CustomTextViewRegular)findViewById(R.id.txt_prize);
        txt_totl_partial_subtotal_value=(CustomTextViewRegular)findViewById(R.id.txt_totl_partial_subtotal_value);
        txt_shipping_cost=(CustomTextViewRegular)findViewById(R.id.txt_shipping_cost);
        txt_deliveryaddress=(CustomTextViewRegular)findViewById(R.id.txt_deliveryaddress);
        rcyl_selcn_un_timeslot.setVisibility(View.VISIBLE);

         btn_checkout.setOnClickListener(this);

        tvFromDate=(CustomTextView)findViewById(R.id.tvFromDate);

        calendar = Calendar.getInstance();
        day = calendar.get(Calendar.DAY_OF_MONTH);
        month = calendar.get(Calendar.MONTH);
        year = calendar.get(Calendar.YEAR);
//calendar.set(day, month, year);

        userDate =  day +"-" + (month + 1) + "-" + year;

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        String dateString = dateFormat.format(calendar.getTime());

        tvFromDate.setText(dateString);
        fromDateSetTime = 0;

        fromDateSet = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.clear();
                calendar.set(year, monthOfYear, dayOfMonth);

              //  userDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

                String dateString = dateFormat.format(calendar.getTime());

                tvFromDate.setText(dateString);
               // userDate =  dayOfMonth +"-" + (monthOfYear + 1) + "-" + year;

            //    tvFromDate.setText(userDate);

                fromDateSetTime = calendar.getTimeInMillis();
                isFromDateSet = true;


            }
        };

        rr_slct_dte.setOnClickListener(this);
        txt_ajouterr.setOnClickListener(this);
        txt_ajouter.setOnClickListener(this);
        txt_dish_name.setOnClickListener(this);

        callWebService();
    }
    @Override
    public void onBackPressed() {

finish();
    }

    public void callWebService()

    {
        if(Utils.isConnected(getApplicationContext()))
        {

            try {
                getCartDetails();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }
    @Override
    public void onClick(View v) {

        if(v==btn_checkout)
        {
            validate();
          //  open_Order_Confrmdialog();
        }
        if(v==rr_back)
        {
            onBackPressed();
        }

        if(v==btn_suivi_de_commande)
        {
            ordr_cnfrm_dialog.dismiss();
            callWebserviceCheckout();


//            Intent ii=new Intent(Checkout_Activity.this,Suivre_TrackOrder_Activity.class);
//            startActivity(ii);
        }

        if(v==rr_slct_dte)
        {
            openFromDatePicker();
        }
        if(v==txt_ajouterr)
        {
            Intent ii=new Intent(Checkout_Activity.this, Add_Gererladdress_Delivery_Address_Activity.class);
            startActivity(ii);
        }
if(v==txt_ajouter)
{
    Intent ii=new Intent(Checkout_Activity.this, Mode_De_Paiement_Activity.class);
    startActivity(ii);
}

if(v==txt_dish_name)
{
    openPromoApplyDialog();
}



    }

    private void getCartDetails() {

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        Singlecasemodel singlecasemodel = new Singlecasemodel();
        singlecasemodel.setUserid(userid);
        Call<ResponseBody> resultCall = loadInterface.getUserCartDetail(singlecasemodel);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status ResponseCart is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is Cart@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {
                              String  tax_price = jsonObject22.getString("tax_price");
                                txt_prize.setText(tax_price+"€");
                                String  total_price = jsonObject22.getString("final_price");
                                txt_totl_partial_subtotal_value.setText(total_price+"€");
                                String  shipping_charge = jsonObject22.getString("shipping_charge");
                                txt_shipping_cost.setText(shipping_charge+"€");

                                String  code_name = jsonObject22.getString("code_name");
                                if(code_name!=null && !code_name.equalsIgnoreCase("") && !code_name.equalsIgnoreCase("null"))
                                {
                                    txt_dish_name.setText(code_name);

                                }
                                else
                                {
                                    txt_dish_name.setText(getResources().getString(R.string.no_promocode_applied));
                                }


                                JSONArray jsonArray_category=jsonObject22.getJSONArray("final_time_sloat");
                                arrayList=new ArrayList<>();
                                for(int k=0;k<jsonArray_category.length();k++)
                                {
                                    JSONObject jsonObject_category = jsonArray_category.getJSONObject(k);
                                    System.out.println("Json Category   "+jsonObject_category);
                                    time_selection_bean=new Time_Selection_Bean();
                                    time_selection_bean.setStr_time(jsonObject_category.getString("timeslot"));
                                    time_selection_bean.setAvailibility(jsonObject_category.getString("availibility"));


                                    arrayList.add(time_selection_bean);
                                }

                                selct_timeslotAdp=new Selct_TimeslotAdp(Checkout_Activity.this,arrayList);



                                // rcyl_selcn_un_timeslot.setLayoutManager(mLayoutManager);
                                final GridLayoutManager manager = new GridLayoutManager( Checkout_Activity.this, 3);
                                rcyl_selcn_un_timeslot.setLayoutManager(manager);
                                rcyl_selcn_un_timeslot.setAdapter(selct_timeslotAdp);
                                rcyl_selcn_un_timeslot.setNestedScrollingEnabled(false);

//                                String  delivery_address = jsonObject22.getString("delivery_address");
//                                JSONObject jsonObject33 = new JSONObject(delivery_address);
//                                String  address = jsonObject33.getString("address");
//                                address_id = jsonObject33.getString("id");
//                                txt_deliveryaddress.setText(address);


                            }

                            else
                            {
                                rcyl_selcn_un_timeslot.setVisibility(GONE);
                                //txt_no_cas_outers.setVisibility(VISIBLE);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });



    }

    private void validate() {

        boolean isError=false;


        if(selected_time==null || selected_time.equalsIgnoreCase("") || selected_time.equalsIgnoreCase("null"))
        {
            isError=true;
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.pls_slct_timeslot),Toast.LENGTH_SHORT).show();
          //  edt_promo_code.setError(getResources().getString(R.string.ce_champ_est_requis));


        }

        else
        {
        }


        if(!isError)
        {
            hideKeyboard(Checkout_Activity.this);
            open_Order_Confrmdialog();
          //  callWebserviceCheckout();
        }

    }
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

 public void  callWebserviceCheckout()
 {
     if(Utils.isConnected(getApplicationContext()))
     {
     try {
         checkout();

         //  addtoCart();
     } catch (Exception e) {
         e.printStackTrace();
     }
 }
        else
    {
        Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

    }
 }

    private void checkout() {

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        Checkout_Model checkout_model = new Checkout_Model();
        checkout_model.setUser_id(userid);
        checkout_model.setOrder_date(tvFromDate.getText().toString());
        checkout_model.setOrder_time(selected_time);
        MySharedPref sp=new MySharedPref();
      String  address_idd=sp.getData(getApplicationContext(),"address_id","");
        checkout_model.setAddress_id(address_idd);
        checkout_model.setPayment_type("Paypal");

        Call<ResponseBody> resultCall = loadInterface.checkout(checkout_model);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {
                                 String  result = jsonObject22.getString("result");
                                JSONObject jsonObject33 = new JSONObject(result);

                                String  order_id = jsonObject33.getString("order_id");
                                String  amount = jsonObject33.getString("amount");


                                Intent ii=new Intent(Checkout_Activity.this,Webview.class);
                                ii.putExtra("order_id",order_id);
                                ii.putExtra("amount",amount);
                                startActivity(ii);

                            }

                            else
                            {
                                String  errorMessage = jsonObject22.getString("errorMessage");
                                Toast.makeText(getApplicationContext(),errorMessage,Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });

    }


    private void openFromDatePicker() {

        DatePickerDialog fromPiker = new DatePickerDialog(this, fromDateSet, year, month, day);
        fromPiker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        fromPiker.show();
    }




    private void openPromoApplyDialog() {
        promoDialog = new CustomDialogVerifyAccount(Checkout_Activity.this, getResources()
                .getString(R.string.text_promo_apply), getResources().getString(R
                .string
                .text_apply), getResources().getString(R.string.text_cancel),
                getResources().getString(R.string.text_enter_promo_hint), false) {
            @Override
            public void doWithEnable(EditText editText) {

                String promoCode = editText.getText().toString();
                if (!promoCode.isEmpty()) {
                    promoDialog.dismiss();
                    hideKeyboard(Checkout_Activity.this);
                    callWebServiceforPromocode(promoCode);
                } else {
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string
                            .text_enter_promo_code), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void doWithDisable() {
                this.dismiss();
            }

            @Override
            public void clickOnText() {
                // Do something with click
            }
        };

        promoDialog.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        promoDialog.show();
    }
    public void callWebServiceforPromocode(String promoCode)

    {
        if(Utils.isConnected(getApplicationContext()))
        {

            try {
                promoCodeApply(promoCode);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }


    private void promoCodeApply(String promoCode) {

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        Applypromocode_Model applypromocode_model = new Applypromocode_Model();
        applypromocode_model.setUserid(userid);
        applypromocode_model.setCode_name(promoCode);


        Call<ResponseBody> resultCall = loadInterface.applyPromoCode(applypromocode_model);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");


                            if (status.equalsIgnoreCase("false")) {

                                 Toast.makeText(getApplicationContext(),getResources().getString(R.string.successfully_added),Toast.LENGTH_SHORT).show();
                                callWebService();
                            }

                            else
                            {
                                String  errorMessage = jsonObject22.getString("errorMessage");

                                Toast.makeText(getApplicationContext(),errorMessage,Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });



    }

    public class Selct_TimeslotAdp extends RecyclerView.Adapter<Selct_TimeslotAdp.ViewHolder> {
        Context context;
        ArrayList<Time_Selection_Bean> arrayList;
        private int focusedItem = 0;


        public Selct_TimeslotAdp(Activity homeActivity, ArrayList<Time_Selection_Bean> arrayList) {
            this.context=homeActivity;
            this.arrayList=arrayList;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

            View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.selcn_un_timeslot_item,viewGroup,false);
            ViewHolder viewHolder=new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {

            viewHolder.txt_time_value.setText(arrayList.get(i).getStr_time());

            viewHolder.rr_time_value.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(arrayList.get(i).getAvailibility().equalsIgnoreCase("1"))
                    {
                        if(arrayList.get(i).isSelected)
                        {
                            focusedItem = i;

                            arrayList.get(i).setSelected(false);
                            notifyDataSetChanged();
                            System.out.println("City Id Recycle###"+arrayList.get(i).getStr_time());
                        }
                        else if(arrayList.get(i).isSelected==false)

                        {
                            focusedItem = i;
                            for(int k=0;k<arrayList.size();k++)
                            {
                                if(arrayList.get(k).isSelected)
                                {
                                    arrayList.get(k).setSelected(false);

                                }

                            }
                            arrayList.get(i).setSelected(true);

                            selected_time=arrayList.get(i).getStr_time();
                            notifyDataSetChanged();
                        }
                    }

                    else

                    {
                        Toast.makeText(context,getResources().getString(R.string.deliver_boynot_availble),Toast.LENGTH_SHORT).show();

                    }




                }
            });

                if(arrayList.get(i).isSelected){

                    Drawable new_image= context.getResources().getDrawable(R.drawable.button_bg);
                    viewHolder.rr_time_value.setBackgroundDrawable(new_image);
                    viewHolder.txt_time_value.setTextColor(Color.parseColor("#FFFFFF"));


                }
                else if(arrayList.get(i).isSelected==false)
                {

                    Drawable new_image= context.getResources().getDrawable(R.drawable.button_unselected_bg);

                    viewHolder.rr_time_value.setBackgroundDrawable(new_image);
                    viewHolder.txt_time_value.setTextColor(Color.parseColor("#FF9F1C"));


                }






        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            CustomTextView txt_time_value;
            RelativeLayout rr_time_value;
            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                txt_time_value=(CustomTextView)itemView.findViewById(R.id.txt_time_value);
                rr_time_value=(RelativeLayout)itemView.findViewById(R.id.rr_time_value);
            }
        }
    }

    public void open_Order_Confrmdialog()
    {
        ordr_cnfrm_dialog=new Dialog(Checkout_Activity.this);

        ordr_cnfrm_dialog.getWindow().getAttributes().windowAnimations = R.style.MyAlertDialogStyle;
        // Include dialog.xml file
        ordr_cnfrm_dialog.setContentView(R.layout.order_cnfrm_screen);
        ordr_cnfrm_dialog.show();
        btn_suivi_de_commande=(Button) ordr_cnfrm_dialog.findViewById(R.id.btn_suivi_de_commande);
        btn_suivi_de_commande.setOnClickListener(this);
    }

}
