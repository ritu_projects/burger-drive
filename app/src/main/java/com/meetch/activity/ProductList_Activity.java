package com.meetch.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.meetch.R;
import com.meetch.activity.categorylist.CategoriesList_Activity;
import com.meetch.bean.getProduct_model.ProductModel;
import com.meetch.bean.productbean_model.ProductBeanModel;
import com.meetch.constant.Constants;
import com.meetch.constant.Utils;
import com.meetch.expandablelist.ExpandableHeightGridView;
import com.meetch.picasso.PicassoTrustAll;
import com.meetch.rectrofit.LoadInterface;
import com.meetch.utils.CustomTextView;
import com.meetch.utils.CustomTextViewRegular;
import com.yahoo.mobile.client.android.util.rangeseekbar.RangeSeekBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class ProductList_Activity extends AppCompatActivity implements View.OnClickListener {

    ProgressBar loader;
    ExpandableHeightGridView category_grid;
    ProductBeanModel productBeanModell;
    ProductsAdapter productsAdapter;
    Bundle b1;
    String category_name,cat_price;
    RelativeLayout rr_filter,rr_back,refresh_button;
    Dialog filter_dialog;
RecyclerView trierpar_recyclerView;
Button btn_appliquer;
ProgressBar loader_filter;
CustomTextView min_price,max_price,txt_no_products_found;
SwipeRefreshLayout pullToRefresh;

    Integer min_pricee=0,max_pricee=1000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_list);
        getBundleData();
        inti();
    }

    private void getBundleData() {
        b1=getIntent().getExtras();
        if(b1!=null)
        {
            category_name=b1.getString("category_name");
        }
    }

    public  void  inti(){
    loader=(ProgressBar)findViewById(R.id.loader);


        rr_filter=(RelativeLayout)findViewById(R.id.rr_filter);
        rr_filter.setVisibility(VISIBLE);

        refresh_button=(RelativeLayout)findViewById(R.id.refresh_button);
        refresh_button.setOnClickListener(this);



        txt_no_products_found=(CustomTextView)findViewById(R.id.txt_no_products_found);

        category_grid=(ExpandableHeightGridView)findViewById(R.id.category_grid);
        rr_filter.setOnClickListener(this);

        rr_back=(RelativeLayout)findViewById(R.id.rr_back);
        rr_back.setOnClickListener(this);
        CustomTextView  title=(CustomTextView)findViewById(R.id.title);

        title.setText(getResources().getString(R.string.products));

        final SwipeRefreshLayout pullToRefresh = findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                cat_price="";
                callWebService(); // your code
                pullToRefresh.setRefreshing(false);
            }
        });
callWebService();
    }
    @Override
    public void onBackPressed() {

        finish();
        Intent ii=new Intent(ProductList_Activity.this,CategoriesList_Activity.class);
        startActivity(ii);
    }

    public void callWebService()

    {
        if(Utils.isConnected(getApplicationContext()))
        {

            try {

                getProductList(category_name);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }

    public void open_Filterdialog(final View v)
    {
        filter_dialog=new Dialog(ProductList_Activity.this);

        filter_dialog.getWindow().getAttributes().windowAnimations = R.style.MyAlertDialogStyle;
        // Include dialog.xml file
        filter_dialog.setContentView(R.layout.filter_layout);
        filter_dialog.show();
        trierpar_recyclerView=(RecyclerView)filter_dialog.findViewById(R.id.trierpar_recyclerView);
        loader_filter=(ProgressBar)filter_dialog.findViewById(R.id.loader_filter);
        btn_appliquer=(Button)filter_dialog.findViewById(R.id.btn_appliquer);
        RangeSeekBar rr_range_prix=(RangeSeekBar)filter_dialog.findViewById(R.id.rr_range_prix);

        min_price=(CustomTextView)filter_dialog.findViewById(R.id.min_price);
        max_price=(CustomTextView)filter_dialog.findViewById(R.id.max_price);

       ImageView img_back=(ImageView)filter_dialog.findViewById(R.id.img_back);
       img_back.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               filter_dialog.dismiss();
           }
       });

        min_price.setText(min_pricee+",00€");
        max_price.setText(max_pricee+",00€");

        rr_range_prix.setRangeValues(0,1000);

        rr_range_prix.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
                min_price.setText(minValue+",00€");
                max_price.setText(maxValue+",00€");
                min_pricee=minValue;
                max_pricee=maxValue;

            }
        });
        cat_price=min_pricee+"-"+max_pricee;

             btn_appliquer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                filter_dialog.dismiss();
                callWebService();
            }
        });



    }

    private void getProductList(final String cat_namee) {

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        final ProductModel productBeanModel = new ProductModel();
        productBeanModel.setCat_name(cat_namee);
        productBeanModel.setPrice(cat_price);

        Call<ResponseBody> resultCall = loadInterface.get_Product(productBeanModel);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response Product is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response Product is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {

                                txt_no_products_found.setText("");

                                Gson gson = new Gson();

                                productBeanModell = gson.fromJson(responedata, ProductBeanModel.class);
                                category_grid.setVisibility(VISIBLE);
                                productsAdapter = new ProductsAdapter(ProductList_Activity.this, productBeanModell.getRecord(),productBeanModell.getImageUrl());
                                category_grid.setAdapter(productsAdapter);
                                category_grid.setNumColumns(1);

//                                if(cat_namee.equalsIgnoreCase("Burger"))
//                                {
//                                    System.out.println("Status Response Product is@@@"+productBeanModell.getRecord().getBurger().size());
//
//                                    category_grid.setVisibility(VISIBLE);
//
//                                    productsAdapter = new ProductsAdapter(ProductList_Activity.this, productBeanModell.getRecord().getBurger(),productBeanModell.getImageUrl());
//                                    category_grid.setAdapter(productsAdapter);
//                                    category_grid.setNumColumns(1);
//                                }
//
//                                else    if(cat_namee.equalsIgnoreCase("BOISSONS"))
//                                {
//                                    category_grid.setVisibility(VISIBLE);
//
//                                    productsAdapter = new ProductsAdapter(ProductList_Activity.this, productBeanModell.getRecord().getBoisson(),productBeanModell.getImageUrl());
//                                    category_grid.setAdapter(productsAdapter);
//                                    category_grid.setNumColumns(1);
//                                }
//                                else    if(cat_namee.equalsIgnoreCase("DESSERT"))
//                                {
//                                    category_grid.setVisibility(VISIBLE);
//
//                                    productsAdapter = new ProductsAdapter(ProductList_Activity.this, productBeanModell.getRecord().getDESSERT(),productBeanModell.getImageUrl());
//                                    category_grid.setAdapter(productsAdapter);
//                                    category_grid.setNumColumns(1);
//                                }


                            }

                            else
                            {
                                category_grid.setVisibility(GONE);
                                txt_no_products_found.setText(getResources().getString(R.string.no_product_available));
                                //txt_no_cas_outers.setVisibility(VISIBLE);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });



    }

    @Override
    public void onClick(View view) {

        if(view==rr_filter)
        {
            open_Filterdialog(view);
        }
        if(view==rr_back)
        {
            onBackPressed();
        }
        if(view==refresh_button)
        {
            cat_price="";
            callWebService();
        }
    }

    public class ProductsAdapter extends BaseAdapter {
        private Context mContext;
        List<ProductBeanModel.ProductModelDescp> arrayList;
        CustomTextView product_name;
        CustomTextViewRegular product_price;
        CardView crd_vl;
        String img_url;

        // Constructor
        public ProductsAdapter(Context c, List<ProductBeanModel.ProductModelDescp> list, String imageUrl) {
            this.mContext = c;
            this.arrayList=list;
            this.img_url=imageUrl;
        }

        public int getCount() {
            return arrayList.size();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return arrayList.size();
        }

        // create a new ImageView for each item referenced by the Adapter
        public View getView(final int position, View convertView, ViewGroup parent) {
            View grid;
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            grid = new View(mContext);
            grid = inflater.inflate(R.layout.product_list_item, null);
            ImageView item_image=(ImageView)grid.findViewById(R.id.img_best_offer);
            crd_vl=(CardView)grid.findViewById(R.id.crd_vew);
            product_name=(CustomTextView)grid.findViewById(R.id.txt_favoris);
            product_price=(CustomTextViewRegular) grid.findViewById(R.id.txt_address);
            PicassoTrustAll.getInstance(getApplicationContext())
                    .load(img_url+arrayList.get(position).getImage())
                    //.resize(520,320)
                    .error(R.drawable.bnanner)
                    .into(item_image);

            product_name.setText(arrayList.get(position).getProductName());
            product_price.setText(arrayList.get(position).getPrice()+"€");

            crd_vl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent ii=new Intent(mContext, My_CommandActivity.class);
                    ii.putExtra("product_id",arrayList.get(position).getId());
                    startActivity(ii);
                }
            });

            return grid;
        }


    }

}
