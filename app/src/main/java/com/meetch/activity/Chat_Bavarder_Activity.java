package com.meetch.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.meetch.R;
import com.meetch.apiclient.ApiClient;
import com.meetch.bean.getChatlist_bean.GetChatList_Bean;
import com.meetch.bean.ordertracking_bean.OrderTrackingBean;
import com.meetch.bean.sendchatbean.ChatSend_Bean;
import com.meetch.constant.Constants;
import com.meetch.constant.GetUserId;
import com.meetch.constant.Utils;
import com.meetch.picasso.PicassoTrustAll;
import com.meetch.rectrofit.LoadInterface;
import com.meetch.utils.CustomEditTextRegular;
import com.meetch.utils.CustomTextView;
import com.meetch.utils.CustomTextViewRegular;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Chat_Bavarder_Activity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView cart_item_list_recyle;
    Conversation_Item_listAdp cart_item_listAdp;
    ArrayList<String> arrayList=new ArrayList<>();
RelativeLayout RLSendtext;
CustomEditTextRegular MessageET;
Bundle b1;
ProgressBar loader;
String order_id,userid,usertoken;
GetChatList_Bean getChatList_bean;
    Runnable runnable;
    private final int FIVE_SECONDS = 5000;
    LinearLayoutManager linearLayout2;
    float visibleItemCount=0;
    CustomTextView title;
    RelativeLayout rr_back;

    Handler handler2=new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bavarder_chat_screen);

         userid = GetUserId.getUserInfo(Chat_Bavarder_Activity.this);
         usertoken = GetUserId.getUserToken(Chat_Bavarder_Activity.this);
        getBundleData();
        inti();


    }

    private void getBundleData() {
        b1=getIntent().getExtras();
        if(b1!=null)
        {
            order_id=b1.getString("order_id");
        }
    }

    public  void  inti(){

        arrayList.add("");
        arrayList.add("");
        arrayList.add("");
        arrayList.add("");
        arrayList.add("");
        arrayList.add("");


        RLSendtext=(RelativeLayout)findViewById(R.id.RLSendtext);
        RLSendtext.setOnClickListener(this);
        MessageET=(CustomEditTextRegular)findViewById(R.id.MessageET);
        loader=(ProgressBar)findViewById(R.id.loader);
        linearLayout2= new LinearLayoutManager(getApplicationContext());
        cart_item_list_recyle=(RecyclerView)findViewById(R.id.InboxDetailRV);

        cart_item_list_recyle.setLayoutManager(linearLayout2);

        title=(CustomTextView)findViewById(R.id.title);
        title.setText(getResources().getString(R.string.chat));

        rr_back=(RelativeLayout)findViewById(R.id.rr_back);
        rr_back.setOnClickListener(this);
        callWebService();
    }
    @Override
    public void onBackPressed() {

   finish();
    }



    @Override
    public void onClick(View v) {

        if(v==RLSendtext)
        {
            validate();
        }
        if(v==rr_back)
        {
onBackPressed();
        }
    }
    public void callWebService()

    {
        if(Utils.isConnected(getApplicationContext()))
        {

            try {
                runnable = new Runnable() {
                    public void run() {
                        visibleItemCount=linearLayout2.findFirstVisibleItemPosition();


                        getOrderChat();

                        handler2.postDelayed(this, FIVE_SECONDS);
                    }
                };

                handler2.postDelayed(runnable, FIVE_SECONDS);
           getOrderChat();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }
    public  void getOrderChat()
    {
     //   loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        OrderTrackingBean orderTrackingBean = new OrderTrackingBean();
        orderTrackingBean.setOrder_id(order_id);

        Call<ResponseBody> resultCall = loadInterface.getOrderchat(orderTrackingBean);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

             //   loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {

                                Gson gson = new Gson();
                                getChatList_bean = gson.fromJson(responedata, GetChatList_Bean.class);


                                cart_item_listAdp=new Conversation_Item_listAdp(Chat_Bavarder_Activity.this,getChatList_bean.getDriverOrderChat());
                                cart_item_list_recyle.setVisibility(View.VISIBLE);
                              //  cart_item_list_recyle.setLayoutManager(new LinearLayoutManager(Chat_Bavarder_Activity.this, LinearLayoutManager.VERTICAL, false));
                                cart_item_list_recyle.setAdapter(cart_item_listAdp);
                                cart_item_list_recyle.scrollToPosition(getChatList_bean.getDriverOrderChat().size() - 1);

                                if(visibleItemCount!=0)
                                {
                                    cart_item_list_recyle.getLayoutManager().scrollToPosition((int) visibleItemCount);
                                }

                                else
                                {
                                    cart_item_list_recyle.scrollToPosition(getChatList_bean.getDriverOrderChat().size() - 1);

                                }


                            }

                            else
                            {
                                //current_list_rcyle.setVisibility(View.GONE);


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });
    }

    private void validate() {
        boolean isError=false;
        String str_message=MessageET.getText().toString();

        if(str_message==null || str_message.equalsIgnoreCase("") || str_message.equalsIgnoreCase("null"))
        {
            isError=true;
            MessageET.setError(getResources().getString(R.string.ce_champ_est_requis));


        }
        else
        {
            MessageET.setError(null);
        }


        if(!isError)
        {
            hideSoftKeyboard(Chat_Bavarder_Activity.this);
            callWebServiceforSendChat();
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }
    public void callWebServiceforSendChat()

    {
        if(Utils.isConnected(getApplicationContext()))
        {

            try {
                orderChatAPI();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }

    private void orderChatAPI() {
        loader.setVisibility(View.VISIBLE);



        Retrofit retrofit= ApiClient.getClient(getApplicationContext());
        LoadInterface loadInterface = retrofit.create(LoadInterface.class);


        ChatSend_Bean chatSend_bean = new ChatSend_Bean();
        chatSend_bean.setOrder_id(order_id);
        chatSend_bean.setSender_id(userid);
        chatSend_bean.setMessage(MessageET.getText().toString());
        chatSend_bean.setMessage_by("1");
        Call<ResponseBody> resultCall = loadInterface.chatSend_Bean(chatSend_bean);

        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {



                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!" + response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@" + responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@" + jsonObject22);

                            String status = jsonObject22.getString("error");


                            if (status.equalsIgnoreCase("false")) {
                        //        callWebService();
                                MessageET.setText("");

                            } else {
                                String errorMessage = jsonObject22.getString("errorMessage");
                                Toast.makeText(getApplicationContext(),errorMessage,Toast.LENGTH_SHORT);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);

                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });

    }


        public class Conversation_Item_listAdp extends RecyclerView.Adapter<Conversation_Item_listAdp.ViewHolder> {
        Context context;
            List<GetChatList_Bean.DriverOrderChat> arrayList;

        public Conversation_Item_listAdp(Activity homeActivity, List<GetChatList_Bean.DriverOrderChat> arrayList) {
            this.context=homeActivity;
            this.arrayList=arrayList;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

            View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chat_row_item,viewGroup,false);
           ViewHolder viewHolder=new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

            if(arrayList.get(i).getMessageBy()==2)
            {
                viewHolder.LL_sendme.setVisibility(View.VISIBLE);
                viewHolder.LL_sendme1.setVisibility(View.GONE);
                viewHolder.UsersMessageTv.setText(arrayList.get(i).getMessage());
                viewHolder.reciever_chat_time.setText(arrayList.get(i).getTime());
                PicassoTrustAll.getInstance(context)
                        .load(arrayList.get(i).getImage())
                        .into(viewHolder.UsersImageChat);

            }

            else if(arrayList.get(i).getMessageBy()==1)
            {
                viewHolder.LL_sendme.setVisibility(View.GONE);
                viewHolder.LL_sendme1.setVisibility(View.VISIBLE);
                viewHolder.UsersMessageTv1.setText(arrayList.get(i).getMessage());
                viewHolder.send_chat_time.setText(arrayList.get(i).getTime());
                PicassoTrustAll.getInstance(context)
                        .load(arrayList.get(i).getImage())
                        .into(viewHolder.UsersImageChat1);
            }

        }



        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
         LinearLayout LL_sendme,LL_sendme1;
CustomTextViewRegular UsersMessageTv,reciever_chat_time,UsersMessageTv1,send_chat_time;
CircleImageView UsersImageChat,UsersImageChat1;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                LL_sendme=(LinearLayout)itemView.findViewById(R.id.LL_sendme);
                LL_sendme1=(LinearLayout)itemView.findViewById(R.id.LL_sendme1);
                UsersMessageTv=(CustomTextViewRegular)itemView.findViewById(R.id.UsersMessageTv);
                UsersMessageTv1=(CustomTextViewRegular)itemView.findViewById(R.id.UsersMessageTv1);
                reciever_chat_time=(CustomTextViewRegular)itemView.findViewById(R.id.reciever_chat_time);
                send_chat_time=(CustomTextViewRegular)itemView.findViewById(R.id.send_chat_time);
                UsersImageChat=(CircleImageView)itemView.findViewById(R.id.UsersImageChat);
                UsersImageChat1=(CircleImageView)itemView.findViewById(R.id.UsersImageChat1);

            }
        }
    }


}
