package com.meetch.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.meetch.MainActivity;
import com.meetch.R;
import com.meetch.activity.bottom_add_delivery_address_gererladdress.Ajouter_Une_Adresse_De_Livraison_Activity;
import com.meetch.adapter.CartComboProduct_ItemAdapter;
import com.meetch.adapter.Cart_ComprendItemAdapter;
import com.meetch.bean.applypromomodel.Applypromocode_Model;
import com.meetch.bean.cartdelete_model.CartDeleteModel;
import com.meetch.bean.catergoryListatCheckoutPage_model.CategroryListCheckout_model;
import com.meetch.bean.combobean.ComboBean;
import com.meetch.bean.getDeliveryAddress_model.GetDeliveryAddressModel;
import com.meetch.bean.productListatCheckoutPage_model.ProductListatCheckoutPage_model;
import com.meetch.bean.singlecasedeatial.Singlecasemodel;
import com.meetch.constant.Constants;
import com.meetch.constant.GetUserId;
import com.meetch.constant.Utils;
import com.meetch.picasso.PicassoTrustAll;
import com.meetch.rectrofit.LoadInterface;
import com.meetch.storage.MySharedPref;
import com.meetch.utils.CustomEditTextRegular;
import com.meetch.utils.CustomTextView;
import com.meetch.utils.CustomTextViewRegular;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class Cart_PageActivity extends AppCompatActivity implements View.OnClickListener {
    RecyclerView cart_item_list_recyle,cart_item_list_alldescp_recyle,list_item_recyl;
    Card_ItemAdapter card_itemAdapter;
    CardDetail_ItemAdapter cardDetail_itemAdapter;
    ArrayList<CategroryListCheckout_model> arrayList =new ArrayList<>();
    ArrayList<ProductListatCheckoutPage_model> arrayList_product =new ArrayList<>();
    ArrayList<ComboBean> arrayList_combo =new ArrayList<>();

    ArrayList<ProductListatCheckoutPage_model> arrayList_combo_product =new ArrayList<>();

    CustomTextViewRegular txt_dlte_cart;
    private CustomTextView title;
Button btn_checkout;
ProgressBar loader;
    RelativeLayout rr_layout_promocode,rr_layout_edt_promo,rr_layout_promocod_appliede,rr_layout_node,rr_layout_note_edt,rr_back,rr_delivery_add;
    String  userid,product_base_url,cart_id,combo_id;
    CustomTextViewRegular txt_addressz;
Button btn_add_promo;
CustomTextView txt_total_price,txt_cartempty,txt_comboname,txt_comboprice,txt_product_cost;
CustomTextViewRegular apply_promocode_value;
CustomEditTextRegular edt_promo_code;
RelativeLayout rr_cancel,rr_for_whole_addresss;
LinearLayout ll_ok,ll_cancel,ll_combo;
ScrollView scroll_vew;
Button btn_add_delivery_address,btn_add_delivery_addresss;
GetDeliveryAddressModel getDeliveryAddressModel;
    Add_Gererladdress_Delivery_Address_Adp add_gererladdress_delivery_address_adp;
    Dialog dialog;
    ImageView img_edt,img_cancel_combo;
    CustomTextView txt_tax_value,txt_shippig_cost;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart__page);
          userid= GetUserId.getUserInfo(getApplicationContext());

        inti();
    }

    public  void  inti(){

        rr_layout_promocode=(RelativeLayout)findViewById(R.id.rr_layout_promocode);
        rr_layout_promocod_appliede = (RelativeLayout)findViewById(R.id.rr_layout_promocod_appliede);

        apply_promocode_value = (CustomTextViewRegular) findViewById(R.id.apply_promocode_value);

        rr_delivery_add=(RelativeLayout)findViewById(R.id.rr_delivery_add);
        rr_layout_edt_promo=(RelativeLayout)findViewById(R.id.rr_layout_edt_promo);
        txt_cartempty=(CustomTextView)findViewById(R.id.txt_cartempty);
        btn_add_delivery_address=(Button)findViewById(R.id.btn_add_delivery_address);

        img_edt=(ImageView)findViewById(R.id.img_edt);
//        img_cancel_combo=(ImageView)findViewById(R.id.img_cancel_combo);
//        img_cancel_combo.setOnClickListener(this);

        txt_total_price=(CustomTextView)findViewById(R.id.txt_total_price);
        txt_product_cost=(CustomTextView)findViewById(R.id.txt_product_cost);
        txt_shippig_cost=(CustomTextView)findViewById(R.id.txt_shippig_cost);
        txt_tax_value=(CustomTextView)findViewById(R.id.txt_tax_value);
        edt_promo_code=(CustomEditTextRegular)findViewById(R.id.edt_promo_code);
        btn_add_promo=(Button)findViewById(R.id.btn_add_promo);
        scroll_vew=(ScrollView)findViewById(R.id.scroll_vew);
        rr_layout_edt_promo.setVisibility(View.GONE);
        loader=(ProgressBar)findViewById(R.id.loader);
        rr_layout_node=(RelativeLayout)findViewById(R.id.rr_layout_node);
        rr_layout_note_edt=(RelativeLayout)findViewById(R.id.rr_layout_note_edt);
        txt_addressz=(CustomTextViewRegular)findViewById(R.id.txt_addressz);
        rr_layout_note_edt.setVisibility(View.GONE);
        rr_back=(RelativeLayout)findViewById(R.id.rr_back);
        title=(CustomTextView)findViewById(R.id.title);
        title.setText(getResources().getString(R.string.text_my_command));



     //   ll_combo=(LinearLayout)findViewById(R.id.ll_combo);
        txt_comboprice=(CustomTextView)findViewById(R.id.txt_comboprice);
        txt_comboname=(CustomTextView)findViewById(R.id.txt_comboname);


        rr_cancel=(RelativeLayout)findViewById(R.id.rr_cancel);
        rr_cancel.setOnClickListener(this);
        rr_layout_promocode.setOnClickListener(this);
        rr_layout_node.setOnClickListener(this);
        rr_back.setOnClickListener(this);
        img_edt.setOnClickListener(this);


        btn_checkout=(Button)findViewById(R.id.btn_checkout);

        btn_add_promo.setOnClickListener(this);
        btn_checkout.setOnClickListener(this);
        btn_add_delivery_address.setOnClickListener(this);

        callWebService();
        chckDeliveryAddress();
    }
    public void callWebService()

    {
        if(Utils.isConnected(getApplicationContext()))
        {

            try {
                getCartDetails();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }

    public void callWebServiceforPromocode()

    {
        if(Utils.isConnected(getApplicationContext()))
        {

            try {
                applyforPromocode();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }
    private void validatenext() {
        boolean isError=false;
        MySharedPref sp=new MySharedPref();
    String address_id=    sp.getData(getApplicationContext(),"address_id","null");
        if(address_id==null || address_id.equalsIgnoreCase("null") || address_id.equalsIgnoreCase(""))
        {
            isError=true;
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.pls_slct_delivryadd),Toast.LENGTH_SHORT).show();

        }
        if(!isError)
        {
            Intent ii=new Intent(Cart_PageActivity.this,Checkout_Activity.class);
            startActivity(ii);
        }



    }

    private void validate() {

        boolean isError=false;
        String str_promocode=edt_promo_code.getText().toString();


        if(str_promocode==null || str_promocode.equalsIgnoreCase("") || str_promocode.equalsIgnoreCase("null"))
        {
            isError=true;
            edt_promo_code.setError(getResources().getString(R.string.ce_champ_est_requis));


        }

        else
        {
            edt_promo_code.setError(null);
        }


        if(!isError)
        {
            hideKeyboard(Cart_PageActivity.this);
            callWebServiceforPromocode();
        }

    }
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    private void applyforPromocode() {

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        Applypromocode_Model applypromocode_model = new Applypromocode_Model();
        applypromocode_model.setUserid(userid);
        applypromocode_model.setCode_name(edt_promo_code.getText().toString());


        Call<ResponseBody> resultCall = loadInterface.applyPromoCode(applypromocode_model);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");


                            if (status.equalsIgnoreCase("false")) {


                                // Toast.makeText(getApplicationContext(),errorMessage,Toast.LENGTH_SHORT).show();
                                callWebService();

                            }

                            else
                            {
                                String  errorMessage = jsonObject22.getString("errorMessage");

                                Toast.makeText(getApplicationContext(),errorMessage,Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });



    }


    @Override
    public void onClick(View v) {
        if(v==rr_layout_promocode){
            rr_layout_edt_promo.setVisibility(View.VISIBLE);
        }
        if(v==rr_layout_node){
            rr_layout_note_edt.setVisibility(View.VISIBLE);
        }
        if(v==img_edt)
        {
            callWebService_foraddDelivery_address();
        }
        if(v==rr_back){
          //  finish();
            onBackPressed();

        }
        if(v==btn_checkout)
        {
            validatenext();


        }
        if(v==btn_add_promo) {
            validate();
        }
        if(v==rr_cancel)
        {
          //  showDialogforCancelCart();
        }
//        if(v==img_cancel_combo)
//        {
//            System.out.println("Combo Id  "+combo_id);
//            showDialogforCancelCart(combo_id,"combo");
//        }

        if(v==ll_cancel)
        {
            dialog.dismiss();


        }
        if(v==btn_add_delivery_address)
        {
            callWebService_foraddDelivery_address();
        }
//        if(v==ll_ok)
//        {
//            dialog.dismiss();
//
//            callWebServiceforDeleteCart();
//
//        }


    }
    @Override
    public void onBackPressed() {

        finish();
        Intent ii=new Intent(Cart_PageActivity.this,MainActivity.class);
        startActivity(ii);
    }
    public  void callWebService_foraddDelivery_address()
    {

        if(Utils.isConnected(getApplicationContext()))
        {

            try {
                getDeliveryAddress();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }
    public  void getDeliveryAddress()
    {

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        String  userid= GetUserId.getUserInfo(getApplicationContext());


        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        Singlecasemodel singlecasemodel = new Singlecasemodel();
        singlecasemodel.setUserid(userid);

        Call<ResponseBody> resultCall = loadInterface.getallDeliveryAddress(singlecasemodel);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {
                                //    addtoCart();

                                Gson gson = new Gson();
                                getDeliveryAddressModel = gson.fromJson(responedata, GetDeliveryAddressModel.class);
                                add_gererladdress_delivery_address_adp=new
                                        Add_Gererladdress_Delivery_Address_Adp(Cart_PageActivity.this,getDeliveryAddressModel.getRecord());

                                showDialogforAddress();


                            }

                            else
                            {
                                Intent ii=new Intent(getApplicationContext(), Ajouter_Une_Adresse_De_Livraison_Activity.class);
                                ii.putExtra("cart_id","cart_id");
                                startActivity(ii);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });

    }
    private void showDialogforAddress() {

        dialog = new Dialog(Cart_PageActivity.this);
        dialog.setContentView(R.layout.add_gererladdress_delivery_address_activity);

        // set the custom dialog components - text, image and button
        RelativeLayout rr_ajourte_crd = (RelativeLayout) dialog.findViewById(R.id.rr_ajourte_crd);
        rr_ajourte_crd.setVisibility(View.VISIBLE);

        rr_ajourte_crd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                Intent ii=new Intent(getApplicationContext(), Ajouter_Une_Adresse_De_Livraison_Activity.class);
                ii.putExtra("cart_id","cart_id");
                startActivity(ii);
            }
        });

     CustomTextView   titlee=(CustomTextView)dialog.findViewById(R.id.title);
        titlee.setText(getResources().getString(R.string.add_livr_dlivered));

        RelativeLayout rr_back=(RelativeLayout)dialog.findViewById(R.id.rr_back);
        rr_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        RecyclerView    add_gererladdress_delivery_recyclerView=(RecyclerView)dialog.findViewById(R.id.add_gererladdress_delivery_recyclerView);
        add_gererladdress_delivery_recyclerView.setVisibility(View.VISIBLE);
        add_gererladdress_delivery_recyclerView.setLayoutManager(new LinearLayoutManager(Cart_PageActivity.this, LinearLayoutManager.VERTICAL, false));
        add_gererladdress_delivery_recyclerView.setAdapter(add_gererladdress_delivery_address_adp);


        dialog.show();
    }



    public void callWebServiceforDeleteCart(String str_cart_id,String combo_product)

    {
        if(Utils.isConnected(getApplicationContext()))
        {

            try {
                deleteCart(str_cart_id,combo_product);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }
    private void deleteCart(String str_cart_id,String combo_product) {

        loader.setVisibility(View.VISIBLE);
        System.out.println("Remove Cart id   "+str_cart_id);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        CartDeleteModel cartDeleteModel = new CartDeleteModel();

        if(combo_product.equalsIgnoreCase("product"))
        {
            cartDeleteModel.setUser_id(userid);
            cartDeleteModel.setCart_id(str_cart_id);
        }

else if(combo_product.equalsIgnoreCase("combo"))
        {
            cartDeleteModel.setUser_id(userid);
            cartDeleteModel.setCombo_id(str_cart_id);
        }

        Call<ResponseBody> resultCall = loadInterface.remove_cart_single(cartDeleteModel);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");
                            String  errorMessage = jsonObject22.getString("errorMessage");


                            if (status.equalsIgnoreCase("false")) {

                              Toast.makeText(getApplicationContext(),errorMessage,Toast.LENGTH_SHORT).show();

                                Intent ii=new Intent(Cart_PageActivity.this, MainActivity.class);
                                startActivity(ii);
                            }

                            else
                            {
                                Toast.makeText(getApplicationContext(),errorMessage,Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });



    }


    public  void showDialogforCancelCart(final String str_cart_id, final String combo_product) {

        dialog = new Dialog(Cart_PageActivity.this);
        dialog.setContentView(R.layout.dlg_cacl_cart);
        txt_dlte_cart=(CustomTextViewRegular)dialog.findViewById(R.id.txt_dlte_cart);

        txt_dlte_cart.setText(getResources().getString(R.string.dlte_cart));

        ll_ok=(LinearLayout)dialog.findViewById(R.id.ll_ok);
        ll_cancel=(LinearLayout)dialog.findViewById(R.id.ll_cancel);

        ll_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                callWebServiceforDeleteCart(str_cart_id,combo_product);

            }
        });
        ll_cancel.setOnClickListener(this);
            dialog.show();
    }

    private void getCartDetails() {

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        Singlecasemodel singlecasemodel = new Singlecasemodel();
        singlecasemodel.setUserid(userid);
        Call<ResponseBody> resultCall = loadInterface.getUserCart(singlecasemodel);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status ResponseCart is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is Cart@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {

                                txt_cartempty.setVisibility(GONE);

                                product_base_url = jsonObject22.getString("product_url");
                            String str_tax=jsonObject22.getString("tax_price");
                                txt_tax_value.setText(str_tax+"€");
                                String str_shipping_charge=jsonObject22.getString("shipping_charge");
                                txt_shippig_cost.setText(str_shipping_charge+"€");
                                int  in_appy_promo=jsonObject22.getInt("applied_promo");

                                if(in_appy_promo ==0)
                                {
                                    rr_layout_promocode.setVisibility(VISIBLE);
                                    rr_layout_promocod_appliede.setVisibility(GONE);
                                }
                                else if(in_appy_promo == 1)
                                {
                                    rr_layout_promocode.setVisibility(GONE);
                                    rr_layout_promocod_appliede.setVisibility(VISIBLE);

                                    apply_promocode_value.setText("Code promo appliqué");
                                }


                                JSONArray jsonArray=jsonObject22.getJSONArray("all_delivery_address");

                                if(jsonArray!=null && jsonArray.length()>0)
                                {
                                    JSONObject jsonObject33 = jsonArray.getJSONObject(0);
                                    String  address = jsonObject33.getString("address");
                                    txt_addressz.setText(address);
                                }



                                JSONObject jsonObject44=jsonObject22.getJSONObject("record");

                                JSONArray jsonArray_combo=jsonObject44.getJSONArray("combo");


                                if(jsonArray_combo!=null && jsonArray_combo.length()>0)
                                {



                                 //   ll_combo.setVisibility(View.VISIBLE);

                                    RecyclerView  list_item_recyl_combo=(RecyclerView)findViewById(R.id.list_item_recyl_combo);
                                    list_item_recyl_combo.setVisibility(VISIBLE);

                                    for (int k = 0; k < jsonArray_combo.length(); k++) {
                                        JSONObject jsonObject_product = jsonArray_combo.getJSONObject(k);

                                        ComboBean comboBean =new ComboBean();
                                        comboBean.setCombo_id(jsonObject_product.getInt("combo_id"));
                                        comboBean.setCombo_name(jsonObject_product.getString("combo_name"));
                                        comboBean.setCombo_img(jsonObject_product.getString("combo_img"));
                                        comboBean.setCombo_price(jsonObject_product.getString("combo_price"));

                                        JSONArray jsonArray_combo2=jsonObject_product.getJSONArray("cartCombo");

                                        arrayList_combo_product=new ArrayList<>();

                                        for (int l = 0; l < jsonArray_combo2.length(); l++) {

                                            JSONObject jsonObject_productt = jsonArray_combo2.getJSONObject(l);


                                            ProductListatCheckoutPage_model productListatCheckoutPage_model = new ProductListatCheckoutPage_model();
                                            productListatCheckoutPage_model.setProduct_name(jsonObject_productt.getString("product_name"));
                                            productListatCheckoutPage_model.setQuantity(jsonObject_productt.getString("quantity"));

                                            productListatCheckoutPage_model.setCat_name(jsonObject_productt.getString("cat_name"));
                                            // productListatCheckoutPage_model.setId(jsonObject_product.getString("id"));


                                            arrayList_combo_product.add(productListatCheckoutPage_model);
                                        }


                                        comboBean.setProductListatCheckoutPage_models(arrayList_combo_product);
                                        arrayList_combo.add(comboBean);


                                    }


                                    System.out.println("Combo List Size  "+arrayList_combo.size());
                      //  String str_combo_img=jsonObject22.getString("combo_img");

                                    list_item_recyl_combo.setHasFixedSize(true);
                                    LinearLayoutManager  linearLayoutManager2 = new LinearLayoutManager(getApplicationContext());

                                    list_item_recyl_combo.setLayoutManager(linearLayoutManager2);

                                Cart_ComboItemAdapter  cart_comprendItemAdapter2=
                                        new Cart_ComboItemAdapter(Cart_PageActivity.this,arrayList_combo);

                                list_item_recyl_combo.setAdapter(cart_comprendItemAdapter2);
                                    list_item_recyl_combo.setNestedScrollingEnabled(false);

                                    //     combo_id=jsonObject22.getString("combo_id");


                                }

                                else
                                {
                                    RecyclerView  list_item_recyl_combo=(RecyclerView)findViewById(R.id.list_item_recyl_combo);

                                    list_item_recyl_combo.setVisibility(GONE);
                                 //   ll_combo.setVisibility(GONE);
                                }

                                JSONArray jsonArray_category=jsonObject44.getJSONArray("single");
                                if(jsonArray_category!=null && jsonArray_category.length()>0)
                                {
                                    for(int k=0;k<jsonArray_category.length();k++)
                                    {
                                        JSONObject jsonObject_category = jsonArray_category.getJSONObject(k);
                                        System.out.println("Json Category   "+jsonObject_category);
                                        CategroryListCheckout_model categroryListCheckout_model=new CategroryListCheckout_model();
                                        categroryListCheckout_model.setId(jsonObject_category.getString("id"));
                                        categroryListCheckout_model.setCat_name(jsonObject_category.getString("cat_name"));
                                        categroryListCheckout_model.setCat_product_name(jsonObject_category.getString("product_name"));

                                        categroryListCheckout_model.setCat_img(jsonObject_category.getString("image"));
                                        categroryListCheckout_model.setCat_id(jsonObject_category.getString("cat_id"));
                                        categroryListCheckout_model.setC_code_name(jsonObject_category.getString("c_code_name"));
                                        categroryListCheckout_model.setCat_prize(jsonObject_category.getString("price"));
                                        categroryListCheckout_model.setCat_quantity(jsonObject_category.getString("quantity"));

                                        arrayList.add(categroryListCheckout_model);
                                    }
                                    card_itemAdapter=new Card_ItemAdapter( Cart_PageActivity.this,arrayList,product_base_url);
                                    cart_item_list_recyle=(RecyclerView)findViewById(R.id.cart_item_list_recyle);
                                    cart_item_list_recyle.setLayoutManager(new LinearLayoutManager(Cart_PageActivity.this,LinearLayoutManager.VERTICAL,false));
                                    cart_item_list_recyle.setAdapter(card_itemAdapter);
                                    cart_item_list_recyle.setNestedScrollingEnabled(false);


                                }

                                else
                                {
                                    cart_item_list_recyle=(RecyclerView)findViewById(R.id.cart_item_list_recyle);

                                    cart_item_list_recyle.setVisibility(GONE);
                                }


                                JSONArray jsonArray_all_related_product=jsonObject22.getJSONArray("all_related_product");

                                if(jsonArray_all_related_product!=null && jsonArray_all_related_product.length()>0) {

                                    for (int k = 0; k < jsonArray_all_related_product.length(); k++) {
                                        JSONObject jsonObject_product = jsonArray_all_related_product.getJSONObject(k);
                                        ProductListatCheckoutPage_model productListatCheckoutPage_model = new ProductListatCheckoutPage_model();
                                        productListatCheckoutPage_model.setId(jsonObject_product.getString("id"));
                                        productListatCheckoutPage_model.setCat_id(jsonObject_product.getString("cat_id"));
                                        productListatCheckoutPage_model.setProduct_name(jsonObject_product.getString("product_name"));
                                        productListatCheckoutPage_model.setDescription(jsonObject_product.getString("description"));
                                        productListatCheckoutPage_model.setPrice(jsonObject_product.getString("price"));
                                        productListatCheckoutPage_model.setQuantity(jsonObject_product.getString("quantity"));
                                        productListatCheckoutPage_model.setImage(jsonObject_product.getString("image"));


                                        arrayList_product.add(productListatCheckoutPage_model);
                                    }

                                    cardDetail_itemAdapter=new CardDetail_ItemAdapter( Cart_PageActivity.this,arrayList,product_base_url);
                                    cart_item_list_alldescp_recyle=(RecyclerView)findViewById(R.id.cart_item_list_alldescp_recyle);
                                    cart_item_list_alldescp_recyle.setVisibility(GONE);

                                    cart_item_list_alldescp_recyle.setLayoutManager(new LinearLayoutManager(Cart_PageActivity.this,LinearLayoutManager.VERTICAL,false));
                                    cart_item_list_alldescp_recyle.setAdapter(cardDetail_itemAdapter);

                                    cart_item_list_alldescp_recyle.setNestedScrollingEnabled(false);

                                Cart_ComprendItemAdapter    cart_comprendItemAdapterr=new Cart_ComprendItemAdapter(Cart_PageActivity.this,arrayList_product,product_base_url);
                                    list_item_recyl=(RecyclerView)findViewById(R.id.list_item_recyl);
                                    list_item_recyl.setLayoutManager(new LinearLayoutManager(Cart_PageActivity.this,LinearLayoutManager.HORIZONTAL,false));
                                    list_item_recyl.setAdapter(cart_comprendItemAdapterr);
                                    list_item_recyl.setNestedScrollingEnabled(false);


                                }
                                else
                                {
                                    cart_item_list_alldescp_recyle=(RecyclerView)findViewById(R.id.cart_item_list_alldescp_recyle);
                                    list_item_recyl=(RecyclerView)findViewById(R.id.list_item_recyl);

                                    cart_item_list_alldescp_recyle.setVisibility(GONE);

                                    list_item_recyl.setVisibility(GONE);
                                }

                                if(jsonObject22.getString("total_price")!=null &&
                                        !jsonObject22.getString("total_price").equalsIgnoreCase("") &&
                                        !jsonObject22.getString("total_price").equalsIgnoreCase("null")
                                )
                                {
                                    txt_product_cost.setText(jsonObject22.getString("total_price")+"€");

                                }



                                if(jsonObject22.getString("final_price")!=null &&
                                        !jsonObject22.getString("final_price").equalsIgnoreCase("") &&
                                !jsonObject22.getString("final_price").equalsIgnoreCase("null")
 )
                                {
                                    txt_total_price.setText(jsonObject22.getString("final_price")+"€");

                                }



                            }




                            else
                            {
                                scroll_vew.setVisibility(GONE);
                                txt_cartempty.setVisibility(View.VISIBLE);
                                //txt_no_cas_outers.setVisibility(VISIBLE);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });



    }

    public class CardDetail_ItemAdapter extends RecyclerView.Adapter<CardDetail_ItemAdapter.ViewHolder> {
        Context context;
        ArrayList<CategroryListCheckout_model> arrayList;
        String product_base_url;

        public CardDetail_ItemAdapter(Activity cart_pageActivity, ArrayList<CategroryListCheckout_model> arrayList,String product_base_url) {
            this.context=cart_pageActivity;
            this.arrayList=arrayList;
            this.product_base_url=product_base_url;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cart_category_item,viewGroup,false);
            CardDetail_ItemAdapter.ViewHolder viewHolder=new CardDetail_ItemAdapter.ViewHolder(view);
            return viewHolder;

        }

        @Override
        public void onBindViewHolder(@NonNull CardDetail_ItemAdapter.ViewHolder viewHolder, final int i) {


            viewHolder.txt_productname.setText(arrayList.get(i).getCat_quantity()+" "+arrayList.get(i).getCat_name());
            viewHolder.txt_prize.setText(arrayList.get(i).getCat_prize()+"€");
            viewHolder.cancel_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showDialogforCancelCart(arrayList.get(i).getId(),"product");
                }
            });

        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView img_banner;
          CustomTextViewRegular  txt_productname,txt_prize;
            ImageView cancel_image;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                img_banner=(ImageView)itemView.findViewById(R.id.img_banner);
                txt_productname=(CustomTextViewRegular)itemView.findViewById(R.id.txt_productname);
                txt_prize=(CustomTextViewRegular)itemView.findViewById(R.id.txt_prize);
                cancel_image=(ImageView)itemView.findViewById(R.id.cancel_image);

            }
        }
    }

  public void   chckDeliveryAddress()
  {
      MySharedPref sp=new MySharedPref();

      String address_id=sp.getData(getApplicationContext(),"address_id","");
      System.out.println("Address Id "+address_id);

      if(address_id!=null && !address_id.equalsIgnoreCase("null") && !address_id.equalsIgnoreCase(""))
      {
          rr_delivery_add.setVisibility(View.VISIBLE);
          btn_add_delivery_address.setVisibility(GONE);
          String address_name=sp.getData(getApplicationContext(),"address_name","");
          System.out.println("Address Name "+address_name);

          if(address_name!=null && !address_name.equalsIgnoreCase("null") && !address_name.equalsIgnoreCase(""))
          {
              txt_addressz.setText(address_name);

          }
      }
  }

    public class Add_Gererladdress_Delivery_Address_Adp extends RecyclerView.Adapter<Add_Gererladdress_Delivery_Address_Adp.ViewHolder> {
        Context context;
        List<GetDeliveryAddressModel.DeliverAddressDetailsModel> arrayList;

        public Add_Gererladdress_Delivery_Address_Adp(Activity homeActivity, List<GetDeliveryAddressModel.DeliverAddressDetailsModel> arrayList) {
            this.context=homeActivity;
            this.arrayList=arrayList;
        }

        @NonNull
        @Override
        public Add_Gererladdress_Delivery_Address_Adp.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

            View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.add_gererladdress_delivery_addredd_item,viewGroup,false);
            Add_Gererladdress_Delivery_Address_Adp.ViewHolder viewHolder=new Add_Gererladdress_Delivery_Address_Adp.ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull Add_Gererladdress_Delivery_Address_Adp.ViewHolder viewHolder, final int i) {

            viewHolder.txt_crd_vale.setText(arrayList.get(i).getAddress());

            viewHolder.crd_vew.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialog.dismiss();
                    MySharedPref sp=new MySharedPref();
                    sp.saveData(getApplicationContext(),"address_id",arrayList.get(i).getId());
                    sp.saveData(getApplicationContext(),"address_name",arrayList.get(i).getAddress());
                    chckDeliveryAddress();
                    //   address_id=arrayList.get(i).getId();

                }
            });

        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            CustomTextViewRegular txt_crd_vale;
            CardView crd_vew;
            ImageView img_delete;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                crd_vew=(CardView)itemView.findViewById(R.id.card);

                txt_crd_vale=(CustomTextViewRegular)itemView.findViewById(R.id.txt_crd_vale);
                img_delete=(ImageView)itemView.findViewById(R.id.img_delete);
                img_delete.setVisibility(GONE);
            }
        }
    }

    public class Card_ItemAdapter extends RecyclerView.Adapter<Card_ItemAdapter.ViewHolder> {
        Context context;
        ArrayList<CategroryListCheckout_model> arrayList;
        String product_base_url;

        public Card_ItemAdapter(Activity cart_pageActivity, ArrayList<CategroryListCheckout_model> arrayList,String product_base_url) {
            this.context=cart_pageActivity;
            this.arrayList=arrayList;
            this.product_base_url=product_base_url;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cart_item,viewGroup,false);
           ViewHolder viewHolder=new ViewHolder(view);
            return viewHolder;

        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {


            viewHolder.cat_name.setText(arrayList.get(i).getCat_product_name());
            PicassoTrustAll.getInstance(context)
                    .load(product_base_url+arrayList.get(i).getCat_img())
                    //   .resize(1000,100)
                    .error(R.drawable.bnanner)
                    .into(viewHolder.img_banner);

            viewHolder.txt_cat_price.setText(arrayList.get(i).getCat_prize()+"€");
            viewHolder.txt_cat_quantity.setText(context.getResources().getString(R.string.quantity)+": "+arrayList.get(i).getCat_quantity());
            viewHolder.ll_cncl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDialogforCancelCart(arrayList.get(i).getId(),"product");
                }

            });


        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView img_banner;
            CustomTextView cat_name;
            CustomTextViewRegular txt_cat_price,txt_cat_quantity;
            ImageView cancel_image;
LinearLayout ll_cncl;
            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                img_banner=(ImageView)itemView.findViewById(R.id.img_banner);
                cat_name=(CustomTextView)itemView.findViewById(R.id.cat_name);
                txt_cat_price=(CustomTextViewRegular)itemView.findViewById(R.id.txt_cat_price);
                txt_cat_quantity=(CustomTextViewRegular)itemView.findViewById(R.id.txt_cat_quantity);
                cancel_image=(ImageView)itemView.findViewById(R.id.cancel_image);
                ll_cncl=(LinearLayout)itemView.findViewById(R.id.ll_cncl);

                ll_cncl.setVisibility(View.VISIBLE);
                cancel_image.setVisibility(View.VISIBLE);
                txt_cat_price.setVisibility(View.VISIBLE);
                txt_cat_quantity.setVisibility(View.VISIBLE);

            }
        }

    }

    public class Cart_ComboItemAdapter extends RecyclerView.Adapter<Cart_ComboItemAdapter.ViewHolder> {
        Context context;
        ArrayList<ComboBean> arrayListt;
        String product_base_url,str_combo_img;




        public Cart_ComboItemAdapter(AppCompatActivity cart_pageActivity, ArrayList<ComboBean> arrayList_product) {
            this.context=cart_pageActivity;
            this.arrayListt=arrayList_product;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.combo_name_item,viewGroup,false);
         ViewHolder viewHolder=new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

            viewHolder.txt_comboname.setText(getResources().getString(R.string.combo_name)+": " +arrayListt.get(i).getCombo_name());
            viewHolder.txt_comboprice.setText(getResources().getString(R.string.combo_price)+": " +arrayListt.get(i).getCombo_price());

            viewHolder.rr_img_cncl_combo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    System.out.println("Combo Id  "+arrayListt.get(i).getCombo_id());
                    String in_combo= String.valueOf(arrayListt.get(i).getCombo_id());
                    showDialogforCancelCart(in_combo,"combo");
                }
            });
            System.out.println("Combo Product List Size "+arrayListt.get(i).getProductListatCheckoutPage_models().size());


            CartComboProduct_ItemAdapter cart_comprendItemAdapter2=
                    new CartComboProduct_ItemAdapter(Cart_PageActivity.this,arrayListt.get(i).getProductListatCheckoutPage_models(),
                            arrayListt.get(i).getCombo_img());

        viewHolder.list_item_recyl_combo_product.setLayoutManager(new LinearLayoutManager(Cart_PageActivity.this,LinearLayoutManager.HORIZONTAL,false));
            viewHolder.list_item_recyl_combo_product.setAdapter(cart_comprendItemAdapter2);




        }

        @Override
        public int getItemCount() {
            return arrayListt.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView img_cancel_combo;
            CustomTextView txt_comboname,txt_comboprice;
            RecyclerView list_item_recyl_combo_product;
            RelativeLayout rr_img_cncl_combo;

            public ViewHolder(@NonNull View itemView) {

                super(itemView);
                img_cancel_combo=(ImageView)itemView.findViewById(R.id.img_cancel_combo);
                txt_comboname=(CustomTextView) itemView.findViewById(R.id.txt_comboname);
                txt_comboprice=(CustomTextView) itemView.findViewById(R.id.txt_comboprice);
                list_item_recyl_combo_product=(RecyclerView) itemView.findViewById(R.id.list_item_recyl_combo_product);
                rr_img_cncl_combo=(RelativeLayout)itemView.findViewById(R.id.rr_img_cncl_combo);

            }
        }
    }

}


