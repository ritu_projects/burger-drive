package com.meetch.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.meetch.MainActivity;
import com.meetch.R;
import com.meetch.activity.bottom_add_delivery_address_gererladdress.Ajouter_Une_Adresse_De_Livraison_Activity;
import com.meetch.adapter.Comprend_ListAdapter;
import com.meetch.bean.addtoCart_model.Addtocart_Model;
import com.meetch.bean.getDeliveryAddress_model.GetDeliveryAddressModel;
import com.meetch.bean.productid_model.ProductId_Model;
import com.meetch.bean.singlecasedeatial.Singlecasemodel;
import com.meetch.constant.Constants;
import com.meetch.constant.GetUserId;
import com.meetch.constant.Utils;
import com.meetch.picasso.PicassoTrustAll;
import com.meetch.rectrofit.LoadInterface;
import com.meetch.storage.MySharedPref;
import com.meetch.utils.CustomTextView;
import com.meetch.utils.CustomTextViewRegular;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.view.View.GONE;

public class My_CommandActivity extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout rr_add_value,rr_sub_valve,rr_for_add_tocart,rr_back;
    TextView txt_total_value;
    private int index=1;
    private RecyclerView list_item_recyl;
    Comprend_ListAdapter comprend_listAdapter;
    ArrayList<String> arrayList=new ArrayList<>();
    MySharedPref sp=new MySharedPref();
    String loggedin,cat_id,product_id,total_prize;
    double totll_prize=0.00;
    Bundle b1;
    CustomTextView product_name,txt_quantity;
    CustomTextViewRegular txt_address;
    ProgressBar loader;
    ImageView product_image;
 Add_Gererladdress_Delivery_Address_Adp   add_gererladdress_delivery_address_adp;
    GetDeliveryAddressModel getDeliveryAddressModel;
     Dialog dialog;
     String address_id;
     double quantity;
     CustomTextView title,txt_voir_le_painer_quant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_command_new);
        getBundle();
        inti();
    }

    private void getBundle() {
        b1=getIntent().getExtras();
        if(b1!=null)
        {
            product_id=b1.getString("product_id");
            System.out.println("Product Id MyCommande  "+product_id);

        }
    }

    public  void inti(){

        rr_add_value=(RelativeLayout)findViewById(R.id.rr_add_value);
        rr_back=(RelativeLayout)findViewById(R.id.rr_back);
        rr_sub_valve=(RelativeLayout)findViewById(R.id.rr_sub_valve);
        txt_total_value=(TextView)findViewById(R.id.txt_total_value);
        list_item_recyl= (RecyclerView)findViewById(R.id.list_recyl_chroix);
        rr_for_add_tocart=(RelativeLayout) findViewById(R.id.rr_for_add_tocart);
        product_name=(CustomTextView)findViewById(R.id.product_name);
        txt_quantity=(CustomTextView)findViewById(R.id.txt_quantity);
        title=(CustomTextView)findViewById(R.id.title);
        txt_voir_le_painer_quant=(CustomTextView)findViewById(R.id.txt_voir_le_painer_quant);

        txt_address=(CustomTextViewRegular)findViewById(R.id.txt_address);
        product_image=(ImageView)findViewById(R.id.product_image);
        loader=(ProgressBar)findViewById(R.id.loader);

        rr_back.setOnClickListener(this);
        rr_for_add_tocart.setOnClickListener(this);
        rr_sub_valve.setOnClickListener(this);
        rr_add_value.setOnClickListener(this);


        arrayList.add("Pretzel Roll");
        arrayList.add("Sliced Bread");

        txt_total_value.setText(index+"");
        txt_voir_le_painer_quant.setText(index+"");


        callWebService();


    }
    @Override
    public void onBackPressed() {

        Intent ii=new Intent(My_CommandActivity.this,MainActivity.class);
        startActivity(ii);
    }
    public void callWebService()

    {
        if(Utils.isConnected(getApplicationContext()))
        {

            try {
                getSingleProductDetails();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }
    private void getSingleProductDetails() {

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        ProductId_Model productBeanModel = new ProductId_Model();
        productBeanModel.setProduct_id(product_id);
        Call<ResponseBody> resultCall = loadInterface.get_SingleProduct(productBeanModel);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {

                                String  record = jsonObject22.getString("record");
                                JSONObject jsonObject33 = new JSONObject(record);
                                String  product_namee = jsonObject33.getString("product_name");


                                if(product_namee!=null && !product_namee.equalsIgnoreCase("null") &&
                                        !product_namee.equalsIgnoreCase(""))
                                {
                                    product_name.setText(product_namee);
                                    title.setText(product_namee);
                                }
                                String  price = jsonObject33.getString("price");




                                String  description = jsonObject33.getString("description");
                                quantity= Double.parseDouble(jsonObject33.getString("quantity"));

                                if(description!=null && !description.equalsIgnoreCase("null") &&
                                        !description.equalsIgnoreCase(""))
                                {
                                    txt_address.setText(description);
                                }

                                PicassoTrustAll.getInstance(getApplicationContext())
                                        .load(jsonObject22.getString("image_url")+jsonObject33.getString("image"))
                                      //.resize(520,320)
                                        .error(R.drawable.bnanner)
                                        .into(product_image);

                                comprend_listAdapter=new Comprend_ListAdapter(My_CommandActivity.this, arrayList);
                                list_item_recyl.setLayoutManager(new LinearLayoutManager(My_CommandActivity.this,LinearLayoutManager.VERTICAL,false));
                                list_item_recyl.setAdapter(comprend_listAdapter);

                                cat_id=jsonObject33.getString("cat_id");
                                total_prize=jsonObject33.getString("price");

                              String tot_prz=total_prize.replace(",",".");

                                if(tot_prz!=null && !tot_prz.equalsIgnoreCase("null") &&
                                        !tot_prz.equalsIgnoreCase(""))
                                {
                                   // txt_quantity.setText("€"+price);

                                   // String intValue = price.replaceAll("[^0-9]", ""); // returns 123
                                    double int_tot_prz= Double.valueOf(tot_prz);


                                    totll_prize=int_tot_prz;
                                    txt_quantity.setText(total_prize+""+"€");
                                }

                             }

                            else
                            {
                                list_item_recyl.setVisibility(GONE);
                                //txt_no_cas_outers.setVisibility(VISIBLE);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });



    }


    public  void callWebService_foraddtocart()
    {

        if(Utils.isConnected(getApplicationContext()))
        {

            try {
               // getDeliveryAddress();
                addtoCart();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }


    public  void getDeliveryAddress()
    {

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        String  userid= GetUserId.getUserInfo(getApplicationContext());


        LoadInterface loadInterface = retrofit.create(LoadInterface.class);
        Singlecasemodel singlecasemodel = new Singlecasemodel();
        singlecasemodel.setUserid(userid);

        Call<ResponseBody> resultCall = loadInterface.getallDeliveryAddress(singlecasemodel);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {
                            //    addtoCart();

                                Gson gson = new Gson();
                                getDeliveryAddressModel = gson.fromJson(responedata, GetDeliveryAddressModel.class);
                                add_gererladdress_delivery_address_adp=new Add_Gererladdress_Delivery_Address_Adp(My_CommandActivity.this,getDeliveryAddressModel.getRecord());

                                showDialogforAddress();


                            }

                            else
                            {
                                Intent ii=new Intent(getApplicationContext(), Ajouter_Une_Adresse_De_Livraison_Activity.class);
                                ii.putExtra("product_id",product_id);
                                startActivity(ii);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });

    }

    private void showDialogforAddress() {

         dialog = new Dialog(My_CommandActivity.this);
        dialog.setContentView(R.layout.add_gererladdress_delivery_address_activity);

        // set the custom dialog components - text, image and button
        RelativeLayout rr_ajourte_crd = (RelativeLayout) dialog.findViewById(R.id.rr_ajourte_crd);
        rr_ajourte_crd.setVisibility(GONE);

        RecyclerView    add_gererladdress_delivery_recyclerView=(RecyclerView)dialog.findViewById(R.id.add_gererladdress_delivery_recyclerView);
        add_gererladdress_delivery_recyclerView.setVisibility(View.VISIBLE);
        add_gererladdress_delivery_recyclerView.setLayoutManager(new LinearLayoutManager(My_CommandActivity.this, LinearLayoutManager.VERTICAL, false));
        add_gererladdress_delivery_recyclerView.setAdapter(add_gererladdress_delivery_address_adp);


        dialog.show();
    }

    private void addtoCart() {

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        String  userid= GetUserId.getUserInfo(getApplicationContext());


        LoadInterface loadInterface = retrofit.create(LoadInterface.class);

        List<Addtocart_Model.CartList> cartLists=new ArrayList<>();
        Addtocart_Model.CartList cartList=new Addtocart_Model.CartList();
        cartList.setCatId(cat_id);
        cartList.setPrice(total_prize);
        cartList.setProductId(product_id);
        cartList.setQuantity(txt_total_value.getText().toString());

        cartLists.add(cartList);

        Addtocart_Model addtocart_model = new Addtocart_Model();
       // addtocart_model.setComboid(userid);
        addtocart_model.setCartList(cartLists);
        addtocart_model.setProductType("1");
        addtocart_model.setUserId(userid);
   //     addtocart_model.setAddress_id(address_id);

//        addtocart_model.setQuantity(txt_total_value.getText().toString());
//        addtocart_model.setAddress_id(address_id);

        Call<ResponseBody> resultCall = loadInterface.addtoCart(addtocart_model);
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {

                                Intent ii=new Intent(My_CommandActivity.this,MainActivity.class);
                                startActivity(ii);


                            }

                            else
                            {


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });

    }


    @Override
    public void onClick(View v) {
        if(v==rr_add_value){
            if (index==1){

            }
            if(index==quantity)
            {

            }
            else {

                String add = String.valueOf(index + 1);
                index = Integer.parseInt(add);

                    double totll_prizee=totll_prize*index;

                    String tot_prz= String.valueOf(totll_prizee);
                    String tot_przz= tot_prz.replace(".",",");
                txt_quantity.setText(tot_przz+"€");


                System.out.println("add:::::" + index);

                txt_total_value.setText(add);
                txt_voir_le_painer_quant.setText(add);
            }


        }
        if(v==rr_back)
        {
            onBackPressed();
        }
        if(v==rr_sub_valve){

            if (index==1){

            }
else if(index == quantity)
            {

            }

            else {

                String add= String.valueOf(index-1);
                index= Integer.parseInt(add);


                double totll_prizee=totll_prize*index;

                String tot_prz= String.valueOf(totll_prizee);
                String tot_przz= tot_prz.replace(".",",");
                txt_quantity.setText(tot_przz+"€");
                //txt_quantity.setText("€"+totll_prizee+"");

                System.out.println("add:::::"+index);
                txt_total_value.setText(add);
                txt_voir_le_painer_quant.setText(add);

            }



        }
        if(v==rr_for_add_tocart){


           /* Intent intent=new Intent(this, Cart_PageActivity.class);
            startActivity(intent);*/

                            loggedin=sp.getData(getApplicationContext(),"ldata","null");

                if(loggedin!=null && !loggedin.equalsIgnoreCase("")  &&
                        !loggedin.equalsIgnoreCase("null")) {

                    callWebService_foraddtocart();
                }
                else
                {
                    Intent ii=new Intent(My_CommandActivity.this,LoginActivity .class);
                    startActivity(ii);
                }
//            Intent intent=new Intent(this, LoginActivity.class);
//            startActivity(intent);
        }

    }



    public class Add_Gererladdress_Delivery_Address_Adp extends RecyclerView.Adapter<Add_Gererladdress_Delivery_Address_Adp.ViewHolder> {
        Context context;
        List<GetDeliveryAddressModel.DeliverAddressDetailsModel> arrayList;

        public Add_Gererladdress_Delivery_Address_Adp(Activity homeActivity, List<GetDeliveryAddressModel.DeliverAddressDetailsModel> arrayList) {
            this.context=homeActivity;
            this.arrayList=arrayList;
        }

        @NonNull
        @Override
        public Add_Gererladdress_Delivery_Address_Adp.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

            View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.add_gererladdress_delivery_addredd_item,viewGroup,false);
            Add_Gererladdress_Delivery_Address_Adp.ViewHolder viewHolder=new Add_Gererladdress_Delivery_Address_Adp.ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull Add_Gererladdress_Delivery_Address_Adp.ViewHolder viewHolder, final int i) {

            viewHolder.txt_crd_vale.setText(arrayList.get(i).getAddress());
            viewHolder.crd_vew.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialog.dismiss();
                    address_id=arrayList.get(i).getId();
                    addtoCart();
                }
            });

        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            CustomTextViewRegular txt_crd_vale;
            CardView crd_vew;
            ImageView img_delete;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                crd_vew=(CardView)itemView.findViewById(R.id.card);

                txt_crd_vale=(CustomTextViewRegular)itemView.findViewById(R.id.txt_crd_vale);
                img_delete=(ImageView)itemView.findViewById(R.id.img_delete);
                img_delete.setVisibility(GONE);
            }
        }
    }

}
