package com.meetch.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.meetch.MainActivity;
import com.meetch.R;

import java.util.ArrayList;

public class Annulaer_cancel_Order_Activity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView cart_item_list_recyle;
    Cart_Item_listAdp cart_item_listAdp;
    ArrayList<String> arrayList=new ArrayList<>();
    Dialog order_cancel_annulercnfrm_dlg;
    Button btn_annuler_la_commande_cancel_order,btn_etour,btn_retour;
    ImageView img_order_cnfrm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_annulaer_cancel_order_activity);
        inti();
    }

    public  void  inti(){
        arrayList.add("Burger");
        arrayList.add("Coco Cola");
        arrayList.add("");
        arrayList.add("");
        arrayList.add("");
        arrayList.add("");

        cart_item_listAdp=new Cart_Item_listAdp(Annulaer_cancel_Order_Activity.this,arrayList);
        cart_item_list_recyle=(RecyclerView)findViewById(R.id.cart_item_list_recyle);
        cart_item_list_recyle.setVisibility(View.VISIBLE);
        cart_item_list_recyle.setLayoutManager(new LinearLayoutManager(Annulaer_cancel_Order_Activity.this, LinearLayoutManager.HORIZONTAL, false));
        cart_item_list_recyle.setAdapter(cart_item_listAdp);

        btn_annuler_la_commande_cancel_order=(Button)findViewById(R.id.btn_annuler_la_commande_cancel_order);
        btn_annuler_la_commande_cancel_order.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v==btn_annuler_la_commande_cancel_order)
        {
            open_Order_Confrm_cancleannnulerdialog();
        }
        if(v==btn_etour)
        {
            order_cancel_annulercnfrm_dlg.dismiss();

            Intent ii=new Intent(Annulaer_cancel_Order_Activity.this, MainActivity.class);
            startActivity(ii);
        }
        if(v==btn_retour)
        {
            order_cancel_annulercnfrm_dlg.dismiss();

        }
        if(v==img_order_cnfrm)
        {
            order_cancel_annulercnfrm_dlg.dismiss();

        }
    }

    public class Cart_Item_listAdp extends RecyclerView.Adapter<Cart_Item_listAdp.ViewHolder> {
        Context context;
        ArrayList<String> arrayList;

        public Cart_Item_listAdp(Activity homeActivity, ArrayList<String> arrayList) {
            this.context=homeActivity;
            this.arrayList=arrayList;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

            View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cart_item,viewGroup,false);
      ViewHolder viewHolder=new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        }



        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public ViewHolder(@NonNull View itemView) {
                super(itemView);
            }
        }
    }
    public void open_Order_Confrm_cancleannnulerdialog()
    {
        order_cancel_annulercnfrm_dlg=new Dialog(Annulaer_cancel_Order_Activity.this);

        order_cancel_annulercnfrm_dlg.getWindow().getAttributes().windowAnimations = R.style.MyAlertDialogStyle;
        // Include dialog.xml file
        order_cancel_annulercnfrm_dlg.setContentView(R.layout.annuler_retur_cancel_dlg);
        order_cancel_annulercnfrm_dlg.show();
        btn_etour=(Button) order_cancel_annulercnfrm_dlg.findViewById(R.id.btn_etour);
        btn_etour.setOnClickListener(this);

        img_order_cnfrm=(ImageView)order_cancel_annulercnfrm_dlg.findViewById(R.id.img_order_cnfrm);
        img_order_cnfrm.setOnClickListener(this);

        btn_retour=(Button) order_cancel_annulercnfrm_dlg.findViewById(R.id.btn_retour);
        btn_retour.setOnClickListener(this);
    }

}
