package com.meetch.activity.bottom_soutien;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.meetch.R;
import com.meetch.bean.faq_model.FAQModel;
import com.meetch.constant.Constants;
import com.meetch.constant.Utils;
import com.meetch.rectrofit.LoadInterface;
import com.meetch.utils.CustomTextView;
import com.meetch.utils.CustomTextViewRegular;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Soutien_Activity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView soutien_questions_fréquemment_posées_recyclerView;
    Soutien_Adp soutien_adp;
    FAQModel faqModel;
    ArrayList<String> arrayList=new ArrayList<>();
    ProgressBar loader;
    private CustomTextView title;
    RelativeLayout rr_back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.soutien_activity);
        inti();
    }

    public  void  inti(){

        loader=(ProgressBar)findViewById(R.id.loader);
        title=(CustomTextView)findViewById(R.id.title);

        rr_back=(RelativeLayout)findViewById(R.id.rr_back);

        rr_back.setOnClickListener(this);

        title.setText(getResources().getString(R.string.soutien));
//
//        arrayList.add("Ma commande n'a pas été livrée?");
//        arrayList.add("Ma commande n'a pas été livrée?");
//        arrayList.add("Ma commande n'a pas été livrée?");
//        arrayList.add("Ma commande n'a pas été livrée?");
//        arrayList.add("Ma commande n'a pas été livrée?");
//        arrayList.add("Ma commande n'a pas été livrée?");
//        arrayList.add("Ma commande n'a pas été livrée?");

        callWebServiceforFaq();

    }


    public void callWebServiceforFaq()
    {
        if(Utils.isConnected(getApplicationContext()))
        {

            try {
                faqget();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_internet_newtwork), Toast.LENGTH_SHORT).show();

        }
    }

    public  void faqget()
    {

        loader.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        LoadInterface loadInterface = retrofit.create(LoadInterface.class);

        Call<ResponseBody> resultCall = loadInterface.faq();
        resultCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                loader.setVisibility(View.GONE);

                System.out.println("Status Response is!!!"+response);

                if (response.isSuccessful()) {

                    try {

                        String responedata = response.body().string();
                        System.out.println("Status Response is@@@"+responedata);
                        try {

                            JSONObject jsonObject22 = new JSONObject(responedata);
                            System.out.println("Status is@@@"+jsonObject22);

                            String  status = jsonObject22.getString("error");

                            if (status.equalsIgnoreCase("false")) {
                                Gson gson = new Gson();
                                faqModel = gson.fromJson(responedata, FAQModel.class);


                                soutien_adp=new Soutien_Adp(Soutien_Activity.this,faqModel.getFaqs());
                                soutien_questions_fréquemment_posées_recyclerView=(RecyclerView)findViewById(R.id.soutien_questions_fréquemment_posées_recyclerView);
                                soutien_questions_fréquemment_posées_recyclerView.setVisibility(View.VISIBLE);
                                soutien_questions_fréquemment_posées_recyclerView.setLayoutManager(new LinearLayoutManager(Soutien_Activity.this, LinearLayoutManager.VERTICAL, false));
                                soutien_questions_fréquemment_posées_recyclerView.setAdapter(soutien_adp);

                            }

                            else
                            {
                                soutien_questions_fréquemment_posées_recyclerView.setVisibility(View.GONE);
                                //txt_no_cas_outers.setVisibility(VISIBLE);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                } else {
                }
//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loader.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_server_newtwork), Toast.LENGTH_SHORT).show();


            }
        });

    }

    @Override
    public void onBackPressed() {

        finish();


    }
    @Override
    public void onClick(View view) {
        if(view==rr_back)
        {
            onBackPressed();
        }

    }

    public class Soutien_Adp extends RecyclerView.Adapter<Soutien_Adp.ViewHolder> {
        Context context;
        List<FAQModel.FaqDescp> arrayList;

        public Soutien_Adp(Activity homeActivity, List<FAQModel.FaqDescp> arrayList) {
            this.context=homeActivity;
            this.arrayList=arrayList;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

            View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.soutien_item,viewGroup,false);
           ViewHolder viewHolder=new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

            viewHolder.txt_question.setText(arrayList.get(i).getQuestion());
            viewHolder.txt_answer.setText(arrayList.get(i).getAnswer());

        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            CardView crd_vew;
            Button btn_voir_le_reçu;
          CustomTextView txt_question;
            CustomTextViewRegular txt_answer;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                crd_vew=(CardView)itemView.findViewById(R.id.crd_vew);
                btn_voir_le_reçu=(Button)itemView.findViewById(R.id.btn_voir_le_reçu);
                txt_question=(CustomTextView)itemView.findViewById(R.id.txt_question);

                txt_answer=(CustomTextViewRegular)itemView.findViewById(R.id.txt_answer);

            }
        }
    }


}
