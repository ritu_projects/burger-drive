package com.meetch.activity.bottom_partage_lapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.meetch.R;
import com.meetch.utils.CustomTextView;

public class Partage_Lapplication_Activity extends AppCompatActivity implements View.OnClickListener {
    RelativeLayout rr_back,rr_shre_via;
    CardView crd_numéro_de_carte;
    private CustomTextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.partage_lapplication_activity);
        inti();
    }

    public  void  inti(){
        title=(CustomTextView)findViewById(R.id.title);
        rr_shre_via=(RelativeLayout)findViewById(R.id.rr_shre_via);
        crd_numéro_de_carte=(CardView)findViewById(R.id.crd_numéro_de_carte);
        crd_numéro_de_carte.setOnClickListener(this);

        rr_back=(RelativeLayout)findViewById(R.id.rr_back);
        rr_back.setOnClickListener(this);

        rr_shre_via.setOnClickListener(this);

        title.setText(getResources().getString(R.string.mode_de_paiement));

    }
    @Override
    public void onBackPressed() {

        finish();


    }

    @Override
    public void onClick(View view) {

        if(view==rr_back)
        {
            onBackPressed();
        }

        if(view==crd_numéro_de_carte)
        {
            shareApp();
        }

    }

    public void shareApp()
    {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "MEETECH";
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "MEETECH");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Partager via"));
    }

}
