package com.meetch;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.messaging.FirebaseMessaging;
import com.meetch.activity.FavoritesActivity;
import com.meetch.activity.LoginActivity;
import com.meetch.R;
import com.meetch.config.Config;
import com.meetch.fragment.bottomfragment.Account_Mon_Complete_Frag;
import com.meetch.fragment.bottomfragment.Favrois_Frag;
import com.meetch.fragment.bottomfragment.HomeFragment;
import com.meetch.fragment.bottomfragment.Nos_SpecialBurger_Frag;
import com.meetch.gpstracker.GPSTracker;
import com.meetch.storage.MySharedPref;

public class MainActivity extends AppCompatActivity {


    BottomNavigationView bottomNavigationView;
    Fragment fragment;
    public static final int RequestPermissionCode = 1;
MySharedPref sp=new MySharedPref();;
String loggedin="";
Bundle b1;
    String fcm_id="";
    private static BroadcastReceiver mRegistrationBroadcastReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getBundleData();
        checkForPermission();
        EnableRuntimePermission();
      //  getFirebaseid();
        initUi();
    }

    private void getBundleData() {
        b1=getIntent().getExtras();
        if(b1!=null)
        {

        }
    }

    private void getFirebaseid() {

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {

                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId(getApplicationContext());

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {

                    String message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                }
            }
        };

        displayFirebaseRegId(getApplicationContext());
    }
    public String displayFirebaseRegId(Context context) {


        fcm_id=sp.getData(context,"token","null");
        Log.e("FCM Id Main",fcm_id);
        System.out.println("Firebase Id at Main is@@@"+fcm_id);
        return  fcm_id;
    }

    private void checkForPermission() {
        int permissionCheckForCamera = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionCheckForGallery = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionCheckForAccessCamera = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        int permissionCheckForAccessAudioRecord = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION);


        if (permissionCheckForCamera != PackageManager.PERMISSION_GRANTED ||
                permissionCheckForGallery != PackageManager.PERMISSION_GRANTED ||
                permissionCheckForAccessCamera != PackageManager.PERMISSION_GRANTED ||
                permissionCheckForAccessAudioRecord != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},
                    1001);
        }

    }
    public void EnableRuntimePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION)) {


            Toast.makeText(MainActivity.this, getResources().getString(R.string.location_permission), Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION}, RequestPermissionCode);

        }
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {

            case RequestPermissionCode:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {

                    GPSTracker gps = new GPSTracker(MainActivity.this);
                    double latitude = gps.getLatitude();
                    double longitude = gps.getLongitude();
                    System.out.println("Latitude  "+latitude);
                    System.out.println("Longitude  "+longitude);

                } else {

                    Toast.makeText(MainActivity.this, getResources().getString(R.string.location_permission_cancelled), Toast.LENGTH_LONG).show();

                }
                break;
        }
    }
    private void initUi() {

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setItemIconTintList(null);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment, new HomeFragment())
                .commit();

        bottomNavigationView.getMenu().getItem(0).setChecked(true);
        bottomNavigationView.getMenu().getItem(1).setChecked(false);
        bottomNavigationView.getMenu().getItem(2).setChecked(false);
        bottomNavigationView.getMenu().getItem(3).setChecked(false);
        MySharedPref sp=new MySharedPref();
        String   loggedin=sp.getData(getApplicationContext(),"ldata","null");
        if(loggedin!=null && !loggedin.equalsIgnoreCase("")  &&
                !loggedin.equalsIgnoreCase("null")) {

            bottomNavigationView.getMenu().getItem(3).setTitle(getResources().getText(R.string.account_mon_compte));


        }

        else
        {
            bottomNavigationView.getMenu().getItem(3).setTitle(getResources().getText(R.string.text_login));


        }




            bottomNavigationView.getMenu().getItem(0).setIcon(R.drawable.accueil_home_filled_icon);

        bottomNavigationView.getMenu().getItem(1).setIcon(R.drawable.special_burger_icon);
        bottomNavigationView.getMenu().getItem(2).setIcon(R.drawable.favorite_icons);
        bottomNavigationView.getMenu().getItem(3).setIcon(R.drawable.account_mon_compte_icon);

        if(b1!=null)
        {
            bottomNavigationView.getMenu().getItem(3).setChecked(true);

            bottomNavigationView.getMenu().getItem(0).setIcon(R.drawable.accueil_home_icon);

            bottomNavigationView.getMenu().getItem(1).setIcon(R.drawable.special_burger_icon);
            bottomNavigationView.getMenu().getItem(2).setIcon(R.drawable.favorite_icons);
            bottomNavigationView.getMenu().getItem(3).setIcon(R.drawable.account_mon_compte_filled_icon);

            ckCondition();
        }



        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {

                    case R.id.action_homeicon:
                        bottomNavigationView.getMenu().getItem(0).setChecked(true);
                        bottomNavigationView.getMenu().getItem(0).setIcon(R.drawable.accueil_home_filled_icon);

                        bottomNavigationView.getMenu().getItem(1).setIcon(R.drawable.special_burger_icon);
                        bottomNavigationView.getMenu().getItem(2).setIcon(R.drawable.favorite_icons);
                        bottomNavigationView.getMenu().getItem(3).setIcon(R.drawable.account_mon_compte_icon);
                        fragment = new HomeFragment();
                        loadFragment2(fragment);
                        break;

                    case R.id.action_special_burger:

                        bottomNavigationView.getMenu().getItem(1).setChecked(true);
                        bottomNavigationView.getMenu().getItem(0).setIcon(R.drawable.accueil_home_icon);

                        bottomNavigationView.getMenu().getItem(1).setIcon(R.drawable.special_burger_filled_icon);
                        bottomNavigationView.getMenu().getItem(2).setIcon(R.drawable.favorite_icons);
                        bottomNavigationView.getMenu().getItem(3).setIcon(R.drawable.account_mon_compte_icon);

                        fragment = new Nos_SpecialBurger_Frag();
                        //Code Change at 18-March-2020

                        // loadFragment2(fragment);
                        MySharedPref sp=new MySharedPref();
                        String   loggedin=sp.getData(getApplicationContext(),"ldata","null");
                        if(loggedin!=null && !loggedin.equalsIgnoreCase("")  &&
                                !loggedin.equalsIgnoreCase("null")) {
                            Intent intent=new Intent(MainActivity.this, FavoritesActivity.class);
                            startActivity(intent);
                        }
                        else
                        {
                            Intent ii = new Intent(MainActivity.this, LoginActivity.class);
                            startActivity(ii);
                        }
                        break;
                    case R.id.action_favrois:

                        bottomNavigationView.getMenu().getItem(2).setChecked(true);

                        bottomNavigationView.getMenu().getItem(0).setIcon(R.drawable.accueil_home_icon);

                        bottomNavigationView.getMenu().getItem(1).setIcon(R.drawable.special_burger_icon);
                        bottomNavigationView.getMenu().getItem(2).setIcon(R.drawable.favorite_filled_icons);
                        bottomNavigationView.getMenu().getItem(3).setIcon(R.drawable.account_mon_compte_icon);


                        MySharedPref sp2=new MySharedPref();
                        String   loggedin2=sp2.getData(getApplicationContext(),"ldata","null");
                        if(loggedin2!=null && !loggedin2.equalsIgnoreCase("")  &&
                                !loggedin2.equalsIgnoreCase("null")) {

                            fragment = new Favrois_Frag();
                            loadFragment2(fragment);

                        }
                        else
                        {
                            Intent intent=new Intent(MainActivity.this, FavoritesActivity.class);
                            startActivity(intent);
                        }

                        break;

                    case R.id.action_account_mon_complete:

                        bottomNavigationView.getMenu().getItem(3).setChecked(true);

                        bottomNavigationView.getMenu().getItem(0).setIcon(R.drawable.accueil_home_icon);

                        bottomNavigationView.getMenu().getItem(1).setIcon(R.drawable.special_burger_icon);
                        bottomNavigationView.getMenu().getItem(2).setIcon(R.drawable.favorite_icons);
                        bottomNavigationView.getMenu().getItem(3).setIcon(R.drawable.account_mon_compte_filled_icon);

                        ckCondition();


                        break;
                }
                return true;
            }
        });


    }
    @Override
    public void onBackPressed() {

        int count = getSupportFragmentManager().getBackStackEntryCount();
        System.out.println("Count%%%" + count);

        if (count == 0) {


            // initUi();
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getResources().getString(R.string.doyou_wnt_to_exit))
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.text_yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            Intent i = new Intent(Intent.ACTION_MAIN);
                            i.addCategory(Intent.CATEGORY_HOME);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            // finish();
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.text_no), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }

        else {

          /*  tint.setVisibility(View.VISIBLE);

            rr_cross.setVisibility(View.GONE);
            menu_lay.setVisibility(View.GONE);*/
            getSupportFragmentManager().popBackStack();
        }
       /* DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }*/
    }


    private void ckCondition() {
        loggedin=sp.getData(getApplicationContext(),"ldata","null");

        if(loggedin!=null && !loggedin.equalsIgnoreCase("")  &&
                !loggedin.equalsIgnoreCase("null")) {

            fragment = new Account_Mon_Complete_Frag();
            loadFragment2(fragment);


        }
        else
        {
            Intent ii=new Intent(MainActivity.this,LoginActivity.class);
            startActivity(ii);

        }
    }

    private void loadFragment2(Fragment fragment) {



        System.out.println("Fragment is@@@"+fragment.toString());
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, fragment);
        transaction.addToBackStack("1");
        transaction.commit();




    }
}
