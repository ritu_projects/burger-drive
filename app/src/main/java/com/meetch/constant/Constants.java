package com.meetch.constant;

public class Constants {

  public static final String BASE_URL = "https://curzenncloud.fr/burger/";


  public static final String USERNAME = "username";
  public static final String USEREMAIL = "email";

  public static final String USERPASSWORD = "password";

  public static final String DEVICE_TOKEN = "device_token";
  public static final String DEVICE_TYPE = "device_type";

  public static final String CONST_ACCEPT = "Content-Type: application/json";
  public static final String PROFILE_UPDATE_URL = BASE_URL+"api/edituserprofile";

  public static final int NOTIFICATION_ID_BIG_IMAGE = 101;


}


