package com.meetch.constant;

import android.content.Context;
import android.util.Log;


import com.meetch.storage.MySharedPref;

public class GetUserId {
    static MySharedPref sp;
    static String ldata;
    static String user_id,token,name,fcm_id,email;


    public static String getUserInfo(Context context) {

        sp = new MySharedPref();
        user_id = sp.getData(context, "user_id", "null");
        Log.e("LdataHome", user_id);
        return user_id;



    }
        public static String getUserToken(Context context) {

            sp = new MySharedPref();
            token = sp.getData(context, "token", "null");
            Log.e("LdataHome", token);


            return  token;
        }


    public static String getUserEmail(Context context) {

        sp = new MySharedPref();
        email = sp.getData(context, "email", "null");
        Log.e("LdataHome", email);


        return  email;
    }



}
