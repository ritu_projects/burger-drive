package com.meetch.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.meetch.R;
import com.meetch.activity.combodetails.COmboDetails_Activity;
import com.meetch.bean.comboList.ComboList_Bean;
import com.meetch.picasso.PicassoTrustAll;
import com.meetch.utils.CustomTextView;
import com.meetch.utils.CustomTextViewRegular;

import java.util.List;

public class BurgerAdapter extends RecyclerView.Adapter<BurgerAdapter.ViewHolder> {
    Context context;
    List<ComboList_Bean.ComboList_DetailsBean> arrayList;

    public BurgerAdapter(Activity homeActivity, List<ComboList_Bean.ComboList_DetailsBean> arrayList) {
        this.context=homeActivity;
        this.arrayList=arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.best_offer_item,viewGroup,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.txt_favoris.setText(arrayList.get(i).getName());
        viewHolder.txt_address.setText(arrayList.get(i).getPrice()+"€");

        PicassoTrustAll.getInstance(context)
                .load(arrayList.get(i).getComboImg())
                .error(R.drawable.bnanner)
                .into(viewHolder.img_best_offer);

        viewHolder.crd_vew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent ii=new Intent(context, COmboDetails_Activity.class);
                ii.putExtra("combo_id",arrayList.get(i).getId());
                context.startActivity(ii);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CardView crd_vew;
        CustomTextView txt_favoris;
        CustomTextViewRegular txt_address;
        ImageView img_best_offer;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            crd_vew=(CardView)itemView.findViewById(R.id.crd_vew);
            txt_favoris=(CustomTextView)itemView.findViewById(R.id.txt_favoris);
            img_best_offer=(ImageView)itemView.findViewById(R.id.img_best_offer);
            txt_address=(CustomTextViewRegular)itemView.findViewById(R.id.txt_address);
            txt_address.setVisibility(View.VISIBLE);

        }
    }
}
