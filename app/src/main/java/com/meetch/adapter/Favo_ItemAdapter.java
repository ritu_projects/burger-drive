package com.meetch.adapter;

import android.app.Activity;
import android.content.Context;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.meetch.R;
import com.meetch.activity.My_CommandActivity;
import com.meetch.bean.myfavorite_bean.MyFavorite_Bean;
import com.meetch.picasso.PicassoTrustAll;
import com.meetch.utils.CustomTextView;
import com.meetch.utils.CustomTextViewRegular;

import java.util.List;

public class Favo_ItemAdapter extends RecyclerView.Adapter<Favo_ItemAdapter.ViewHolder> {
    Context context;
    List<MyFavorite_Bean.OrderList> list;

    public Favo_ItemAdapter(Activity favoritesActivity, List<MyFavorite_Bean.OrderList> arrayList) {
        this.context=favoritesActivity;
        this.list=arrayList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cart_item,viewGroup,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {

        PicassoTrustAll.getInstance(context)
                .load(list.get(i).getImage())
               // .resize(R.dimen.mrg100,R.dimen.mrg100)
               // .error(R.drawable.bnanner)
                .into(viewHolder.img_banner);

        viewHolder.cat_name.setText(list.get(i).getProductName());
        viewHolder.product_price.setText(list.get(i).getPurchaseQuantity()+" commande");
        viewHolder.crd_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                        Intent ii=new Intent(context, My_CommandActivity.class);
                        ii.putExtra("product_id",list.get(i).getProductId());
                        context.startActivity(ii);

            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_banner;
        CustomTextView cat_name;
        CustomTextViewRegular product_price;
CardView crd_view;
        public  ViewHolder(View view){
            super(view);
            img_banner=(ImageView) view.findViewById(R.id.img_banner);
            cat_name=(CustomTextView)view.findViewById(R.id.cat_name);
            product_price=(CustomTextViewRegular)view.findViewById(R.id.txt_cat_price);
            crd_view=(CardView)view.findViewById(R.id.crd_view);
            product_price.setVisibility(View.VISIBLE);
        }

    }
}
