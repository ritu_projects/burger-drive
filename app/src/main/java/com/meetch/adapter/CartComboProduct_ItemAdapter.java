package com.meetch.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.meetch.R;
import com.meetch.activity.My_CommandActivity;
import com.meetch.activity.categorylist.CategoriesList_Activity;
import com.meetch.bean.productListatCheckoutPage_model.ProductListatCheckoutPage_model;
import com.meetch.picasso.PicassoTrustAll;
import com.meetch.utils.CustomTextViewRegular;

import java.util.ArrayList;

public class CartComboProduct_ItemAdapter extends RecyclerView.Adapter<CartComboProduct_ItemAdapter.ViewHolder> {
    Context context;
    ArrayList<ProductListatCheckoutPage_model> arrayList;
    String product_base_url,str_combo_img;
//    public Cart_ComprendItemAdapter(AppCompatActivity cart_pageActivity, ArrayList<String> arrayList) {
//        this.context=cart_pageActivity;
//        this.arrayList=arrayList;
//    }

    public CartComboProduct_ItemAdapter(AppCompatActivity cart_pageActivity, ArrayList<ProductListatCheckoutPage_model> arrayList_product, String product_base_url) {
        this.context=cart_pageActivity;
        this.arrayList=arrayList_product;
        this.str_combo_img=product_base_url;
    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tu_aimer_item,viewGroup,false);
    ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        viewHolder.product_name.setText(arrayList.get(i).getProduct_name());




        try {
            if (arrayList.get(i).getCat_name() != null) {

                if (arrayList.get(i).getCat_name() != null && !arrayList.get(i).getCat_name().equalsIgnoreCase("") &&
                        !arrayList.get(i).getCat_name().equalsIgnoreCase("null")) {
                    viewHolder.cat_name.setVisibility(View.VISIBLE);
                    // viewHolder.cat_name.setText(context.getResources().getString(R.string.cat_name)+arrayList.get(i).getCat_name());
                    viewHolder.cat_name.setText(arrayList.get(i).getCat_name());

                    viewHolder.product_name.setVisibility(View.VISIBLE);
                    viewHolder.product_name.setText(arrayList.get(i).getProduct_name());

                    viewHolder.quantity.setVisibility(View.VISIBLE);
                    viewHolder.quantity.setText(context.getResources().getString(R.string.quantity) + ": " + arrayList.get(i).getQuantity());

                    PicassoTrustAll.getInstance(context)
                            .load(str_combo_img)
                            //   .resize(100,100)
                            .error(R.drawable.bnanner)
                            .into(viewHolder.img_category);

                }

            }
        }
        catch (Exception e)
        {

        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_category;
        CustomTextViewRegular cat_name,product_name,quantity;

        public ViewHolder(@NonNull View itemView) {

            super(itemView);
            img_category=(ImageView)itemView.findViewById(R.id.img_category);
            cat_name=(CustomTextViewRegular)itemView.findViewById(R.id.cat_name);
            quantity=(CustomTextViewRegular)itemView.findViewById(R.id.quantity);
            product_name=(CustomTextViewRegular) itemView.findViewById(R.id.product_name);
        }
    }
}
