package com.meetch.adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.meetch.R;
import com.meetch.bean.productbean_model.ProductBeanModel;
import com.meetch.utils.CustomTextViewRegular;

import java.util.ArrayList;
import java.util.List;

public class Comprend_ListAdapter  extends RecyclerView.Adapter<Comprend_ListAdapter.ViewHolder> {
    Context context;
    ArrayList<String> arrayList;
    List<ProductBeanModel.ProductModelDescp> arrayList2;

    public Comprend_ListAdapter(AppCompatActivity my_commandActivity, ArrayList<String> arrayList) {
        this.context=my_commandActivity;
        this.arrayList=arrayList;
    }
//
//    public Comprend_ListAdapter(My_CommandActivity my_commandActivity, List<ProductBeanModel.ProductModelDescp> burger) {
//        this.context=my_commandActivity;
//        this.arrayList=burger;
//    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chroix_item,viewGroup,false);
        ViewHolder viewHolder =new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        System.out.println("List name:::::"+arrayList.get(i).toString());
        viewHolder.txt_chroix.setText(arrayList.get(i).toString());

//        if(viewHolder.rado_check.isChecked())
//        {
//            viewHolder.rado_check.setChecked(false);
//        }
//        else if(viewHolder.rado_check.isChecked() == false)
//        {
//            viewHolder.rado_check.setChecked(true);
//
//        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CustomTextViewRegular txt_chroix;
        RadioButton rado_check;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_chroix=(CustomTextViewRegular)itemView.findViewById(R.id.txt_chroix);
            rado_check=(RadioButton) itemView.findViewById(R.id.rado_check);
        }
    }
}
