package com.meetch.adapter;

import android.app.Activity;
import android.content.Context;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.meetch.R;
import com.meetch.activity.My_CommandActivity;
import com.meetch.bean.myfavorite_bean.MyFavorite_Bean;
import com.meetch.picasso.PicassoTrustAll;
import com.meetch.utils.CustomTextView;
import com.meetch.utils.CustomTextViewRegular;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class Special_BurgerAdapter extends RecyclerView.Adapter<Special_BurgerAdapter.ViewHolder> {
    Context context;
    List<MyFavorite_Bean.OrderList> list;

    public Special_BurgerAdapter(Activity favoritesActivity, List<MyFavorite_Bean.OrderList> arrayList) {
        this.context=favoritesActivity;
        this.list=arrayList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.spcial_burger_item,viewGroup,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull Special_BurgerAdapter.ViewHolder viewHolder, final int i) {
        PicassoTrustAll.getInstance(context)
                .load(list.get(i).getImage())
                // .resize(R.dimen.mrg100,R.dimen.mrg100)
                // .error(R.drawable.bnanner)
                .into(viewHolder.item_image);

        viewHolder.product_name.setText(list.get(i).getProductName());
        viewHolder.product_price.setText(list.get(i).getPrice()+"€");
        viewHolder.crd_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ii=new Intent(context, My_CommandActivity.class);
                ii.putExtra("product_id",list.get(i).getProductId());
                context.startActivity(ii);
            }
        });




    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView item_image;
        CustomTextView product_name;
        CardView crd_view;
        CustomTextViewRegular product_price;

        public  ViewHolder(View view){
            super(view);
            item_image=(CircleImageView)view.findViewById(R.id.item_image);
            product_name=(CustomTextView)view.findViewById(R.id.product_name);
            product_price=(CustomTextViewRegular)view.findViewById(R.id.product_price);
            crd_view=(CardView)view.findViewById(R.id.crd_view);
        }

    }
}
