package com.meetch.adapter;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.meetch.fragment.ChildFragment1;
import com.meetch.fragment.ChildFragment2;
import com.meetch.fragment.ChildFragment3;
import com.meetch.fragment.ChildFragment4;

public class ViewPagerAdapter  extends FragmentPagerAdapter {


    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                return new ChildFragment1(); //ChildFragment1 at position 0
            case 1:
                return new ChildFragment2(); //ChildFragment2 at position 1
            case 2:
                return new ChildFragment3(); //ChildFragment3 at position 2
            case 3:
                return new ChildFragment4(); //ChildFragment4 at position 3

        }
        return new ChildFragment1(); //does not happen
    }

    @Override
    public int getCount() {
        return 4; //three fragments
    }
}

