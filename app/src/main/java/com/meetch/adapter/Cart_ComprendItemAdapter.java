package com.meetch.adapter;

import android.content.Context;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.meetch.R;
import com.meetch.activity.My_CommandActivity;
import com.meetch.bean.productListatCheckoutPage_model.ProductListatCheckoutPage_model;
import com.meetch.picasso.PicassoTrustAll;
import com.meetch.utils.CustomTextViewRegular;

import java.util.ArrayList;

public class Cart_ComprendItemAdapter extends RecyclerView.Adapter<Cart_ComprendItemAdapter.ViewHolder> {
    Context context;
    ArrayList<ProductListatCheckoutPage_model> arrayList;
    String product_base_url,str_combo_img;

//    public Cart_ComprendItemAdapter(AppCompatActivity cart_pageActivity, ArrayList<String> arrayList) {
//        this.context=cart_pageActivity;
//        this.arrayList=arrayList;
//    }

    public Cart_ComprendItemAdapter(AppCompatActivity cart_pageActivity, ArrayList<ProductListatCheckoutPage_model> arrayList_product, String product_base_url) {
        this.context=cart_pageActivity;
       this.arrayList=arrayList_product;
       this.product_base_url=product_base_url;
    }

    public Cart_ComprendItemAdapter(AppCompatActivity cart_pageActivity, ArrayList<ProductListatCheckoutPage_model> arrayList_product,
                                    String product_base_url,String str_combo_img) {
        this.context=cart_pageActivity;
        this.arrayList=arrayList_product;
        this.product_base_url=product_base_url;
        this.str_combo_img=str_combo_img;
    }

    @NonNull
    @Override
    public Cart_ComprendItemAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tu_aimer_item,viewGroup,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        viewHolder.product_name.setText(arrayList.get(i).getProduct_name());





        try {
            if(arrayList.get(i).getCat_name()!=null)
            {

                if(arrayList.get(i).getCat_name()!=null && !arrayList.get(i).getCat_name().equalsIgnoreCase("") &&
                        !arrayList.get(i).getCat_name().equalsIgnoreCase("null"))
                {
                    viewHolder.cat_name.setVisibility(View.VISIBLE);
                   // viewHolder.cat_name.setText(context.getResources().getString(R.string.cat_name)+arrayList.get(i).getCat_name());
                    viewHolder.cat_name.setText(arrayList.get(i).getCat_name());

                    viewHolder.quantity.setVisibility(View.VISIBLE);
                    viewHolder.quantity.setText(context.getResources().getString(R.string.quantity)+": "+arrayList.get(i).getQuantity());

                    PicassoTrustAll.getInstance(context)
                            .load(str_combo_img)
                         //   .resize(100,100)
                            .error(R.drawable.bnanner)
                            .into(viewHolder.img_category);

                }
                else {
                    PicassoTrustAll.getInstance(context)
                            .load(product_base_url+arrayList.get(i).getImage())
                         //   .resize(100,100)
                            .error(R.drawable.bnanner)
                            .into(viewHolder.img_category);
                }
            }

            else {
                PicassoTrustAll.getInstance(context)
                        .load(product_base_url+arrayList.get(i).getImage())
                     //   .resize(100,100)
                        .error(R.drawable.bnanner)
                        .into(viewHolder.img_category);
            }
            viewHolder.ll_related_product.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    System.out.println("Related Product "+"Related Product");

                    Intent ii=new Intent(context, My_CommandActivity.class);
                    ii.putExtra("product_id",arrayList.get(i).getId());
                    context.startActivity(ii);

                }
            });


        }
        catch (Exception e)
        {
            System.out.println("Exception is  "+e);
            PicassoTrustAll.getInstance(context)
                    .load(product_base_url+arrayList.get(i).getImage())
                 //   .resize(100,100)
                    .error(R.drawable.bnanner)
                    .into(viewHolder.img_category);
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_category;
        CustomTextViewRegular cat_name,product_name,quantity;
        LinearLayout ll_related_product;

        public ViewHolder(@NonNull View itemView) {

            super(itemView);
            img_category=(ImageView)itemView.findViewById(R.id.img_category);
            cat_name=(CustomTextViewRegular)itemView.findViewById(R.id.cat_name);
            quantity=(CustomTextViewRegular)itemView.findViewById(R.id.quantity);
            product_name=(CustomTextViewRegular) itemView.findViewById(R.id.product_name);
            ll_related_product = (LinearLayout) itemView.findViewById(R.id.ll_related_product);
        }
    }
}
