package com.meetch.adapter;

import android.app.Activity;
import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.meetch.R;
import com.meetch.bean.catergoryListatCheckoutPage_model.CategroryListCheckout_model;
import com.meetch.picasso.PicassoTrustAll;
import com.meetch.utils.CustomTextView;
import com.meetch.utils.CustomTextViewRegular;

import java.util.ArrayList;

public class Card_ItemAdapter extends RecyclerView.Adapter<Card_ItemAdapter.ViewHolder> {
    Context context;
    ArrayList<CategroryListCheckout_model> arrayList;
    String product_base_url;

    public Card_ItemAdapter(Activity cart_pageActivity, ArrayList<CategroryListCheckout_model> arrayList,String product_base_url) {
        this.context=cart_pageActivity;
        this.arrayList=arrayList;
        this.product_base_url=product_base_url;
    }

    @NonNull
    @Override
    public Card_ItemAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cart_item,viewGroup,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull Card_ItemAdapter.ViewHolder viewHolder, final int i) {


        viewHolder.cat_name.setText(arrayList.get(i).getCat_product_name());
        PicassoTrustAll.getInstance(context)
                .load(product_base_url+arrayList.get(i).getCat_img())
             //   .resize(1000,100)
                .error(R.drawable.bnanner)
                .into(viewHolder.img_banner);

        viewHolder.txt_cat_price.setText(arrayList.get(i).getCat_prize()+"€");
        viewHolder.txt_cat_quantity.setText(context.getResources().getString(R.string.quantity)+": "+arrayList.get(i).getCat_quantity());
        viewHolder.cancel_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             //   Cart_PageActivity.showDialogforCancelCart(arrayList.get(i).getId());
            }

        });


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_banner;
CustomTextView cat_name;
CustomTextViewRegular txt_cat_price,txt_cat_quantity;
ImageView cancel_image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_banner=(ImageView)itemView.findViewById(R.id.img_banner);
            cat_name=(CustomTextView)itemView.findViewById(R.id.cat_name);
            txt_cat_price=(CustomTextViewRegular)itemView.findViewById(R.id.txt_cat_price);
            txt_cat_quantity=(CustomTextViewRegular)itemView.findViewById(R.id.txt_cat_quantity);
            cancel_image=(ImageView)itemView.findViewById(R.id.cancel_image);

            cancel_image.setVisibility(View.VISIBLE);
            txt_cat_price.setVisibility(View.VISIBLE);
            txt_cat_quantity.setVisibility(View.VISIBLE);

        }
    }

}
