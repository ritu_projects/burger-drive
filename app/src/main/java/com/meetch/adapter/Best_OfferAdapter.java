package com.meetch.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.meetch.R;
import com.meetch.activity.My_CommandActivity;
import com.meetch.bean.productbean_model.ProductBeanModel;
import com.meetch.picasso.PicassoTrustAll;
import com.meetch.utils.CustomTextView;
import com.meetch.utils.CustomTextViewRegular;

import java.util.List;

public class Best_OfferAdapter extends RecyclerView.Adapter<Best_OfferAdapter.ViewHolder> {
    Context context;
    List<ProductBeanModel.ProductModelDescp> arrayList;
    String imageUrl;

//    public Best_OfferAdapter(Activity homeActivity, List<GetProduct_Bean.GetProductDescp_Bean> arrayList) {
//        this.context=homeActivity;
//        this.arrayList=arrayList;
//
//    }

    public Best_OfferAdapter(Activity homeActivity, List<ProductBeanModel.ProductModelDescp> burger, String imageUrl) {

        this.context=homeActivity;
        this.arrayList=burger;
        this.imageUrl=imageUrl;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.best_offer_item,viewGroup,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.txt_favoris.setText(arrayList.get(i).getProductName());
        viewHolder.txt_address.setText(arrayList.get(i).getPrice()+"€");
        viewHolder.txt_qunatity.setText(arrayList.get(i).getQuantity());

        PicassoTrustAll.getInstance(context)
                .load(imageUrl+arrayList.get(i).getImage())
                .error(R.drawable.bnanner)
                .into(viewHolder.img_best_offer);

        viewHolder.crd_vew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent ii=new Intent(context, My_CommandActivity.class);
                ii.putExtra("product_id",arrayList.get(i).getId());
                context.startActivity(ii);

            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView crd_vew;
        CustomTextView txt_favoris;
        ImageView img_best_offer;
        CustomTextViewRegular txt_address,txt_qunatity;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            crd_vew=(CardView)itemView.findViewById(R.id.crd_vew);
            txt_favoris=(CustomTextView)itemView.findViewById(R.id.txt_favoris);
            img_best_offer=(ImageView)itemView.findViewById(R.id.img_best_offer);
            txt_address=(CustomTextViewRegular)itemView.findViewById(R.id.txt_address);
            txt_qunatity=(CustomTextViewRegular)itemView.findViewById(R.id.txt_qunatity);
            txt_address.setVisibility(View.VISIBLE);
            txt_qunatity.setVisibility(View.GONE);
        }
    }
}
